//
//  AppDelegate.swift
//  Gir Gau Jatan Sansthan
//
//  Created by Miral Gondaliya on 03/03/20.
//  Copyright © 2020 Miral Gondaliya. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Firebase
import UserNotifications
import GLNotificationBar
import IHProgressHUD
import GoogleMobileAds
//import  Stripe

let SharedAppDelegate = UIApplication.shared.delegate as! AppDelegate

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, MessagingDelegate, UNUserNotificationCenterDelegate {


    var window: UIWindow?
    var sideMenuWidht: CGFloat = 300
    var sideMenuObj = SideMenuVC();

    var sideMenu = UIView();
    var subSideMenu = UIView();
    var overlayButton = UIButton();

    var navigationControl = UINavigationController()
    var initialViewController = UIViewController();
    var localization = HMLocalization()

    var strMobile = ""
    var dataUser = NSDictionary()
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        IQKeyboardManager.shared.enable = true
        GADMobileAds.sharedInstance().start(completionHandler: nil)
//GADMobileAds.sharedInstance.requestConfiguration.testDeviceIdentifiers = [ "1e175560c36464ed2e5babb076c3aaa1" ]
        GADMobileAds.sharedInstance().requestConfiguration.testDeviceIdentifiers = ["1e175560c36464ed2e5babb076c3aaa1"]
        self.window = UIWindow(frame: UIScreen.main.bounds)

        FirebaseApp.configure()
        initialViewController = Util.getStoryboard().instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        Util.setUserDefaultValue(key: "Language", value: "EN")
        localization.setLanguage(UserDefaults.standard.string(forKey: "EN"))
        //STPPaymentConfiguration.shared.publishableKey = "pk_test_WkjizjxN2MdJ3Mvsjbwpd0E7q"
        
        Util.sharedInstance.user_language = Util.getUserDefaultValue(key: "user_language")

        if UserDefaults.standard.string(forKey: "user_language") != nil {
            
            if UserDefaults.standard.string(forKey: "user_language") == "en" {
                localization.setLanguage("EN")

            } else if UserDefaults.standard.string(forKey: "user_language") == "hi" {
                localization.setLanguage("HI")

            } else if UserDefaults.standard.string(forKey: "user_language") == "gj" {
                localization.setLanguage("GJ")

            }

        } else {
            Util.setUserDefaultValue(key: "user_language", value: "en")
            localization.setLanguage(UserDefaults.standard.string(forKey: "user_language"))
        }
        
        if Util.isLogin() {

            Util.sharedInstance.user_id = Util.getUserDefaultValue(key: "user_id")
            Util.sharedInstance.user_unique_id = Util.getUserDefaultValue(key: "user_unique_id")
            //Remove this line
            Util.sharedInstance.user_unique_id = "1"
            //Remove this line
            Util.sharedInstance.user_name = Util.getUserDefaultValue(key: "user_name")
            Util.sharedInstance.user_city = Util.getUserDefaultValue(key: "user_city")
            Util.sharedInstance.user_phone = Util.getUserDefaultValue(key: "user_phone")
            Util.sharedInstance.user_subscription = Util.getUserDefaultValue(key: "user_subscription")
            Util.sharedInstance.user_subscription_details = Util.getUserDefaultValue(key: "user_subscription_details")
            Util.sharedInstance.user_status = Util.getUserDefaultValue(key: "user_status")
            Util.sharedInstance.advertisement_amount = Util.getUserDefaultValue(key: "advertisement_amount")
            
            
            //advertisement_amount
            // apiCallDashboard()
            

            apiCallDashboard()

        }
        self.navigationControl = UINavigationController(rootViewController: self.initialViewController)

        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = self.navigationControl;
        self.window?.makeKeyAndVisible()
        self.navigationControl.setNavigationBarHidden(true, animated: true);
        if screenWidth > 375 {
            sideMenuWidht = 300
        } else {
            sideMenuWidht = 265
        }
        configureSideMenu()

        #if compiler(>=5.1)
            if #available(iOS 13.0, *) {
                // Always adopt a light interface style.
                window?.overrideUserInterfaceStyle = .light
            }
        #endif

        Messaging.messaging().delegate = self
        registerRemoteNoti()
        return true
    }
    
    

    func configureSideMenu() {
        self.sideMenuObj = SideMenuVC.init(nibName: "SideMenuVC", bundle: nil);
        self.sideMenu = UIView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight))

        self.sideMenu.backgroundColor = UIColor.clear
        self.sideMenu.isHidden = true;
        self.window?.addSubview(self.sideMenu);

        if (IS_IPHONE_X) {
            self.overlayButton = UIButton(frame: CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight + 40));
        } else {
            self.overlayButton = UIButton(frame: CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight));
        }

        self.overlayButton.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.5);
        self.overlayButton.addTarget(self, action: #selector(doSingleTap), for: .touchUpInside);
        self.sideMenu.addSubview(self.overlayButton);

        self.subSideMenu = UIView(frame: CGRect(x: -(sideMenuWidht), y: 0, width: sideMenuWidht, height: screenHeight))
        self.subSideMenu.backgroundColor = UIColor.clear;
        self.sideMenuObj.view.frame = CGRect(x: 0, y: 0, width: self.subSideMenu.frame.size.width, height: screenHeight);
        self.subSideMenu.addSubview(self.sideMenuObj.view);
        self.sideMenu.addSubview(self.subSideMenu);
    }

    func showSideMenu() {
        self.sideMenu.isHidden = false;
        self.overlayButton.alpha = 0.0;

        let notificationIdentifier: String = "refreshSideMenuNotification"
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificationIdentifier), object: nil)

        UIView.animate(withDuration: 0.3) {
            var sideMenuFrame: CGRect = self.subSideMenu.frame
            sideMenuFrame.origin.x = 0
            self.subSideMenu.frame = sideMenuFrame
            self.overlayButton.alpha = 1.0
        }
    }

    @objc func doSingleTap() {
        self.overlayButton.alpha = 1.0;
        UIView.animate(withDuration: 0.3, animations: {
            var sideMenuFrame: CGRect = self.subSideMenu.frame
            sideMenuFrame.origin.x = -(self.sideMenuWidht)
            self.subSideMenu.frame = sideMenuFrame
            self.overlayButton.alpha = 0.0
        }) { (completion) in
            self.sideMenu.isHidden = true;
        }
    }
    func showLoader() {
        //IHProgressHUD.set(defaultStyle: .dark)
       // IHProgressHUD.show()

    }
    func hideLoader() {
       //IHProgressHUD.dismiss()

    }
    func apiCallDashboard() {
        if (Util.isInternetAvailable()) {
               SharedAppDelegate.showLoader()

            let parameter: [String: Any] = ["user_id": Util.sharedInstance.user_id, "user_unique_id": Util.sharedInstance.user_unique_id, "language": Util.sharedInstance.user_language];

            ServerAPIs.postRequestWithoutToken(apiUrl: apiDashboardAndSidemenu, parameter, completion: { (response, error, statusCode) in
                  SharedAppDelegate.hideLoader()

                if response["success"].intValue == 1 {
                    if let tempData: NSDictionary = response["data"].object as? NSDictionary {
                        print(tempData)
                        if let banners = tempData["banners"] as? [String] {
                            Util.sharedInstance.bannersArr = banners
                        }
                        //is_subscribed
                        if let is_subscribed = tempData["is_subscribed"] as? Int {
                        print(is_subscribed)
                            Util.sharedInstance.is_subscribed = is_subscribed

                        }
                        if let subscription_expiry = tempData["subscription_expiry"] as? String {

                            Util.sharedInstance.subscription_expiry = subscription_expiry

                        }
                        if let cms_links = tempData["cms_links"] as? NSDictionary {
                            if let about_us = cms_links["about_us"] as? String {
                                Util.sharedInstance.about_us = "\(about_us)"
                            }
                            if let contact_us = cms_links["contact_us"] as? String {
                                Util.sharedInstance.contact_us = "\(contact_us)"
                            }
                            if let sidebar_shop_link = cms_links["sidebar_shop_link"] as? String {
                                Util.sharedInstance.sidebar_shop_link = "\(sidebar_shop_link)"
                            }
                            if let share_app_ios_link = cms_links["share_app_ios_link"] as? String {
                                Util.sharedInstance.share_app_ios_link = "\(share_app_ios_link)"
                            }
                            if let product_link = cms_links["product_link"] as? String {
                                Util.sharedInstance.product_link = "\(product_link)"
                            }
                            if let share_app_android_link = cms_links["share_app_android_link"] as? String {
                                Util.sharedInstance.share_app_android_link = "\(share_app_android_link)"
                            }

                        }
                        if let ios_titles = tempData["ios_titles"] as? NSDictionary {
                            if let postlist = ios_titles["postlist"] as? String {
                                Util.sharedInstance.postlist = "\(postlist)"
                            }
                            if let add_post = ios_titles["add_post"] as? String {
                                Util.sharedInstance.add_post = "\(add_post)"
                            }
                            if let donation = ios_titles["donation"] as? String {
                                Util.sharedInstance.donation = "\(donation)"
                            }
                            if let donate_already = ios_titles["donate_already"] as? String {
                                Util.sharedInstance.donate_already = "\(donate_already)"
                            }
                            if let donate_amount_message = ios_titles["donate_amount_message"] as? String {
                                Util.sharedInstance.donate_amount_message = "\(donate_amount_message)"
                            }
                            if let donate = ios_titles["donate"] as? String {
                                Util.sharedInstance.donate = "\(donate)"
                            }

                        }
                        /*
                          "ios_titles": {
                                    "postlist": "Posts",
                                    "add_post": "Add Post",
                                    "donation": "Donation",
                                    "donate": "Donate",
                                    "donate_already": "-",
                                    "donate_amount_message": "-"
                                }
                         }
                         */
                        if let paid = tempData["advertisement_amount"] as? Int {
                            Util.sharedInstance.advertisement_amount = "\(paid)"
                        }
                        if let paid = tempData["subscription_amount"] as? Int {
                            Util.sharedInstance.subscription_amount = "\(paid)"
                        }
                        if let menus = tempData["menus"] as? NSDictionary {
                            if let paid = menus["paid"] as? [NSDictionary] {
                                Util.sharedInstance.menusPaid = paid
                            }
                            if let paid = menus["free"] as? [NSDictionary] {
                                Util.sharedInstance.menusFree = paid
                            }

                        }
                    }

                } else {
                    Toast(text: response["message"].stringValue).show()


                }
            })

        } else {
            Toast(text: Util.localized("InternetError")).show()
        }
    }




    // MARK: - Push Notification
    func registerRemoteNoti() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { (granted, error) in
            print("granted: (\(granted)")
        }

        let aappObje = UIApplication.shared
        // iOS 10 support
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self

            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: { _, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            aappObje.registerUserNotificationSettings(settings)
        }
        aappObje.registerForRemoteNotifications()

        let token = Messaging.messaging().fcmToken
        print("FCM token: \(token ?? "")")
        if (token != nil) {
            Util.sharedInstance.FCMToken = token!
        }
    }

    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        Util.sharedInstance.FCMToken = fcmToken
    }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {

        let deviceTokenString = deviceToken.reduce("", { $0 + String(format: "%02X", $1) })
        Util.sharedInstance.deviceToken = deviceTokenString;
        print("iOS Device Token: \(deviceTokenString)")
        Messaging.messaging().apnsToken = deviceToken

    }
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print(error.localizedDescription);
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print("userInfo : \(userInfo)")
        self.handleRemoteNotification(withUserInfo: userInfo);
    }

    func application(_ application: UIApplication, handleActionWithIdentifier identifier: String?, forRemoteNotification userInfo: [AnyHashable: Any], withResponseInfo responseInfo: [AnyHashable: Any], completionHandler: @escaping () -> Swift.Void) {

        // AudioServicesPlaySystemSound(1315)
        print("userInfo 7: \(userInfo)")
        completionHandler()
    }

    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (_ options: UNNotificationPresentationOptions) -> Void) {
        print("while Forground")
        let userInfo = notification.request.content.userInfo

        print(notification.request.content.userInfo)
        //handke

        self.receiveNotificationWhileForground(withUserInfo: notification.request.content.userInfo);
    }

    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("while Not active")
        self.handleRemoteNotification(withUserInfo: response.notification.request.content.userInfo);
        completionHandler()
    }

    func application(_ application: UIApplication,
        handleActionWithIdentifier identifier: String?,
        forRemoteNotification userInfo: [AnyHashable: Any],
        completionHandler: @escaping () -> Void) {
    }

    @objc func handleRemoteNotification(withUserInfo userInfo: [AnyHashable: Any]) {
        print("User info 3: \(userInfo)")
        let state: UIApplication.State = UIApplication.shared.applicationState
        if state == .background {
            print(userInfo)
            //Background
            guard let aps = userInfo["aps"] as? [String: AnyObject] else {
                return
            }
            /*
             [AnyHashable("aps"): {
                 alert =     {
                     body = test;
                     title = "New Post Added";
                 };
             }, AnyHashable("notification_type"): content, AnyHashable("gcm.message_id"): 1583559431251286, AnyHashable("notification_user_id"): 2, AnyHashable("notification_id"): 0, AnyHashable("google.c.sender.id"): 1081450138142, AnyHashable("google.c.a.e"): 1, AnyHashable("notification_body"): test, AnyHashable("content_ios"): http://todaystechnology.in/gir/app-user-content-view/13?language=gj, AnyHashable("notification_title"): New Post added, AnyHashable("content_url"): http://todaystechnology.in/gir/app-user-content-view/13?title=test&language=gj, AnyHashable("submenu_name"): test]
             */

            if let notification_type = userInfo["notification_type"] as? String {
                //notification_id
                if notification_type == "content" {
                    if let content_ios = userInfo["content_ios"] as? String {

                        if let submenu_name = userInfo["submenu_name"] as? String {
                            print(submenu_name)
                            let vc = Util.getStoryboard().instantiateViewController(withIdentifier: "MoreVC") as! MoreVC
                            vc.strTitle = submenu_name
                            vc.strUrl = "\(content_ios)"
                            SharedAppDelegate.navigationControl.pushViewController(vc, animated: true)

                        }
                    }
                }
            }





        }
        else if state == .inactive {
            print(userInfo)
            //Background
            guard let aps = userInfo["aps"] as? [String: AnyObject] else {
                return
            }

            if let notification_type = userInfo["notification_type"] as? String {
                if notification_type == "content" {
                    if let content_ios = userInfo["content_ios"] as? String {

                        if let submenu_name = userInfo["submenu_name"] as? String {
                            let vc = Util.getStoryboard().instantiateViewController(withIdentifier: "MoreVC") as! MoreVC
                            vc.strTitle = submenu_name
                            vc.strUrl = "\(content_ios)"
                            SharedAppDelegate.navigationControl.pushViewController(vc, animated: true)

                        }
                    }
                }
            }





        }

        //Handle for store notification
    }

    func receiveNotificationWhileForground(withUserInfo userInfo: [AnyHashable: Any]) {
        print(userInfo)

        guard let aps = userInfo["aps"] as? [String: AnyObject] else {
            return
        }

        if let alert = aps["alert"] as? NSDictionary {
            if let body = alert["body"] as? String {
                print(body)
                if let title = alert["title"] as? String {
                    print(title)
                    var style: GLNotificationStyle!
                    let colorStyle: GLNotificationColorType = .extraLight

                    style = .detailedBanner

                    let notificationBar = GLNotificationBar(title: title, message: body, preferredStyle: style) { (bool) in
                        print(userInfo)
                        if let notification_type = userInfo["notification_type"] as? String {

                            if notification_type == "content" {
                                if let content_ios = userInfo["content_ios"] as? String {

                                    if let submenu_name = userInfo["submenu_name"] as? String {
                                        let vc = Util.getStoryboard().instantiateViewController(withIdentifier: "MoreVC") as! MoreVC
                                        vc.strTitle = submenu_name
                                        vc.strUrl = "\(content_ios)"
                                        SharedAppDelegate.navigationControl.pushViewController(vc, animated: true)

                                    }
                                }
                            }
                            //notification_id

                        }
                    }
                    // Set visual effectview style.
                    notificationBar.setColorStyle(colorStyle)
                    notificationBar.setShadow(true)



                }
            }

        }



        //Handle for store notification

    }


}

