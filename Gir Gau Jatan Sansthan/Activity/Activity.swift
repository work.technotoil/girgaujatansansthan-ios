//
//  Activity.swift
//  Gir Gau Jatan Sansthan
//
//  Created by mac on 16/01/21.
//  Copyright © 2021 Miral Gondaliya. All rights reserved.
//

import UIKit

class ActivityIndicatorVC: NSObject{

    var activityView: UIActivityIndicatorView?
    var view: UIView?

    static let sharedInstance:ActivityIndicatorVC = {
        let instance = ActivityIndicatorVC ()
        return instance
    } ()

    // MARK: Init
    override init() {
        print("")
        // initialized with variable or property
    }

    func startIndicator() {
        makeVisible()
        activityView?.startAnimating()

    }

    func stopIndicator() {
        let delegate = (UIApplication.shared.delegate as? AppDelegate)
        let view =   delegate?.window?.viewWithTag(9999)
        view?.removeFromSuperview()


        view?.removeFromSuperview()
        activityView?.stopAnimating()
    }


    func makeVisible() {
        let delegate = (UIApplication.shared.delegate as? AppDelegate)
        let view = UIView(frame: UIScreen.main.bounds)
        view.tag = 9999
       let activityView = UIActivityIndicatorView(style: .whiteLarge)
      //  let activityView = UIActivityIndicatorView(style: .medium)
        activityView.center = view.center
        activityView.color = .white
        view.addSubview(activityView)
        view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.4)
        
        delegate?.window?.addSubview(view)
        activityView.startAnimating()
    }

}
