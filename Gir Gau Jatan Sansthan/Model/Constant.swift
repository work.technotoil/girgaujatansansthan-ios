//  Constant.swift

import Foundation
import UIKit

let screenHeight = UIScreen.main.bounds.size.height
let screenWidth =  UIScreen.main.bounds.size.width

let IS_IPAD = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad
let IS_IPHONE = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.phone
let IS_IPHONE_5 = IS_IPHONE && UIScreen.main.bounds.size.height == 568.0
let IS_IPHONE_6 = IS_IPHONE && UIScreen.main.bounds.size.height == 667.0
let IS_IPHONE_6_PLUS = IS_IPHONE && UIScreen.main.bounds.size.height == 736.0
let IS_IPHONE_4 = IS_IPHONE && UIScreen.main.bounds.size.height == 460.0
let IS_IPHONE_X = IS_IPHONE && (UIScreen.main.bounds.size.height == 812.0 || UIScreen.main.bounds.size.height == 896.0)

let baseUrl: String =  "http://vedicgopalan.in/gaupalan/"            //"http://www.restrodies.com/gir_cowcare/"//
//let baseURL:String =  "http://vedicgopalan.in/gaupalan/"
//let baseUrl: String = "http://b2b.todaystechnology.in/"
//http://b2b.todaystechnology.in
let about = ""
let ToDoTable = "ProductTable" //Date
let CategoryTable = "CategoryTable" //Date
let UserTable = "UserTable" //Date
let ItemTable = "ItemTable" //Date
let ItemName = "ItemName" //Date
let BookedHistoryTable = "BookedHistoryTable" //Date
let appGrayColor = UIColor.returnRGBColor(r: 197, g: 199, b: 202, alpha: 1.0)
//49AE44
let appBlueColor = UIColor.returnRGBColor(r: 226, g: 109, b: 121, alpha: 1.0)
let appColour = UIColor.returnRGBColor(r: 73, g: 174, b: 68, alpha: 1.0)
let appUrl : String = "https://inlancer.in"
