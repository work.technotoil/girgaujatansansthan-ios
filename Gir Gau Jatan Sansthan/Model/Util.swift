//
//  Util.swift

import UIKit
import Foundation
import SystemConfiguration

class Util: NSObject {

    private override init() { }
    static let sharedInstance: Util = Util()

    var deviceToken: String = "ADFLdfdjf54654SS"
    var FCMToken: String = "FCMSimulatorToken"
    var deviceType: String = "1"
    //User Details Viral Niaga
    var storyBoardName: String = "Main"
    var storyBoardNameIPad: String = "Main"
    var email: String = ""
    var customer_id: String = ""
    var selectedIndex = 0
  /*
     "ios_titles": {
               "postlist": "Posts",
               "add_post": "Add Post",
               "donation": "Donation",
               "donate": "Donate",
               "donate_already": "-",
               "donate_amount_message": "-"
           }
    }
    */
    var postlist: String = ""
    var add_post: String = ""
    var donation: String = ""
    var donate: String = ""
    var Subscribe: String = ""

    var donate_already: String = ""
    var donate_amount_message: String = ""

    
    var appStatus: String = "0"

    
    var about_us: String = ""
    var shop: String = ""
    var contact_us: String = ""
    var content_Id:String = "" 
    
    var sidebar_shop_link: String = ""
    var share_app_ios_link: String = ""
    var product_link: String = ""
    var share_app_android_link: String = ""

    //sidebar_shop_link
    //share_app_ios_link
    //product_link
    //share_app_android_link
    
    var advertisement_amount: String = "200"
    var subscription_amount: String = "200"
    var user_id: String = ""

    var is_subscribed: Int = 0
    var user_unique_id: String = "1"
    var user_name: String = ""
    var user_language: String = ""
    var user_city: String = ""
    var user_phone: String = ""
    var user_subscription: String = ""
    var user_subscription_details: String = ""
    var user_status: String = ""
    var subscription_expiry = ""
    var bannersArr = [String]()
    var menusPaid = [NSDictionary]()
    var menusFree = [NSDictionary]()

    /*

    {
         "success": 1,
         "message": "Login Successfully",
         "data": {
             "user_id": 12,
             "user_unique_id": null,
             "user_name": "Pradip Dobariya",
             "user_language": "hi",
             "user_city": "rajkot",
             "user_password": null,
             "user_sweets": null,
             "user_phone": 9824845095,
             "user_email": null,
             "user_otp_status": 0,
             "user_otp": null,
             "user_fcm": null,
             "user_address": null,
             "user_details": null,
             "user_phone_verified": 0,
             "user_subscription": 0,
             "user_subscription_details": null,
             "user_status": 1,
             "is_delete": 0,
             "created_at": "2020-03-04 12:40:03",
             "updated_at": null
         }
     }
     
     */

    var profile_image: String = ""

    //Sorting And Filter
    var sorting: String = "0"
    var maxPrice: String = ""
    var minPrice: String = ""
    var categoryId: String = ""
    var arrBrandID = [String]()
    var arrCartId = [String]()


    var is_approved: String = ""
    var appLogoUrl: String = ""

    var selectedLangugae = 0
    var strSelectedLanguage = "en"
    class func isStringNull(_ srcString: String) -> Bool {
        if srcString != "" && srcString != "null" && !(srcString == "<null>") && !(srcString == "(null)") && (srcString.count) > 0 {
            return false
        }
        return true
    }
    class func getStoryboard() -> UIStoryboard {
        if IS_IPAD {
            return UIStoryboard(name: Util.sharedInstance.storyBoardNameIPad, bundle: nil)
        } else {
            return UIStoryboard(name: Util.sharedInstance.storyBoardName, bundle: nil)
        }

    }

    class func getDirectoryPath() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    class func getUserDefaultValue(key: String) -> String {
        let SharedPref = UserDefaults.standard
        var strValue = ""
        if let keyValue = SharedPref.value(forKey: key) as? String {
            strValue = keyValue
        }
        return strValue
    }
    class func getUserDefaultValueAny(key: String) -> [NSDictionary] {
        let SharedPref = UserDefaults.standard
        let dic = [NSDictionary]()
        if let keyValue = SharedPref.value(forKey: key) as? [NSDictionary] {
            return keyValue
        }

        return dic
    }
    class func setUserDefaultValue(key: String, value: Any) {
        UserDefaults.standard.setValue(value, forKey: key)
        UserDefaults.standard.synchronize()
    }
    class func isLogin() -> Bool {
        if (UserDefaults.standard.value(forKey: "user_id") != nil) {
            return true
        }
        return false
    }
    class func validatePhoneNumber(enteredPhonenumber: String) -> Bool {
        let phonenumberFormat = "^[0-9]{10}$"
        let phonePredicate = NSPredicate(format: "SELF MATCHES %@", phonenumberFormat)
        return phonePredicate.evaluate(with: enteredPhonenumber)
    }
    class func validateEmail(enteredEmail: String) -> Bool {

        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        let emailPredicate = NSPredicate(format: "SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: enteredEmail)
    }
    class func localized(_ srcMsg: String) -> String {
        //return String.localizedStringWithFormat(NSLocalizedString(srcMsg, comment: ""));
        return SharedAppDelegate.localization.localizedString(forKey: srcMsg)
    }
    class func isInternetAvailable() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)

        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) { zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }

        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    class func resetDefault() {
        let prefs = UserDefaults.standard



        Util.sharedInstance.user_id = ""
        prefs.removeObject(forKey: "user_id")


        prefs.synchronize();
    }

    class func setUserDetails() {
        /*var user_id : String = ""
           var user_unique_id : String = ""
           var user_name : String = ""
           var user_language : String = ""
           var user_city : String = ""
           var user_phone : String = ""
           var user_subscription : String = ""
           var user_subscription_details : String = ""
           var user_status : String = ""*/

        if let abc = SharedAppDelegate.dataUser["user_id"] as? Int {
            Util.sharedInstance.user_id = "\(abc)"
            Util.setUserDefaultValue(key: "user_id", value: "\(abc)")

        }
        Util.sharedInstance.user_unique_id = "1"
        if let abc = SharedAppDelegate.dataUser["user_unique_id"] as? String {
            Util.sharedInstance.user_unique_id = "\(abc)"
            Util.setUserDefaultValue(key: "user_unique_id", value: "\(abc)")

        } else if let abc = SharedAppDelegate.dataUser["user_unique_id"] as? Int {
            Util.sharedInstance.user_unique_id = "\(abc)"
            Util.setUserDefaultValue(key: "user_unique_id", value: "\(abc)")

        }

        if let abc = SharedAppDelegate.dataUser["user_name"] as? String {
            Util.sharedInstance.user_name = "\(abc)"
            Util.setUserDefaultValue(key: "user_name", value: "\(abc)")

        }

        if let abc = SharedAppDelegate.dataUser["user_language"] as? String {
            Util.sharedInstance.user_language = "\(abc)"
            Util.setUserDefaultValue(key: "user_language", value: "\(abc)")

        }
        if let abc = SharedAppDelegate.dataUser["user_city"] as? String {
            Util.sharedInstance.user_city = "\(abc)"
            Util.setUserDefaultValue(key: "user_city", value: "\(abc)")

        }
        if let abc = SharedAppDelegate.dataUser["user_phone"] as? String {
            Util.sharedInstance.user_phone = "\(abc)"
            Util.setUserDefaultValue(key: "user_phone", value: "\(abc)")

        } else if let abc = SharedAppDelegate.dataUser["user_phone"] as? Int {
            Util.sharedInstance.user_phone = "\(abc)"
            Util.setUserDefaultValue(key: "user_phone", value: "\(abc)")

        }
        
        if let abc = SharedAppDelegate.dataUser["country_code"] as? String {
            Util.sharedInstance.user_phone = "\(abc)"
            Util.setUserDefaultValue(key: "country_code", value: "\(abc)")

        } else if let abc = SharedAppDelegate.dataUser["country_code"] as? Int {
            Util.sharedInstance.user_phone = "\(abc)"
            Util.setUserDefaultValue(key: "country_code", value: "\(abc)")

        }

        if let abc = SharedAppDelegate.dataUser["user_subscription"] as? String {
            Util.sharedInstance.user_subscription = "\(abc)"
            Util.setUserDefaultValue(key: "user_subscription", value: "\(abc)")

        } else if let abc = SharedAppDelegate.dataUser["user_subscription"] as? Int {
            Util.sharedInstance.user_subscription = "\(abc)"
            Util.setUserDefaultValue(key: "user_subscription", value: "\(abc)")

        }
        
        if let abc = SharedAppDelegate.dataUser["user_status"] as? String {
            Util.sharedInstance.user_status = "\(abc)"
            Util.setUserDefaultValue(key: "user_status", value: "\(abc)")

        } else if let abc = SharedAppDelegate.dataUser["user_status"] as? Int {
            Util.sharedInstance.user_status = "\(abc)"
            Util.setUserDefaultValue(key: "user_status", value: "\(abc)")

        }

        if let abc = SharedAppDelegate.dataUser["user_subscription_details"] as? String {
            Util.sharedInstance.user_subscription_details = "\(abc)"
            Util.setUserDefaultValue(key: "user_subscription_details", value: "\(abc)")

        }

        if UserDefaults.standard.string(forKey: "user_language") != nil {
            if UserDefaults.standard.string(forKey: "user_language") == "en" {
                SharedAppDelegate.localization.setLanguage("EN")

            } else if UserDefaults.standard.string(forKey: "user_language") == "hi" {
                SharedAppDelegate.localization.setLanguage("HI")

            } else if UserDefaults.standard.string(forKey: "user_language") == "gj" {
                SharedAppDelegate.localization.setLanguage("GJ")

            }

        } else {
            Util.setUserDefaultValue(key: "user_language", value: "en")
            SharedAppDelegate.localization.setLanguage(UserDefaults.standard.string(forKey: "user_language"))
        }
    }

}

extension UIView {
    func addConstraintsWithFormat(format: String, views: UIView...) {

        var viewsDictionary = [String: UIView]()
        for (index, view) in views.enumerated() {
            let key = "v\(index)"
            view.translatesAutoresizingMaskIntoConstraints = false
            viewsDictionary[key] = view
        }
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: viewsDictionary))
    }

    func addConstraintsWithFormatRight(format: String, views: UIView...) {

        var viewsDictionary = [String: UIView]()
        for (index, view) in views.enumerated() {
            let key = "v\(index)"
            view.translatesAutoresizingMaskIntoConstraints = false
            viewsDictionary[key] = view
        }
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: [], metrics: nil, views: viewsDictionary))
    }
    func addShadow() {
        self.layer.cornerRadius = 3
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.layer.shadowRadius = 2
        self.layer.shadowOpacity = 0.7
    }
    func addShadowButton() {
        //  self.layer.cornerRadius = self.frame.height / 2
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.layer.shadowRadius = 10
        self.layer.shadowOpacity = 0.7
    }
}

extension UIColor {
    static func returnRGBColor(r: CGFloat, g: CGFloat, b: CGFloat, alpha: CGFloat) -> UIColor {
        return UIColor(red: r / 255, green: g / 255, blue: b / 255, alpha: alpha)
    }
}
extension String {
    func randomStringWithLength (len: Int) -> NSString {

        let letters: NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

        var randomString: NSMutableString = NSMutableString(capacity: len)

        for i in 0..<len {
            var length = UInt32 (letters.length)
            var rand = arc4random_uniform(length)
            randomString.appendFormat("%C", letters.character(at: Int(rand)))
        }
        return randomString
    }
}
