//
//  ServerAPIs.swift

import UIKit
import Alamofire
import SwiftyJSON
let apiRegister: String = "\(baseUrl)app-user-verify-otp"
let apiLogin: String = "\(baseUrl)app-user-authentication"
let apiDashboardAndSidemenu: String = "\(baseUrl)app-user-dashboard"
let apiSubMenu: String = "\(baseUrl)app-user-submenus"
let apiAdList: String = "\(baseUrl)app-user-ad-list"
let apiLogout: String = "\(baseUrl)app-user-logout"
let apiChangeLanguage: String = "\(baseUrl)app-user-change-language"
let apiAdPost: String = "\(baseUrl)app-user-save-ad"
let apiTransactionInit: String = "\(baseUrl)app-user-transaction-init"
let apiTransactionStatus: String = "\(baseUrl)app-user-transaction-done"
let apiContentList: String = "\(baseUrl)app-user-contents"
let apiSearch: String = "\(baseUrl)app-user-search-content"
let apiConnect: String = "\(baseUrl)app-user-category"
let apiStateList: String = "\(baseUrl)app-user-state-list"
let apiCityList: String = "\(baseUrl)app-user-city-list?state_id="
let apiConnectDetail: String = "\(baseUrl)app-user-connection"
let apiSubmenuDetail:String = "\(baseUrl)app-user-submenus"
let apiCeckSubscribe:String = "\(baseUrl)app-user-check-subscribe"
let apiblogReport:String = "\(baseUrl)reportBlogs"
let appUserBlock:String = "\(baseUrl)app-user-block"


//https://www.getpostman.com/collections/4cb4a18678e5fd8d3155
//app-user-search-content
//app-user-change-languageuseruser
class ServerAPIs: NSObject {
    
    class func postRequestWithoutToken(apiUrl:String, _ parameter: [String: Any], completion: @escaping (_ response: JSON, _ error: NSError?, _ statusCode: Int)-> ()){
        
        Alamofire.request(apiUrl, method: .post, parameters: parameter).responseJSON { (response) -> Void in
            
            if (response.result.error == nil) {
                print(response.result);
                
                if (response.result.value != nil && ((response.response?.statusCode)! < 300)){
                    let dataLog = try! JSON(data: response.data!)
                    print(dataLog);
                    completion(dataLog,nil,(response.response?.statusCode)!)
                }else{
                    completion(JSON.null, nil,(response.response?.statusCode)!)
                }
            }else{
                if (response.response != nil){
                    completion(JSON.null,nil,(response.response?.statusCode)!)
                }else{
                    completion(JSON.null,nil,0)
                }
            }
        }
    }
    
  class  func requestPOSTwithoutToken(strURL : String, params : Parameters?, successBlock:@escaping (NSDictionary) -> Void, failure:@escaping (Error) -> Void)
    {
        print(strURL)
        print(params ?? "")
        Alamofire.request(strURL, method:.post, parameters:params, headers:nil).responseJSON { response in
             switch response.result
             {
                case .success:
                    print(response)
                    
                    let res  :  NSDictionary = response.value as! NSDictionary
                    successBlock(res)
                   
                case .failure(let error):
                    print(error)
                    failure(error)
            }
        }
    }
    
    
    class func getRequestWithoutToken(apiUrl:String, completion: @escaping (_ response: JSON, _ error: NSError?, _ statusCode: Int)-> ()){
        
        Alamofire.request(apiUrl, method: .get).responseJSON { (response) -> Void in
            
            if (response.result.error == nil) {
                print(response.result);
                
                if (response.result.value != nil && ((response.response?.statusCode)! < 300)){
                    let dataLog = try! JSON(data: response.data!)
                    print(dataLog);
                    completion(dataLog,nil,(response.response?.statusCode)!)
                }else{
                    completion(JSON.null, nil,(response.response?.statusCode)!)
                }
            }else{
                if (response.response != nil){
                    completion(JSON.null,nil,(response.response?.statusCode)!)
                }else{
                    completion(JSON.null,nil,0)
                }
            }
        }
    }

    
    class func postRequestWithHeader(apiUrl:String, _ parameter: [String: Any], completion: @escaping (_ response: JSON, _ error: NSError?, _ statusCode: Int)-> ()){
        
        let headers: HTTPHeaders = [
            "usertoken": Util.getUserDefaultValue(key: "user_token"),
            ]
        
        Alamofire.request(apiUrl, method: .post, parameters: parameter,encoding: JSONEncoding.default,headers: headers).responseJSON { (response) -> Void in
            
            if (response.result.error == nil) {
                
                print(response.result);
                if (response.result.value != nil && ((response.response?.statusCode)! < 300)){
                    let dataLog = try! JSON(data: response.data!)
                    print(dataLog);
                    completion(dataLog,nil,(response.response?.statusCode)!)
                }else{
                    completion(JSON.null, nil,(response.response?.statusCode)!)
                }
                
            }else{
                if (response.response != nil){
                    completion(JSON.null,nil,(response.response?.statusCode)!)
                }else{
                    completion(JSON.null,nil,0)
                }
            }
        }
    }
    
    class func postRequest(apiUrl:String, _ parameter: [String: Any], completion: @escaping (_ response: JSON, _ error: NSError?)-> ()){
        
        Alamofire.request(apiUrl, method: .post, parameters: parameter, encoding: JSONEncoding.default).responseJSON { (response) -> Void in
            
            if (response.result.error == nil) {
                print(response.result);
                
                let dataLog = try! JSON(data: response.data!)
                print(dataLog);
                
                completion(dataLog,nil)
                
            }else{
                completion(JSON.null,nil)
            }
        }
    }
    class func postMultipartRequestWithVideo (apiUrl: String,_ parameter: [String: Any], imageParameterName: String,imageData: [Data?], videoData : Data?,video_mimeType : String, completion: @escaping (_ response: JSON, _ error: NSError?, _ statusCode: Int)-> ()) {
          
          Alamofire.upload(multipartFormData: { (multipartFormData) in

            for imageData in imageData {
                multipartFormData.append(imageData!, withName: "\(imageParameterName)[]", fileName: "\(Date().timeIntervalSince1970).jpeg", mimeType: "image/jpeg")
            }
          
              
            if video_mimeType != ""{
                multipartFormData.append(videoData!, withName: "item_video", fileName: "swift_file_video.mp4", mimeType: "application/octet-stream");

            }
              for (key, value) in parameter {
                  multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
              }
          }, to:apiUrl, method:.post){ (result) in
              
              switch result {
              case .success(let upload, _, _):
                  
                  upload.uploadProgress(closure: { (Progress) in
                      
                      print("Upload Progress: \(Progress.fractionCompleted)")
                  })
                  upload.responseJSON { response in
                      
                      if let JSON = response.result.value {
                          
                          print("JSON: \(JSON)")
                      }
                      if (response.result.error == nil) {
                          
                          let dataLog = try! JSON(data: response.data!)
                          print(dataLog);
                          completion(dataLog,nil,(response.response?.statusCode)!)
                          
                      }else{
                          completion(JSON.null,nil,(response.response?.statusCode)!)
                      }
                  }
                  
              case .failure(let encodingError):
                  
                  print(encodingError)
              }
          }
      }
    
    class func postMultipartRequestWithOptionData (apiUrl: String,_ parameter: [String: Any], imageParameterName: String,imageData: Data?, mimeType: String, fileName: String, completion: @escaping (_ response: JSON, _ error: NSError?, _ statusCode: Int)-> ()) {
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
//            if (imageData?.bytes ?? 0 != 0){
//            multipartFormData.append(imageData!, withName: imageParameterName, fileName: "swift_file.jpeg", mimeType: "image/jpeg");
//
//            /*if imageData != nil {
//
//                multipartFormData.append(imageData!, withName: imageParameterName, fileName: fileName, mimeType: mimeType);
//            }*/
//
//            }
            if mimeType != ""{
                multipartFormData.append(imageData!, withName: imageParameterName, fileName: "swift_file.jpeg", mimeType: "image/jpeg");

            }
            for (key, value) in parameter {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to:apiUrl, method:.post){ (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (Progress) in
                    
                    print("Upload Progress: \(Progress.fractionCompleted)")
                })
                upload.responseJSON { response in
                    
                    if let JSON = response.result.value {
                        
                        print("JSON: \(JSON)")
                    }
                    if (response.result.error == nil) {
                        
                        let dataLog = try! JSON(data: response.data!)
                        print(dataLog);
                        completion(dataLog,nil,(response.response?.statusCode)!)
                        
                    }else{
                        completion(JSON.null,nil,(response.response?.statusCode)!)
                    }
                }
                
            case .failure(let encodingError):
                
                print(encodingError)
            }
        }
    }
    
    class func postMultipartRequest (apiUrl: String,_ parameter: [String: Any], imageParameterName: String,imageData: Data, mimeType: String, fileName: String, completion: @escaping (_ response: JSON, _ error: NSError?, _ statusCode: Int)-> ()) {
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            //if (imageData.bytes.count != 0){
            //multipartFormData.append(imageData, withName: imageParameterName, fileName: "swift_file.jpeg", mimeType: "image/jpeg");
            if fileName != ""{
                multipartFormData.append(imageData, withName: imageParameterName, fileName: fileName, mimeType: mimeType);

            }

            //}
            for (key, value) in parameter {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to:apiUrl, method:.post){ (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (Progress) in
                    
                    print("Upload Progress: \(Progress.fractionCompleted)")
                })
                upload.responseJSON { response in
                    
                    if let JSON = response.result.value {
                        
                        print("JSON: \(JSON)")
                    }
                    if (response.result.error == nil) {
                        
                        let dataLog = try! JSON(data: response.data!)
                        print(dataLog);
                        completion(dataLog,nil,(response.response?.statusCode)!)
                        
                    }else{
                        completion(JSON.null,nil,(response.response?.statusCode)!)
                    }
                }
                
            case .failure(let encodingError):
                
                print(encodingError)
            }
        }
    }
    class func postMultiPartDoc (apiUrl: String,_ parameter: [String: Any], imageParameterName: String,fileUrl: URL, mimeType: String, fileName: String, completion: @escaping (_ response: JSON, _ error: NSError?, _ statusCode: Int)-> ()) {
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            //if (imageData.bytes.count != 0){
            //multipartFormData.append(imageData, withName: imageParameterName, fileName: "swift_file.jpeg", mimeType: "image/jpeg");
            //multipartFormData.append(imageData, withName: imageParameterName, fileName: fileName, mimeType: mimeType);
            multipartFormData.append(fileUrl, withName: imageParameterName);
            //}
            for (key, value) in parameter {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to:apiUrl, method:.post){ (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (Progress) in
                    
                    print("Upload Progress: \(Progress.fractionCompleted)")
                })
                upload.responseJSON { response in
                    
                    if let JSON = response.result.value {
                        
                        print("JSON: \(JSON)")
                    }
                    if (response.result.error == nil) {
                        
                        let dataLog = try! JSON(data: response.data!)
                        print(dataLog);
                        completion(dataLog,nil,(response.response?.statusCode)!)
                        
                    }else{
                        completion(JSON.null,nil,(response.response?.statusCode)!)
                    }
                }
                
            case .failure(let encodingError):
                
                print(encodingError)
            }
        }
    }
    
    class func postMultipartRequestWithMultiImage (apiUrl: String,_ parameter: [String: Any],imageData1: Data, imageData2: Data,imageData3: Data,mimeType: String, fileName: String,fileName2: String,fileName3: String, completion: @escaping (_ response: JSON, _ error: NSError?, _ statusCode: Int)-> ()) {
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            //if (imageData.bytes.count != 0){
            //multipartFormData.append(imageData, withName: imageParameterName, fileName: "swift_file.jpeg", mimeType: "image/jpeg");
            if !(Util.isStringNull(fileName)){
                 multipartFormData.append(imageData1, withName: "photo1", fileName: "swift_file1.jpeg", mimeType: mimeType);
                
            }
           
            if !(Util.isStringNull(fileName2)){
                multipartFormData.append(imageData2, withName: "photo2", fileName: "swift_file2.jpeg", mimeType: mimeType);

            }
            if !Util.isStringNull(fileName3){
                multipartFormData.append(imageData3, withName: "photo3", fileName: "swift_file3.jpeg", mimeType: mimeType);

            }

            
            //}
            for (key, value) in parameter {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to:apiUrl, method:.post){ (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (Progress) in
                    
                    print("Upload Progress: \(Progress.fractionCompleted)")
                })
                upload.responseJSON { response in
                    
                    if let JSON = response.result.value {
                        
                        print("JSON: \(JSON)")
                    }
                    if (response.result.error == nil) {
                        
                        let dataLog = try! JSON(data: response.data!)
                        print(dataLog);
                        completion(dataLog,nil,(response.response?.statusCode)!)
                        
                    }else{
                        completion(JSON.null,nil,(response.response?.statusCode)!)
                    }
                }
                
            case .failure(let encodingError):
                
                print(encodingError)
            }
        }
    }
    class func postMultipartRequestWithHeader (apiUrl: String,_ parameter: [String: Any], imageParameterName: String,imageData: Data, completion: @escaping (_ response: JSON, _ error: NSError?, _ statusCode: Int)-> ()) {
        
        let header: HTTPHeaders = [
            "usertoken": Util.getUserDefaultValue(key: "user_token"),
            ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            //if (imageData.bytes.count != 0){
            multipartFormData.append(imageData, withName: imageParameterName, fileName: "swift_file.jpeg", mimeType: "image/jpeg");
            //}
            for (key, value) in parameter {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to:apiUrl, method:.post,headers :header){ (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (Progress) in
                    
                    print("Upload Progress: \(Progress.fractionCompleted)")
                })
                upload.responseJSON { response in
                    
                    if let JSON = response.result.value {
                        
                        print("JSON: \(JSON)")
                    }
                    if (response.result.error == nil) {
                        
                        let dataLog = try! JSON(data: response.data!)
                        print(dataLog);
                        completion(dataLog,nil,(response.response?.statusCode)!)
                        
                    }else{
                        completion(JSON.null,nil,(response.response?.statusCode)!)
                    }
                }
                
            case .failure(let encodingError):
                
                print(encodingError)
            }
        }
    }
    
    class func getRequestWithHeader(apiUrl:String, completion: @escaping (_ response: JSON, _ error: NSError?, _ statusCode: Int) -> ()){
        
       
        let headers: HTTPHeaders = [
            "usertoken": Util.getUserDefaultValue(key: "user_token"),
            ]
        
        
        Alamofire.request(apiUrl, method: .get,encoding: JSONEncoding.default, headers: headers).responseJSON { (response) -> Void in
            
            if (response.result.error == nil) {
                
                print(response.result);
                
                if response.data?.count == 0 {
                    completion(JSON.null,nil,(response.response?.statusCode)!)
                }else{
                    let dataLog = try! JSON(data: response.data!)
                    print(dataLog);
                    completion(dataLog,nil,(response.response?.statusCode)!)
                }
                
            }else{
                if (response.response != nil){
                    completion(JSON.null,nil,(response.response?.statusCode)!)
                }else{
                    completion(JSON.null,nil,0)
                }
            }
        }
    }
  class  func requestPOSTBeforeLoginURL(_ strURL : String, params : Parameters?, successBlock:@escaping (NSDictionary) -> Void, failure:@escaping (Error) -> Void)
    {
        print(strURL)
        print(params ?? "")
        Alamofire.request(strURL, method:.post, parameters:params, headers:nil).responseJSON { response in
             switch response.result
             {
                case .success:
                    print(response)
                    
                    let res  :  NSDictionary = response.value as! NSDictionary
                    successBlock(res)
                   
                case .failure(let error):
                    print(error)
                    failure(error)
            }
        }
    }
    //Gereral get request
    class func getRequest(apiUrl:String, completion: @escaping (_ response: JSON, _ error: NSError?, _ statusCode: Int)-> ()){
        
        Alamofire.request(apiUrl, method: .get).responseJSON { (response) -> Void in
            if (response.result.error == nil) {
                
                print(response.result);
                if response.data?.count == 0 {
                    completion(JSON.null,nil,(response.response?.statusCode)!)
                }else{
                    let dataLog = try! JSON(data: response.data!)
                    print(dataLog);
                    completion(dataLog,nil,(response.response?.statusCode)!)
                }
            }else{
                if (response.response != nil){
                    completion(JSON.null,nil,(response.response?.statusCode)!)
                }else{
                    completion(JSON.null,nil,0)
                }
            }
        }
    }
}
//class AFWrapper: NSObject {
//
//static let sharedInstance = AFWrapper()
//
//
//
//func requestPOSTBeforeLoginURL(_ strURL : String, params : Parameters?, successBlock:@escaping (NSDictionary) -> Void, failure:@escaping (Error) -> Void)
//{
//    print(strURL)
//    print(params ?? "")
//    Alamofire.request(strURL, method:.post, parameters:params, headers:nil).responseJSON { response in
//         switch response.result
//         {
//            case .success:
//                print(response)
//
//                let res  :  NSDictionary = response.value as! NSDictionary
//                successBlock(res)
//
//            case .failure(let error):
//                print(error)
//                failure(error)
//        }
//    }
//}
//}
