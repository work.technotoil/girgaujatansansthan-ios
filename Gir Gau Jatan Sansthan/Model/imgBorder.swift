//
//  imgBorder.swift
//  Vyaparam
//
//  Created by Miral Gondaliya on 19/10/19.
//  Copyright © 2019 Miral Gondaliya. All rights reserved.
//

import UIKit

class imgBorder: UIView {

    override func draw(_ rect: CGRect) {
        self.layer.cornerRadius = 1
        self.layer.borderColor = appGrayColor.cgColor
        self.layer.borderWidth = 1.0    }

}
