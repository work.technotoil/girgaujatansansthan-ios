//
//  AdCell.swift
//  Gir Gau Jatan Sansthan
//
//  Created by Miral Gondaliya on 05/03/20.
//  Copyright © 2020 Miral Gondaliya. All rights reserved.
//

import UIKit

class AdCell: UITableViewCell {

    @IBOutlet weak var imgSep: UIImageView!
    @IBOutlet weak var viewMain: UIView!
    
    @IBOutlet weak var btnCall: UIButton!
    @IBOutlet weak var lblDetails: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    override func awakeFromNib() {
        viewMain.layer.cornerRadius = 4
        viewMain.clipsToBounds = true
        viewMain.layer.borderColor = UIColor.lightGray.cgColor
        viewMain.layer.borderWidth = 0.4
        
        
        btnCall.layer.cornerRadius = 3.0
        btnCall.layer.borderColor = UIColor.lightGray.cgColor
        btnCall.layer.borderWidth = 1.0
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
