//
//  HomeCategoryCell.swift
//  Vyaparam
//
//  Created by Miral Gondaliya on 05/11/19.
//  Copyright © 2019 Miral Gondaliya. All rights reserved.
//

import UIKit

class HomeCategoryCell: UICollectionViewCell {

    @IBOutlet weak var lbl2: UILabel!
    @IBOutlet weak var lbl1: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var viewMain: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        img.layer.cornerRadius = 3.0
        viewMain.layer.cornerRadius = 4.0
        viewMain.layer.borderColor = appGrayColor.cgColor
        viewMain.layer.borderWidth = 0.7
        
        // Initialization code
    }

}
