//
//  ConnectDetailCell.swift
//  Gir Gau Jatan Sansthan
//
//  Created by RAJ ORIYA on 29/11/20.
//  Copyright © 2020 Miral Gondaliya. All rights reserved.
//

import UIKit

class ConnectDetailCell: UITableViewCell {
    
    @IBOutlet weak var img: UIImageView!
    
    @IBOutlet weak var lblMainName: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblState: UILabel!
    
    @IBOutlet weak var btnCall: UIButton!

    @IBOutlet weak var btn_report: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
