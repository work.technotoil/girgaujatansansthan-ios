//
//  ConnectCell.swift
//  Gir Gau Jatan Sansthan
//
//  Created by RAJ ORIYA on 28/11/20.
//  Copyright © 2020 Miral Gondaliya. All rights reserved.
//

import UIKit

class ConnectCell: UICollectionViewCell {
    
    @IBOutlet weak var img: UIImageView!
    
    @IBOutlet weak var lblName: UILabel!
    
}
