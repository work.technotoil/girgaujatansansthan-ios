//
//  SideMenuCell.swift
//  Viral Niaga
//
//  Created by miral on 15/01/19.
//  Copyright © 2019 APT Solution. All rights reserved.
//

import UIKit

class SideMenuCell: UITableViewCell {
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var viewMain: UIView!
    @IBOutlet var imgBg: UIImageView!
    @IBOutlet var img: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
