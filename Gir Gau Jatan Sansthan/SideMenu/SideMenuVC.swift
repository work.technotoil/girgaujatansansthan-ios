//
//  SideMenuVC.swift
//  Viral Niaga
//
//  Created by miral on 15/01/19.
//  Copyright © 2019 APT Solution. All rights reserved.
//

import UIKit
import EzPopup
import Kingfisher

class SideMenuVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet weak var view_SideMenu: UIView!
    @IBOutlet var imgProfile: UIImageView!
    @IBOutlet var imgRightArrow: UIImageView!
    @IBOutlet var btnChangeLanguage: UIButton!
    @IBOutlet var tblSideMenu: UITableView!
    var totWidth: CGFloat = 0
    var totHeight: CGFloat = 0
    var currentMode: String = "";
    let notificationName = Notification.Name("refreshSideMenuNotification")
    var gestureRecognizers = [UIGestureRecognizer]()
    
    var menuItems = [String]()
    var imgMenuItems = [String]()
    var menuItemsDetails = [NSDictionary]()
    var shopskip:String = "NotSubscribed"
    @IBOutlet var btnProfile: UIButton!
    @IBOutlet var lblEmail: UILabel!
    @IBOutlet var lblCity: UILabel!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var imgBgProfile: UIImageView!
    @IBOutlet var btnClose: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight - 50);
        NotificationCenter.default.addObserver(self, selector: #selector(receive(_:)), name: notificationName, object: nil)
        
        self.tblSideMenu.register(UINib(nibName: "SideMenuCell", bundle: nil), forCellReuseIdentifier: "SideMenuCell")
        self.view.backgroundColor = UIColor.white
        self.tblSideMenu.backgroundColor = UIColor.white
        imgProfile.layer.cornerRadius = imgProfile.frame.size.height / 2
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        self.loadMenus()
        totWidth = UIScreen.main.bounds.size.width
        totHeight = UIScreen.main.bounds.size.height
        self.tblSideMenu.reloadData()
        setFrame()
        
    }
    
    func setFrame() {
        
        //tblSideMenu.contentSize.height = CGFloat(menuItems.count * 59)
        //scrlView.
    }
    
    
    @objc func receive(_ notification: Notification) {
        
        if (notification.name == notificationName) {
            loadMenus()
            self.tblSideMenu.reloadData()
            setFrame()
            
        }
    }
    
    //    @objc func skipsubscribe(_ notification: Notification){
    //        if (notification.name == notificationSkipSubscribe){
    //
    //            if
    //        }
    //
    //    }
    //
    @IBAction func userHandleActions(_ sender: UIButton) {
        if sender == btnClose {
            SharedAppDelegate.doSingleTap()
            
        } else {
            SharedAppDelegate.doSingleTap()
        }
    }
    func userDefaultKeyAlreadyExist(kUsernameKey: String) -> Bool {
        return UserDefaults.standard.object(forKey: kUsernameKey) != nil
    }
    
    func loadMenus() {
        
        let subscribe = Util.sharedInstance.user_id
        if subscribe == ""{
            self.view_SideMenu.isHidden = true
        }
        else{
            self.view_SideMenu.isHidden = false
            if Util.isLogin() {
                lblName.text = "\(Util.sharedInstance.user_name)"
                lblEmail.text = Util.sharedInstance.user_phone
                lblCity.text = Util.sharedInstance.user_city
                if Util.sharedInstance.profile_image != "" {
                    let resource = ImageResource(downloadURL: URL(string: Util.sharedInstance.profile_image)!, cacheKey: "\(Util.sharedInstance.profile_image)")
                    imgProfile.kf.setImage(with: resource)
                }
            }
        }
        menuItems.removeAll()
        menuItemsDetails.removeAll()
        menuItems.append(Util.localized("Search Content"))
        menuItemsDetails.append(["menu_id": "-1", "type": "Search"])
        
        menuItems.append(Util.localized("About Us"))
        menuItemsDetails.append(["menu_id": "-1", "type": "About"])
        if Util.sharedInstance.appStatus == "0"{
            
        }
        
        menuItems.append(Util.sharedInstance.add_post)
        menuItemsDetails.append(["menu_id": "-1", "type": "Post"])
        
        for i in 0..<Util.sharedInstance.menusFree.count {
            let singleOBJ = Util.sharedInstance.menusFree[i]["menu_name"]
            //   menuItems.append(Util.localized(singleOBJ as! String))
            //  menuItemsDetails.append(["menu_id": "0", "details": Util.sharedInstance.menusFree[i]])
            // print(singleOBJ)
        }
        menuItems.append(Util.localized("shop"))
        menuItemsDetails.append(["menu_id": "-1", "type": "shop"])
        
        menuItems.append(Util.localized("Settings"))
        menuItemsDetails.append(["menu_id": "-1", "type": "Settings"])
        
        menuItems.append(Util.localized("Subscription"))
        menuItemsDetails.append(["menu_id": "-1", "type": "Subscription"])
        
        menuItems.append(Util.localized("Contact Us"))
        menuItemsDetails.append(["menu_id": "-1", "type": "Contact"])
        
        menuItems.append(Util.localized("Share App"))
        menuItemsDetails.append(["menu_id": "-1", "type": "Share"])
        
        
        
        if subscribe == ""
        {
            menuItems.removeAll(where: { $0 == "Logout" })
            menuItemsDetails.removeAll(where: { $0 == ["menu_id": "-1", "type": "Logout"] })
        }
        else{
            menuItems.append(Util.localized("Logout"))
            menuItemsDetails.append(["menu_id": "-1", "type": "Logout"])
            
        }
        
        setContentSize()
        if Util.sharedInstance.selectedIndex == -1 {
            // imgBgProfile.isHidden = false
            //  lblName.textColor = UIColor.white
            // lblEmail.textColor = UIColor.white
            // imgRightArrow.isHidden = true
            
        } else {
            //  imgBgProfile.isHidden = true
            // lblName.textColor = UIColor.darkText
            // lblEmail.textColor = UIColor.lightGray
            //imgRightArrow.isHidden = false
            
            
            
        }
    }
    func setContentSize() {
        //tblSideMenu.contentSize.height = CGFloat(menuItems.count * 59)
    }
    
    // MARK:- UITABLEVIEW DELEGATE
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 62
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return menuItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let reuseIdentifier: String = "SideMenuCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! SideMenuCell
        let todoItem = menuItems[indexPath.row]
        print(todoItem)
        cell.lblTitle.text = todoItem
        setFrame()
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        SharedAppDelegate.doSingleTap();
        let singleObj = menuItemsDetails[indexPath.row]
        print(singleObj)
        if let menu_id = singleObj["menu_id"] as? String {
            if menu_id == "-1" {
                if let type = singleObj["type"] as? String {
                    print(type)
                    if type == "About" {
                        let vc = Util.getStoryboard().instantiateViewController(withIdentifier: "MoreVC") as! MoreVC
                        //  vc.moreskip = shopskip
                        vc.strTitle = Util.localized("About Us")
                        vc.strUrl = Util.sharedInstance.about_us
                        // vc.contentId =
                        SharedAppDelegate.navigationControl.pushViewController(vc, animated: true)
                    } else if type == "Contact" {
                        let vc = Util.getStoryboard().instantiateViewController(withIdentifier: "MoreVC") as! MoreVC
                        vc.strTitle = Util.localized("Contact Us")
                        vc.strUrl = Util.sharedInstance.contact_us
                        SharedAppDelegate.navigationControl.pushViewController(vc, animated: true)
                    } else if type == "Settings" {
                        let vc = Util.getStoryboard().instantiateViewController(withIdentifier: "SettingsVC") as! SettingsVC
                        SharedAppDelegate.navigationControl.pushViewController(vc, animated: true)
                    }else if type == "Subscription" {
                        
                        
                        let subscribe = Util.sharedInstance.user_id
                        if subscribe == ""{
                            let alert = UIAlertController(title: "Gir Gau Jatan Sasthan", message: "You Have to login first than Subscribe ", preferredStyle: .alert)
                            let ok = UIAlertAction(title: "Ok", style: .default, handler: { action in
                                
                                let vc = Util.getStoryboard().instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                vc.skiplogin = true
                                
                                SharedAppDelegate.navigationControl.pushViewController(vc, animated: true)
                                
                            })
                            
                            alert.addAction(ok)
                            
                            DispatchQueue.main.async(execute: {
                                self.present(alert, animated: true)
                            })
                            
                        }
                        else{
                            let vc = Util.getStoryboard().instantiateViewController(withIdentifier: "SubscribeVC") as! SubscribeVC
                            
                            SharedAppDelegate.navigationControl.pushViewController(vc, animated: true)
                        }
                    }
                    else if type == "Share" {
                        let fullTextShare = Util.localized("Check out the App at: ") + Util.sharedInstance.share_app_ios_link
                        let imageToShare = [fullTextShare]
                        let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
                        activityViewController.popoverPresentationController?.sourceView = self.view
                        activityViewController.excludedActivityTypes = []
                        self.present(activityViewController, animated: true, completion: nil)
                    } else if type == "Logout" {
                        
                        let customAlertVC = Util.getStoryboard().instantiateViewController(withIdentifier: "LogoutVC") as! LogoutVC
                        
                        let popupVC = PopupViewController(contentController: customAlertVC, position: .center(CGPoint(x: 0, y: 0)), popupWidth: screenWidth, popupHeight: screenHeight)
                        popupVC.backgroundAlpha = 0.3
                        popupVC.backgroundColor = .black
                        popupVC.canTapOutsideToDismiss = false
                        popupVC.shadowEnabled = true
                        self.present(popupVC, animated: true, completion: nil)
                        
                    }
                    else if type == "Post" {
                        
                        let subscribe = Util.sharedInstance.user_id
                        if subscribe == ""{
                            let alert = UIAlertController(title: "Gir Gau Jatan Sasthan", message: "You Have to login first than Subscribe ", preferredStyle: .alert)
                            let ok = UIAlertAction(title: "Ok", style: .default, handler: { action in
                                
                                let vc = Util.getStoryboard().instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                vc.skiplogin = true
                                
                                SharedAppDelegate.navigationControl.pushViewController(vc, animated: true)
                                
                            })
                            
                            alert.addAction(ok)
                            
                            DispatchQueue.main.async(execute: {
                                self.present(alert, animated: true)
                            })
                            
                        }
                        else{
                            let vc = Util.getStoryboard().instantiateViewController(withIdentifier: "AddPostVC") as! AddPostVC
                            SharedAppDelegate.navigationControl.pushViewController(vc, animated: true)
                        }

                        
                    }else if type == "Subscription" {
                        
                        let vc = Util.getStoryboard().instantiateViewController(withIdentifier: "SubscribeVC") as! SubscribeVC
                        
                        SharedAppDelegate.navigationControl.pushViewController(vc, animated: true)
                    }else if type == "Search" {
                        let vc = Util.getStoryboard().instantiateViewController(withIdentifier: "SearchVC") as! SearchVC
                        //vc.moreskip = shopskip
                        SharedAppDelegate.navigationControl.pushViewController(vc, animated: true)
                    }else if type == "shop" {
                        
                        let vc = Util.getStoryboard().instantiateViewController(withIdentifier: "MoreVC") as! MoreVC
                        // vc.moreskip = shopskip
                        vc.strTitle = ""
                        vc.strUrl = Util.sharedInstance.sidebar_shop_link
                        SharedAppDelegate.navigationControl.pushViewController(vc, animated: true)
                    } //DonateVC
                    
                }
                
            } else {
                if let details = singleObj["details"] as? NSDictionary {
                    if let menu_id = details["menu_id"] as? Int {
                        let vc = Util.getStoryboard().instantiateViewController(withIdentifier: "CategoriesVC") as! CategoriesVC
                        vc.strTitle = details["menu_name"] as! String
                        vc.subCateId = "\(details["menu_id"] as! Int)"
                        SharedAppDelegate.navigationControl.pushViewController(vc, animated: true)
                        
                    }
                }
                
                
            }
        }
        
        if indexPath.row == 0 {
            SharedAppDelegate.doSingleTap();
            Util.sharedInstance.selectedIndex = indexPath.row
            
            
        } else if indexPath.row == 1 {
            SharedAppDelegate.doSingleTap();
            Util.sharedInstance.selectedIndex = indexPath.row
            
            
        } else if indexPath.row == 2 {
            ////contact-us
            //terms-and-condition
            //privacy-policy
            SharedAppDelegate.doSingleTap();
            Util.sharedInstance.selectedIndex = indexPath.row
            
            
        } else if indexPath.row == 3 {
            SharedAppDelegate.doSingleTap();
            Util.sharedInstance.selectedIndex = indexPath.row
            
            
        } else if indexPath.row == 5 {
            ////contact-us
            //terms-and-condition
            //privacy-policy
            SharedAppDelegate.doSingleTap();
            Util.sharedInstance.selectedIndex = indexPath.row
            
            
        } else if indexPath.row == 6 {
            ////contact-us
            //terms-and-condition
            //privacy-policy
            SharedAppDelegate.doSingleTap();
            Util.sharedInstance.selectedIndex = indexPath.row
            
            
        } else if indexPath.row == 7 {
            ////contact-us
            //terms-and-condition
            //privacy-policy
            SharedAppDelegate.doSingleTap();
            Util.sharedInstance.selectedIndex = indexPath.row
            
            
        } else if indexPath.row == 8 {
            
        } else {
            
            
        }//AboutUS
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
}
