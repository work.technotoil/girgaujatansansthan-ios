//
//  MenuDetailsVC.swift
//  Gir Gau Jatan Sansthan
//
//  Created by mac on 13/01/21.
//  Copyright © 2021 Miral Gondaliya. All rights reserved.
//

import UIKit
import Kingfisher

class collectionViewCell:UICollectionViewCell{
    
    @IBOutlet weak var imgView:UIImageView!
    @IBOutlet weak var lblname: UILabel!
}


class MenuDetailsVC: UIViewController {
    
    @IBOutlet weak var viewData: UIView!
    @IBOutlet weak var imglogo: UIImageView!
    @IBOutlet weak var lblMenuname: UILabel!
    @IBOutlet weak var collection_View: UICollectionView!
    var array_MenuDetail:NSMutableArray = []
    var array_dict = [NSDictionary]()
    var menuId:String = ""
    var menuname:String = ""
    var submenuid:String = ""
    var submenuname:String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        print(menuId)
        SubmenuDetatil_Api()
        self.lblMenuname.text = menuname
    }
    @IBAction func action_back(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func SubmenuDetatil_Api(){
        if (Util.isInternetAvailable()) {
            ActivityIndicatorVC.sharedInstance.startIndicator()
            let parameter: [String: Any] = ["user_id": Util.sharedInstance.user_id, "user_unique_id":"1","menu_id":menuId,"language":  Util.sharedInstance.user_language]
            print(parameter)
            
            ServerAPIs.postRequestWithoutToken(apiUrl: apiSubmenuDetail, parameter, completion: { (response, error, statusCode) in
                ActivityIndicatorVC.sharedInstance.stopIndicator()

                if response["success"].intValue == 1 {
                    self.array_MenuDetail.addObjects(from:(response["data"].object as! NSArray) as! [Any])
                    print(self.array_MenuDetail)}
                self.collection_View.reloadData()
                DispatchQueue.main.async {
                    self.collection_View.reloadData()
                }
            })
        }
    }
}
extension MenuDetailsVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return array_MenuDetail.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collection_View.dequeueReusableCell(withReuseIdentifier: "collectionViewCell", for: indexPath) as! collectionViewCell
        
        let details:NSDictionary = array_MenuDetail[indexPath.row] as! NSDictionary
        print(details)
        let submenu_name = details["submenu_name"] as?  String ?? ""
        print(submenu_name)
        cell.lblname.text = submenu_name
        let ImageUrl = details["submenu_image_url"] as?  String ?? ""
        let image = URL(string: ImageUrl)
        cell.imgView.kf.setImage(with: image , placeholder:UIImage(named: "gaushala"))
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let obj_CategoryVC = storyboard?.instantiateViewController(withIdentifier: "CategoriesVC") as! CategoriesVC
        let singleobj:NSDictionary = array_MenuDetail[indexPath.row] as! NSDictionary
        let submenu_Id = "\(singleobj["submenu_id"] as!  Int)"
        let submenu_name = singleobj["submenu_name"] as!  String
        obj_CategoryVC.subCateId =  submenu_Id
        obj_CategoryVC.strTitle =   submenu_name
        
        self.navigationController?.pushViewController(obj_CategoryVC, animated: true)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width/2 , height: 250)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
        
    }
    
    
}


