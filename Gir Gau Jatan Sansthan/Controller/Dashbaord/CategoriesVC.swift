//
//  CategoriesVC.swift
//  Vyaparam
//
//  Created by Miral Gondaliya on 19/10/19.
//  Copyright © 2019 Miral Gondaliya. All rights reserved.
//

import UIKit
import Kingfisher
import EzPopup
import GoogleMobileAds

class CategoriesVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {

    @IBOutlet weak var btnHideSearch: UIButton!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var viewNoData: UIView!
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var viewCategory: UIView!
    @IBOutlet weak var tblList: UITableView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    var isSubCategory = false
    var arrCategory = [NSDictionary]()
    var arrFilter = [NSDictionary]()
    var subCateId:String = ""
    var submenuname:String = ""
    var categorySkip:String = ""
    var strTitle:String = ""
    var isSearched = false
    var isSearchedHidden = true

    @IBOutlet weak var viewAd: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        print(subCateId)
        var viewAd1: GADBannerView!
        viewAd1 = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait)
        viewAd1.adUnitID = "ca-app-pub-9346622311979155/7031240531"
        viewAd1.rootViewController = self
        viewAd1.translatesAutoresizingMaskIntoConstraints = false
        viewAd1.load(GADRequest())
        viewAd.addSubview(viewAd1)

        viewHeader.addShadow()
        lblTitle.text = strTitle
        tblList.register(UINib(nibName: "CategoryCell", bundle: nil), forCellReuseIdentifier: "CategoryCell")
        apiCallGetCategory()
        txtSearch.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        txtSearch.attributedPlaceholder = NSAttributedString(string: txtSearch.placeholder!, attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])

        // Do any additional setup after loading the view.
    }
    @objc func textFieldDidChange(_ textField: UITextField) {
        if txtSearch.text!.count > 0 {
            isSearched = true
            getSearchData()

        } else {
            isSearched = false
            tblList.reloadData()


        }
    }

    @IBAction func Actions(_ sender: UIButton) {
        if sender == btnBack
            {
            SharedAppDelegate.navigationControl.popViewController(animated: true)
        } else if sender == btnSearch {
            if isSearchedHidden {
                isSearchedHidden = false
                viewSearch.isHidden = false
                txtSearch.becomeFirstResponder()

            } else {
                isSearchedHidden = true
                isSearched = false
                tblList.reloadData()
                txtSearch.text = ""


            }
        } else if sender == btnHideSearch {
            isSearchedHidden = true
            isSearched = false
            tblList.reloadData()
            txtSearch.text = ""
            viewSearch.isHidden = true
            self.view.endEditing(true)

        }

    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearched {
            return arrFilter.count

        }
        return arrCategory.count
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var singleObj: [String: Any] = [:]
        if isSearched {
            singleObj = arrFilter[indexPath.row] as! [String: Any]
        } else {
            singleObj = arrCategory[indexPath.row] as! [String: Any]

        }
       
        let vc = Util.getStoryboard().instantiateViewController(withIdentifier: "MoreVC") as! MoreVC
        vc.strTitle = singleObj["content_name"] as! String
        vc.strUrl = "\(singleObj["content_ios"] as! String)"
        vc.contentId = "\(singleObj["content_id"] as! Int)"
        SharedAppDelegate.navigationControl.pushViewController(vc, animated: true)

    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let reuseIdentifire = "CategoryCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifire, for: indexPath) as! CategoryCell
        var singleObj: [String: Any] = [:]
        if isSearched {
            singleObj = arrFilter[indexPath.row] as! [String: Any]
        } else {
            singleObj = arrCategory[indexPath.row] as! [String: Any]

        }

        if let name = singleObj["content_name"] as? String {
            cell.lbl.text = name
            if name != "" {
                cell.lblChar.text = "\(name.first!)"

            }
        } else {
            if let name = singleObj["content_name"] as? Int {
                cell.lbl.text = "\(name)"
                cell.lblChar.text = "\("\(name)".first!)"

            }
        }
        cell.lbl.frame.size.width = screenWidth - 88
        cell.lbl.numberOfLines = 0
        cell.lbl.sizeToFit()
        if cell.lbl.frame.size.height <= 50 {
            cell.lbl.frame.origin.y = (cell.viewMain.frame.size.height - cell.lbl.frame.size.height) / 2.0;

        }else{
            
        }
        if indexPath.row % 2 == 0 {
            cell.viewMain.backgroundColor = UIColor.white
        } else {
            cell.viewMain.backgroundColor = UIColor.returnRGBColor(r: 244, g: 244, b: 244, alpha: 1.0)

        }
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //50
        var singleObj: [String: Any] = [:]
        singleObj = arrCategory[indexPath.row] as! [String: Any]
        let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: 17)
        if let name = singleObj["content_name"] as? String {
            lbl.text = name
            lbl.frame.size.width = screenWidth - 88
            lbl.numberOfLines = 0
            lbl.sizeToFit()

        } else if let name = singleObj["content_name"] as? Int {
            lbl.text = "\(name)"
            lbl.frame.size.width = screenWidth - 88
            lbl.numberOfLines = 0
            lbl.sizeToFit()

        }
        if lbl.frame.size.height <= 50 {
            return 75

        } else {
            return lbl.frame.size.height + 25

        }
    }

    func apiCallGetCategory() {
        if (Util.isInternetAvailable()) {
            ActivityIndicatorVC.sharedInstance.startIndicator()


            var parameter = [String: Any]()
            var strUrl = ""

            parameter = ["user_unique_id": "1", "user_id": Util.sharedInstance.user_id, "submenu_id": subCateId, "language": UserDefaults.standard.string(forKey: "user_language") ?? ""]
          print(parameter)

            ServerAPIs.postRequestWithoutToken(apiUrl: apiContentList, parameter, completion: { (response, error, statusCode) in
                ActivityIndicatorVC.sharedInstance.stopIndicator()
               print(response)
                if statusCode == 200 || statusCode == 201 {


                    if response["success"].intValue == 1 {
                        if let tempData: [NSDictionary] = response["data"].arrayObject as? [NSDictionary] {

                            self.arrCategory = tempData
                            self.tblList.reloadData()
                        }
                    }


                }
                if self.arrCategory.count > 0 {
                    self.tblList.isHidden = false
                    self.viewCategory.isHidden = true
                    //self.viewSearch.isHidden = false
                    self.viewNoData.isHidden = true

                } else {
                    self.tblList.isHidden = true
                    self.viewCategory.isHidden = false
                    // self.viewSearch.isHidden = true
                    self.viewNoData.isHidden = false


                }
            })

        } else {
            Toast(text: Util.localized("InternetError")).show()

        }
    }



    func getSearchData() {
        self.arrFilter.removeAll()
        for i in 0..<arrCategory.count {
            var singleObj: [String: Any] = [:]
            singleObj = arrCategory[i] as! [String: Any]
            if let category_name = singleObj["content_name"] as? String {


                let name = category_name.lowercased()
                //Category
                let txtEntered = txtSearch.text!.lowercased()

                if name.contains(txtEntered)
                    {
                    self.arrFilter.append(singleObj as NSDictionary)
                }
            }

        }

        tblList.reloadData()
        if arrFilter.count == 0 {
            tblList.isHidden = true
            viewCategory.isHidden = false
        } else {
            tblList.isHidden = false
            viewCategory.isHidden = true

        }
    }
}
