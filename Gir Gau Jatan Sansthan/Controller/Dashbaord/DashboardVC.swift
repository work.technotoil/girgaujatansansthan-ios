//
//  DashboardVC.swift
//  Gir Gau Jatan Sansthan
//
//  Created by Miral Gondaliya on 05/03/20.
//  Copyright © 2020 Miral Gondaliya. All rights reserved.
//

import UIKit
import BLMultiColorLoader
import Kingfisher

class DashboardVC: UIViewController, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UICollectionViewDelegate {

    var arrStory = [NSDictionary]()
    var array_menu = [NSDictionary]()
    var array_data :NSMutableArray = []
    @IBOutlet var tableView: UITableView!

    @IBOutlet weak var lblMenu: UILabel!
    @IBOutlet var collectionSlider: UICollectionView!
    @IBOutlet weak var collectionMain: UICollectionView!

    @IBOutlet weak var loader: BLMultiColorLoader!
    @IBOutlet weak var scrollMain: UIScrollView!
    @IBOutlet weak var btnSideMenu: UIButton!
    @IBOutlet weak var btnCart: UIButton!
    @IBOutlet weak var lblTitle: UILabel!

    @IBOutlet weak var viewLoader: UIView!
    
    @IBOutlet weak var viewFirst: UIView!
    
    //@IBOutlet weak var imgBG: UIImageView!
    var arrSlider = [String]()
    var arrMenus = [NSDictionary]()
    var timer = Timer()
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet var pageination: UIPageControl!

    var arrMenusNew = [NSDictionary]()
    var isShown = false

    override func viewDidLoad() {
        super.viewDidLoad()

        //lblTitle.text = Util.localized("App Name New")
        lblMenu.text = Util.localized("Menu")
        arrSlider = Util.sharedInstance.bannersArr
      //  arrMenus = Util.sharedInstance.menusPaid
        //self.arrMenus.append(["menu_id": "-1", "menu_name": Util.sharedInstance.postlist, "menu_image_url": "Ad"])
        
                              //   self.arrMenus.append(["menu_id": "-1", "menu_name": Util.localized("Products"), "menu_image_url": "Product"])
        arrMenusNew = Util.sharedInstance.menusPaid
        viewLoader.layer.cornerRadius = 20
        viewLoader.clipsToBounds = true
        apiCallDashboard()

        collectionSlider?.register(UINib(nibName: "sliderCollectionCell", bundle: nil), forCellWithReuseIdentifier: "sliderCollectionCell")
        collectionMain?.register(UINib(nibName: "HomeCategoryCell", bundle: nil), forCellWithReuseIdentifier: "HomeCategoryCell")
        //HomeCategoryCell
        loader.lineWidth = 2
        loader.colorArray = [appColour]

        scrollMain.refreshControl = UIRefreshControl()
        scrollMain.refreshControl?.addTarget(self, action:
                #selector(handleRefreshControl),
            for: .valueChanged)
        //wY18YwPe2kZM55ap9nWcncXW


        //rzp_test_8McmuDNsPfDU2U
    }
    @objc func handleRefreshControl() {
        // Update your content…

        // Dismiss the refresh control.
        apiCallDashboard()

    }
    func apiCallDashboard() {
        
        if (Util.isInternetAvailable()) {
               ActivityIndicatorVC.sharedInstance.startIndicator()
        //    SharedAppDelegate.showLoader()

            let parameter: [String: Any] = ["user_id": Util.sharedInstance.user_id, "user_unique_id":"1", "language": Util.sharedInstance.user_language];
            print(parameter)

            ServerAPIs.postRequestWithoutToken(apiUrl: apiDashboardAndSidemenu, parameter, completion: { (response, error, statusCode) in
                  ActivityIndicatorVC.sharedInstance.stopIndicator()
               // SharedAppDelegate.hideLoader()
                print(response)
                DispatchQueue.main.async {
                    self.scrollMain.refreshControl?.endRefreshing()
                }
                if response["success"].intValue == 1 {
                   if let tempData: NSDictionary = response["data"].object as? NSDictionary {
                        
                    //self.arrMenus = tempData
                 //self.array_data = NSMutableArray(array: response)

                       print(tempData)
                        if let banners = tempData["banners"] as? [String] {
                            Util.sharedInstance.bannersArr = banners
                        }
                        if let is_subscribed = tempData["is_subscribed"] as? Int {

                            Util.sharedInstance.is_subscribed = is_subscribed

                        }
                        if let subscription_expiry = tempData["subscription_expiry"] as? String {

                            Util.sharedInstance.subscription_expiry = subscription_expiry

                        }
                        //subscription_expiry
                        if let cms_links = tempData["cms_links"] as? NSDictionary {
                            if let about_us = cms_links["about_us"] as? String {
                                Util.sharedInstance.about_us = "\(about_us)"
                            }
                            if let contact_us = cms_links["contact_us"] as? String {
                                Util.sharedInstance.contact_us = "\(contact_us)"
                            }
                            if let sidebar_shop_link = cms_links["sidebar_shop_link"] as? String {
                                Util.sharedInstance.sidebar_shop_link = "\(sidebar_shop_link)"
                            }
                            if let share_app_ios_link = cms_links["share_app_ios_link"] as? String {
                                Util.sharedInstance.share_app_ios_link = "\(share_app_ios_link)"
                            }
                            if let product_link = cms_links["product_link"] as? String {
                                Util.sharedInstance.product_link = "\(product_link)"
                            }
                            if let share_app_android_link = cms_links["share_app_android_link"] as? String {
                                Util.sharedInstance.share_app_android_link = "\(share_app_android_link)"
                            }
                            //sidebar_shop_link
                            //share_app_ios_link
                            //product_link
                            //share_app_android_link

                        }
                        if let paid = tempData["advertisement_amount"] as? Int {
                            Util.sharedInstance.advertisement_amount = "\(paid)"
                        }
                        if let paid = tempData["subscription_amount"] as? Int {
                            Util.sharedInstance.subscription_amount = "\(paid)"
                        }
                        if let menus = tempData["menus"] as? NSDictionary {
                            if let paid = menus["paid"] as? [NSDictionary] {
                                Util.sharedInstance.menusPaid = paid
                            }
                            if let paid = menus["free"] as? [NSDictionary] {
                                Util.sharedInstance.menusFree = paid
                            }
                            if let ios_titles = tempData["ios_titles"] as? NSDictionary {
                                if let postlist = ios_titles["postlist"] as? String {
                                    Util.sharedInstance.postlist = "\(postlist)"
                                }
                                if let add_post = ios_titles["add_post"] as? String {
                                    Util.sharedInstance.add_post = "\(add_post)"
                                }
                                if let donation = ios_titles["donation"] as? String {
                                    Util.sharedInstance.donation = "\(donation)"
                                }
                                if let donate_already = ios_titles["donate_already"] as? String {
                                    Util.sharedInstance.donate_already = "\(donate_already)"
                                }
                                if let donate_amount_message = ios_titles["donate_amount_message"] as? String {
                                    Util.sharedInstance.donate_amount_message = "\(donate_amount_message)"
                                }

                                if let donate = ios_titles["donate"] as? String {
                                    Util.sharedInstance.donate = "\(donate)"
                                }

                            }

                            self.arrSlider = Util.sharedInstance.bannersArr
                        //    self.arrMenus = Util.sharedInstance.menusPaid
                         if Util.sharedInstance.donate_already == "-" {
                                           //self.arrMenus.append(["menu_id": "-1", "menu_name": Util.sharedInstance.postlist, "menu_image_url": "gaushala"])

                                       } else {
                                           self.arrMenus.append(["menu_id": "-1", "menu_name": Util.sharedInstance.postlist, "menu_image_url": "Ad"])

                                       }
                            
                            //self.arrMenus.append(["menu_id": "-1", "menu_name": Util.localized("Products"), "menu_image_url": "Product"])

                            self.arrMenusNew = Util.sharedInstance.menusPaid

                            for i in 0..<Util.sharedInstance.menusFree.count {

                                self.arrMenusNew.append(Util.sharedInstance.menusFree[i])
                            }
                            self.pageination.numberOfPages = self.arrSlider.count

                            self.collectionMain.reloadData()
                            self.collectionSlider.reloadData()

                            self.tableView.reloadData()
                            self.timer.invalidate()
                            if self.arrSlider.count > 0 {
                                self.timer = Timer.scheduledTimer(timeInterval: 3.5, target: self, selector: #selector(self.timerAction), userInfo: nil, repeats: true)
                            }


                        }
                    }

                } else {
                    Toast(text: response["message"].stringValue).show()



                }
            })

        } else {
            Toast(text: Util.localized("InternetError")).show()
        }
    }
             
    
//    func menuApi(){
//
//        let parameter: [String: Any] = ["user_id": Util.sharedInstance.user_id, "user_unique_id": Util.sharedInstance.user_unique_id, "language": Util.sharedInstance.user_language];
//        print(parameter)
//
//      //  AFWrapper.sharedInstance.requestPOSTBeforeLoginURL(apiDashboardAndSidemenu, params: parameter,successBlock: { (response) in
//        ServerAPIs.requestPOSTwithoutToken(strURL: apiDashboardAndSidemenu, params: parameter,  successBlock: { (response) in
//
//        print(response)
//          let code = response.object(forKey: "success") as! Int
//
//           if code == 1{
//
//
//             //   let data : NSDictionary = response.object(forKey: "data") as! NSDictionary
//               self.array_data = NSMutableArray(array: response.object(forKey: "data") as! NSArray)
//              //  self.array_data = data
//
//           // let banner = data.object(forKey: "banners") as! NSArray
//             //   print(banner)
//
//          }
//
//        }){ (error) in
//
//        }
//    }

    @IBAction func Actions(_ sender: UIButton) {
        
           SharedAppDelegate.showSideMenu()
        
//        else if sender == btnCart {
//            
//            let vc = Util.getStoryboard().instantiateViewController(withIdentifier: "SettingsVC") as! SettingsVC
//            SharedAppDelegate.navigationControl.pushViewController(vc, animated: true)
//            
//        }
    }

    @IBAction func action_Setting(_ sender: Any) {
        let vc = Util.getStoryboard().instantiateViewController(withIdentifier: "SettingsVC") as! SettingsVC
                   SharedAppDelegate.navigationControl.pushViewController(vc, animated: true)
    }
    
    @IBAction func action_Search(_ sender: Any) {
        let vc = Util.getStoryboard().instantiateViewController(withIdentifier: "SearchVC") as! SearchVC
        SharedAppDelegate.navigationControl.pushViewController(vc, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.arrSlider = Util.sharedInstance.bannersArr
      //  self.arrMenus = Util.sharedInstance.menusPaid
       
  //      self.arrMenus.append(["menu_id": "-1", "menu_name": Util.localized("Products"), "menu_image_url": "Product"])

        self.pageination.numberOfPages = self.arrSlider.count

        self.collectionMain.reloadData()
        self.collectionSlider.reloadData()

        self.timer.invalidate()
        if self.arrSlider.count > 0 {
            self.timer = Timer.scheduledTimer(timeInterval: 3.5, target: self, selector: #selector(self.timerAction), userInfo: nil, repeats: true)

        }
        self.timer.invalidate()
        if arrSlider.count > 0 {
            self.timer = Timer.scheduledTimer(timeInterval: 3.5, target: self, selector: #selector(self.timerAction), userInfo: nil, repeats: true)

        }

    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        //slider.stopAutoPlay()
        self.timer.invalidate()
    }

    @objc func timerAction() {
        var indexPath: IndexPath!
        var current = pageination.currentPage
        current += 1
        if current > arrSlider.count {
            current = 0
        }
        indexPath = IndexPath(item: current, section: 0)

        collectionSlider.selectItem(at: indexPath, animated: true, scrollPosition: .centeredHorizontally)
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {

        if scrollView == self.collectionSlider {
            self.timer.invalidate()

            let index = scrollView.contentOffset.x / scrollView.frame.size.width

            pageination.currentPage = Int(index)
            self.timer = Timer.scheduledTimer(timeInterval: 3.5, target: self, selector: #selector(self.timerAction), userInfo: nil, repeats: true)

        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionSlider {
            return arrSlider.count

        } else {
            return arrMenus.count
        }
        return arrSlider.count

    }

    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        if collectionView == collectionSlider {
            let cellId = "sliderCollectionCell"

            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! sliderCollectionCell

            let singleObj = arrSlider[indexPath.row]
            
            if let image = singleObj as? String {
                cell.img.image = UIImage()
                if URL(string: image) != nil {
                    let resource = ImageResource(downloadURL: URL(string: image)!, cacheKey: image)
                    cell.img.kf.setImage(with: resource)
                } else {
                    let original = image
                    if let encoded = original.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed),
                        let url = URL(string: encoded)
                        {
                        let resource = ImageResource(downloadURL: url, cacheKey: image)
                        cell.img.kf.setImage(with: resource)
                    }
                }

            }
            // cell.viewMain.addShadow()
            //  cell.img.image = UIImage(named: "Slider")
            //  cell.img.clipsToBounds = true
            // cell.img.layer.cornerRadius = 5.0
            setFrame()

            return cell


        } else {
            let cellId = "HomeCategoryCell"

            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! HomeCategoryCell

            var singleObj: [String: Any] = [:]
            singleObj = arrMenus[indexPath.row] as! [String: Any]
print(singleObj)
            if let menu_id = singleObj["menu_id"] as? String {
                if menu_id == "-1" {
                    cell.img.image = UIImage(named: singleObj["menu_image_url"] as! String)
                } else {
                    if let image = singleObj["menu_image_url"] as? String {
                        cell.img.image = UIImage()
                        if URL(string: image) != nil {
                            let resource = ImageResource(downloadURL: URL(string: image)!, cacheKey: image)
                            cell.img.kf.setImage(with: resource)
                        } else {
                            let original = image
                            if let encoded = original.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed),
                                let url = URL(string: encoded)
                                {
                                let resource = ImageResource(downloadURL: url, cacheKey: image)
                                cell.img.kf.setImage(with: resource)
                            }
                        }

                    }
                }
            } else {
                if let image = singleObj["menu_image_url"] as? String {
                    cell.img.image = UIImage()
                    if URL(string: image) != nil {
                        let resource = ImageResource(downloadURL: URL(string: image)!, cacheKey: image)
                        cell.img.kf.setImage(with: resource)
                    } else {
                        let original = image
                        if let encoded = original.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed),
                            let url = URL(string: encoded)
                            {
                            let resource = ImageResource(downloadURL: url, cacheKey: image)
                            cell.img.kf.setImage(with: resource)
                        }
                    }
                }
            }
            
            if let product_name = singleObj["menu_name"] as? String {
                cell.lbl1.text = product_name
            }
            setFrame()

            return cell
       }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == collectionSlider {
            return CGSize(width: screenWidth, height: 179)
        } else {
            return CGSize(width: (screenWidth) / 2, height: 238)
        }



    }


    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collectionMain {
            let singleObj = arrMenus[indexPath.row]
            if let menu_id = singleObj["menu_id"] as? String {
                if menu_id == "-1" {
                    if let menu_id = singleObj["menu_name"] as? String {
                        if menu_id == Util.sharedInstance.postlist {
                            let vc = Util.getStoryboard().instantiateViewController(withIdentifier: "PostListVC") as! PostListVC
                            SharedAppDelegate.navigationControl.pushViewController(vc, animated: true)
                        } else {
                            let vc = Util.getStoryboard().instantiateViewController(withIdentifier: "MoreVC") as! MoreVC
                            vc.strTitle = ""
                            vc.strUrl = Util.sharedInstance.product_link
                            SharedAppDelegate.navigationControl.pushViewController(vc, animated: true)
                        }


                    }

                } else {
                    if let menu_id = singleObj["menu_id"] as? Int {
                        let vc = Util.getStoryboard().instantiateViewController(withIdentifier: "CategoriesVC") as! CategoriesVC
                        vc.strTitle = singleObj["menu_name"] as! String
                        vc.subCateId = "\(singleObj["menu_id"] as! Int)"
                        SharedAppDelegate.navigationControl.pushViewController(vc, animated: true)

                    }


                }
            } else {
                if let menu_id = singleObj["menu_id"] as? Int {
                    let vc = Util.getStoryboard().instantiateViewController(withIdentifier: "CategoriesVC") as! CategoriesVC
                    vc.strTitle = singleObj["menu_name"] as! String
                    vc.subCateId = "\(singleObj["menu_id"] as! Int)"
                    SharedAppDelegate.navigationControl.pushViewController(vc, animated: true)

                }
            }
        }
    }

    func setFrame() {
        if arrSlider.count == 0 {

            collectionSlider.isHidden = true
            pageination.isHidden = true
            viewMain.frame.origin.y = 0

        } else {
            collectionSlider.isHidden = false
            pageination.isHidden = false
            viewMain.frame.origin.y = 187
        }
        //For only One cell
        if arrMenus.count == 0 {

            collectionMain.frame.size.height = 1

        } else {
            collectionMain.frame.size.height = collectionMain.contentSize.height

        }
        collectionMain.frame.size.height = 0
        //  collectionMain.frame.size.height = collectionMain.contentSize.height
        tableView.frame.origin.y = viewFirst.frame.origin.y + viewFirst.frame.size.height + 10
        tableView.frame.size.height = tableView.contentSize.height

        viewMain.frame.size.height = tableView.frame.origin.y + tableView.frame.size.height + 10
        scrollMain.contentSize.height = viewMain.frame.origin.y + viewMain.frame.size.height

    }

    //MARK: - Action Methods

    
    
    @IBAction func onClickProduct(_ sender:Any) {
        
        let vc = Util.getStoryboard().instantiateViewController(withIdentifier: "MoreVC") as! MoreVC
        vc.strTitle = ""
        vc.strUrl = Util.sharedInstance.product_link
        SharedAppDelegate.navigationControl.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func onClickBlog(_ sender:Any) {
        
        let vc = Util.getStoryboard().instantiateViewController(withIdentifier: "blog") as! BlogVC
        vc.strTitle = "Blog"
        vc.isBlog = true
        SharedAppDelegate.navigationControl.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func onClickConnect(_ sender:Any) {
        
        let vc = Util.getStoryboard().instantiateViewController(withIdentifier: "connect") as! ConnectVC
        SharedAppDelegate.navigationControl.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func onClickSponsored(_ sender:Any) {
        
        let vc = Util.getStoryboard().instantiateViewController(withIdentifier: "blog") as! BlogVC
        vc.strTitle = "Sponsored"
        vc.isBlog = false
        SharedAppDelegate.navigationControl.pushViewController(vc, animated: true)
        
    }
    
    func menu_api(){
        
    }
    
    
    
}

extension DashboardVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMenusNew.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TableCell") as! TableCell
        //images
        var singleObj: [String: Any] = [:]
        cell.lblName.text = ""
        singleObj = arrMenusNew[indexPath.row] as! [String: Any]
        if let display_name = singleObj["menu_name"] as? String {
            if display_name != "" {
                cell.lblName.text = display_name
            }
        }else if let display_name = singleObj["menu_name"] as? Int {
            cell.lblName.text = "\(display_name)"
        }
        cell.arrStories = [NSDictionary]()
        if let submenus = singleObj["submenus"] as? [NSDictionary] {
            cell.arrStories = submenus
        }
        
        DispatchQueue.main.async {
            
            cell.viewNameContainer.roundCorners(corners:[.topLeft,.topRight], radius: 10)
            
        }
        
        //cell.lblName.sizeToFit()

        cell.viewNameContainer.frame = CGRect(x: cell.viewNameContainer.frame.origin.x, y: cell.viewNameContainer.frame.origin.y, width: cell.lblName.frame.width+5, height:cell.viewNameContainer.frame.height)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      

        let obj_detailsVc = storyboard?.instantiateViewController(withIdentifier: "MenuDetailsVC") as! MenuDetailsVC
         print(arrMenusNew)
        
        let singleObj = arrMenusNew[indexPath.row]

//        for detail in singleObj{
// "\(singleObj["menu_id"] as! Int)"menu_namesubmenu_id
           let menuId = "\(singleObj.object(forKey: "menu_id") as! Int)"
           let menu_name = singleObj.object(forKey: "menu_name") as! String
           // let submenu_id = "\(singleObj.object(forKey: "submenu_id") as! Int)"

      //  let submenu_name = "\(singleObj.object(forKey: "submenu_name") as! Int)"

        print(menuId)
//            print(arraydetail.count)
//            self.array_menu =  arraydetail
//            print(self.array_menu)
//
//        }
        obj_detailsVc.menuId = menuId
        obj_detailsVc.menuname = menu_name
        //obj_detailsVc.submenuid = submenu_id
        //obj_detailsVc.submenuname = submenu_name

        self.navigationController?.pushViewController(obj_detailsVc, animated: true)
    }
    @objc func handleProfileAction(sender: UIButton) {

    }
    @objc func handleShareAction(sender: UIButton) {

    }
    @objc func handleCommentAction(sender: UIButton) {



    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }

    func show(strUrl: String) {

    }


}
class TableCell: UITableViewCell {

    var images = [NSDictionary]() {
        didSet {
        }
    }
    
    var arrStories = [NSDictionary]() {
        didSet {
        //    collectionView.reloadData()

        }
    }
    //var arrStories = [NSDictionary]()

    @IBOutlet var collectionView: UICollectionView!

    @IBOutlet var lblName: UILabel!
    @IBOutlet var viewNameContainer: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
       // collectionView.delegate = self
       // collectionView.dataSource = self
    }
    @objc func handlePlayAction(sender: UIButton) {

        print("Called")
        //Util.showWebPopUp(Url: images2[sender.tag])
    }
    @objc func handleImageAction(sender: UIButton) {
        let singleObj = self.images[sender.tag]
        //is_process_done
        let selectedIdForMore = singleObj


        let info: [String: Any] = ["URL": selectedIdForMore]
        NotificationCenter.default.post(name: Notification.Name("ShowImage"), object: nil, userInfo: info)


    }
}
/*extension TableCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //  return images.count
        return arrStories.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionCell", for: indexPath) as! CollectionCell
        //cell.imgView.image = images[indexPath.row]
        let singleObj = arrStories[indexPath.row] as! [String: Any]
//submenu_image_url
        if let image = singleObj["submenu_image_url"] as? String {
            cell.imgView.image = UIImage()
            if URL(string: image) != nil {
                let resource = ImageResource(downloadURL: URL(string: image)!, cacheKey: image)
                cell.imgView.kf.setImage(with: resource)
            } else {
                let original = image
                if let encoded = original.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed),
                    let url = URL(string: encoded)
                    {
                    let resource = ImageResource(downloadURL: url, cacheKey: image)
                    cell.imgView.kf.setImage(with: resource)
                }
            }

        }

        cell.lblTitle.text = ""
        if let submenu_name = singleObj["submenu_name"] as? String {
            if submenu_name != "" {
                cell.lblTitle.text = submenu_name
            }
        }else if let submenu_name = singleObj["submenu_name"] as? Int {
            cell.lblTitle.text = "\(submenu_name)"
        }
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: screenWidth / 2, height: 238)
    }

    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let singleObj = arrStories[indexPath.row] as! NSDictionary

        if let menu_id = singleObj["submenu_id"] as? Int {
            let vc = Util.getStoryboard().instantiateViewController(withIdentifier: "CategoriesVC") as! CategoriesVC
            if let submenu_name = singleObj["submenu_name"] as? String {
                       if submenu_name != "" {
                                      vc.strTitle = singleObj["submenu_name"] as! String

                       }
                   }else if let submenu_name = singleObj["submenu_name"] as? Int {
                    vc.strTitle = "\(submenu_name)"
                   }
            vc.subCateId = "\(singleObj["submenu_id"] as! Int)"
            SharedAppDelegate.navigationControl.pushViewController(vc, animated: true)

        }

    }

}

class CollectionCell: UICollectionViewCell {

    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet var imgView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        viewMain.layer.cornerRadius = 7.0
        viewMain.layer.borderColor = appGrayColor.cgColor
        viewMain.layer.borderWidth = 0.7

    }
}*/
extension UIView {
   func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}
@IBDesignable class shadowCornerViewWithShadow: UIView {
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            
            self.layer.cornerRadius = cornerRadius
            self.layer.masksToBounds = true
            setNeedsLayout()
        }
    }
    
    @IBInspectable var shadowRadius: CGFloat = 0 {
        didSet {
            
            self.layer.masksToBounds = false
            self.layer.shadowOffset = CGSize(width: 0, height: 2)
            self.layer.shadowColor = UIColor.darkGray.withAlphaComponent(0.4).cgColor
            self.layer.shadowRadius = shadowRadius
            self.layer.shadowOpacity = 1
            
            let backgroundCGColor = backgroundColor?.cgColor
            backgroundColor = nil
            layer.backgroundColor =  backgroundCGColor
            
            setNeedsLayout()
        }
    }
}
