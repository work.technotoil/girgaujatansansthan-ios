//
//  MoreVC.swift
//  iBuid
//
//  Created by Miral Gondaliya on 15/02/20.
//  Copyright © 2020 Miral Gondaliya. All rights reserved.
//

import UIKit
import WebKit
import GoogleMobileAds

class MoreVC: UIViewController,WKNavigationDelegate {
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    var strTitle:String = ""
    var strUrl:String = ""
    var contentId:String = ""
    var message:String = ""
    
    
    @IBOutlet weak var viewAd: UIView!
    @IBOutlet weak var viewHeader: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        print(contentId)
        print(strUrl)
        print(strTitle)
        
        SharedAppDelegate.showLoader()
        if strTitle == ""{
            viewMain.frame.size.height =  viewMain.frame.size.height  +  viewAd.frame.size.height
            viewAd.frame.size.height = 0
        }else{
            var viewAd1: GADBannerView!
            viewAd1 = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait)
            viewAd1.adUnitID = "ca-app-pub-9346622311979155/7031240531"
            viewAd1.rootViewController = self
            viewAd1.translatesAutoresizingMaskIntoConstraints = false
            viewAd1.load(GADRequest())
            viewAd.addSubview(viewAd1)
        }
        
        viewHeader.addShadow()
        
        if contentId != ""{
            CheckSubscribe_Api()
            
        }
        else{
            let webView = WKWebView(frame: CGRect(x: 0, y: 0, width: self.viewMain.frame.size.width, height: self.viewMain.frame.size.height))
            webView.navigationDelegate  = self
            self.viewMain.addSubview(webView)
            let url = URL(string: self.strUrl)
            webView.load(URLRequest(url: url!))
            self.lblTitle.text = self.strTitle
        }
        
        
        
        // Do any additional setup after loading the view.
    }
    
    func CheckSubscribe_Api(){
        
        if (Util.isInternetAvailable()) {
            
            let subscribe = Util.sharedInstance.user_id
            
//            if subscribe == ""{
//
//                let alert = UIAlertController(title: "Gir Gau Jatan Sasthan", message: "You Have to login first ", preferredStyle: .alert)
//                let ok = UIAlertAction(title: "Ok", style: .default, handler: { action in
//
//                    let vc = Util.getStoryboard().instantiateViewController(withIdentifier: "ViewController") as! ViewController
//                    vc.skiplogin = true
//                    // vc.notSubscribe = self.moreskip
//                    SharedAppDelegate.navigationControl.pushViewController(vc, animated: true)
//
//                })
//
//                alert.addAction(ok)
//                //alert.addAction(cancel)
//                DispatchQueue.main.async(execute: {
//                    self.present(alert, animated: true)
//                })
//
//            }else{
                ActivityIndicatorVC.sharedInstance.startIndicator()
                let parameter: [String: Any] = ["user_id": Util.sharedInstance.user_id, "cid": contentId]
                print(parameter)
                // let tempData: NSDictionary = response["data"].object as? NSDictionary
                
                ServerAPIs.postRequestWithoutToken(apiUrl: apiCeckSubscribe, parameter, completion: { (response, error, statusCode) in
                    ActivityIndicatorVC.sharedInstance.stopIndicator()
                    print(response)
                    
                    let message = response["message"]
                    print(message)
                    print(self.strUrl)
                    
                    if response["success"].intValue == 1
                    
                    {
                        let webView = WKWebView(frame: CGRect(x: 0, y: 0, width: self.viewMain.frame.size.width, height: self.viewMain.frame.size.height))
                        webView.navigationDelegate  = self
                        self.viewMain.addSubview(webView)
                        let url = URL(string: self.strUrl)
                        webView.load(URLRequest(url: url!))
                        self.lblTitle.text = self.strTitle
                        
                    }
                    
                    else {
                        
                        let alert = UIAlertController(title: "Gir Gau Jatan Sasthan", message: "You Have not Subscribed.", preferredStyle: .alert)
                        let ok = UIAlertAction(title: "Yes", style: .default, handler: { action in
                            
                            let vc = Util.getStoryboard().instantiateViewController(withIdentifier: "SubscribeVC") as! SubscribeVC
                            // vc.notSubscribe = self.moreskip
                            SharedAppDelegate.navigationControl.pushViewController(vc, animated: true)
                            
                        })
                        let cancel = UIAlertAction(title: "No", style: .default, handler: { action in
                            self.navigationController?.popViewController(animated: true)
                            
                        })
                        alert.addAction(ok)
                        alert.addAction(cancel)
                        DispatchQueue.main.async(execute: {
                            self.present(alert, animated: true)
                        })
                        
                    }
                })
            }
//        }
    }
    
    @IBAction func Action(_ sender: UIButton) {
        if sender == btnBack{
            SharedAppDelegate.navigationControl.popViewController(animated: true)
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        SharedAppDelegate.hideLoader()
        
    }
    func getQueryStringParameter(url: String, param: String) -> String? {
        guard let url = URLComponents(string: url) else { return nil }
        return url.queryItems?.first(where: { $0.name == param })?.value
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        // SharedAppDelegate.hideLoader()
    }
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        SharedAppDelegate.hideLoader()
    }
    
}
extension  MoreVC {
    
    func showAlert(message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "Yes", style: .default, handler: { action in
            
            
        })
        let cancel = UIAlertAction(title: "No", style: .default, handler: { action in
            
            
        })
        alert.addAction(ok)
        alert.addAction(cancel)
        DispatchQueue.main.async(execute: {
            self.present(alert, animated: true)
        })
    }
}
