//
//  ExtensionViewController.swift
//  Gir Gau Jatan Sansthan
//
//  Created by TechnoToil on 22/02/21.
//  Copyright © 2021 Miral Gondaliya. All rights reserved.
//

import UIKit
import AVFoundation

extension UIView{
    
    @IBInspectable var shadowOffset1: CGSize{
        get{
            return self.layer.shadowOffset
        }
        set{
            self.layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable var shadowColor1: UIColor{
        get{
            return UIColor(cgColor: self.layer.shadowColor!)
        }
        set{
            self.layer.shadowColor = newValue.cgColor
        }
    }
    
    @IBInspectable var shadowRadius1: CGFloat{
        get{
            return self.layer.shadowRadius
        }
        set{
            self.layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable var shadowOpacity1: Float{
        get{
            return self.layer.shadowOpacity
        }
        set{
            self.layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable var cornerRadius1: CGFloat{
        get{
            return self.layer.cornerRadius
        }
        set{
            self.layer.cornerRadius = newValue
        }
    }
    
    @IBInspectable var borderColor1: UIColor{
        get{
            return UIColor(cgColor: self.layer.borderColor!)
        }
        set{
            self.layer.borderColor = newValue.cgColor
        }
    }
    @IBInspectable var borderWidth1: CGFloat{
        get{
            return self.layer.borderWidth
        }
        set{
            self.layer.borderWidth = newValue
        }
    }
    @IBInspectable var padding_left1: CGFloat {
        get {

            return 0
        }
        set (f) {
            layer.sublayerTransform = CATransform3DMakeTranslation(f, 0, 0)
        }
    }
    
     @IBInspectable var padding_right1: CGFloat {
       get {

                  return 0
              }
              set (f) {
                  layer.sublayerTransform = CATransform3DMakeTranslation(0, 0, f)
              }
     }
        
      func roundCorners(_ corners: CACornerMask, radius: CGFloat, borderColor: UIColor, borderWidth: CGFloat) {
            self.layer.maskedCorners = corners
            self.layer.cornerRadius = radius
            self.layer.borderWidth = borderWidth
            self.layer.borderColor = borderColor.cgColor
        
        }
   
}

@IBDesignable
extension UITextField {

    @IBInspectable var paddingLeftCustom1: CGFloat {
        get {
            return leftView!.frame.size.width
        }
        set {
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: newValue, height: frame.size.height))
            leftView = paddingView
            leftViewMode = .always
        }
    }

    @IBInspectable var paddingRightCustom1: CGFloat {
        get {
            return rightView!.frame.size.width
        }
        set {
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: newValue, height: frame.size.height))
            rightView = paddingView
            rightViewMode = .always
        }
    }
}
