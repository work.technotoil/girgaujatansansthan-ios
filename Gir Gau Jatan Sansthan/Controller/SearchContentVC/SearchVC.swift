//
//  SearchVC.swift
//  Gir Gau Jatan Sansthan
//
//  Created by Miral Gondaliya on 12/05/20.
//  Copyright © 2020 Miral Gondaliya. All rights reserved.
//

import UIKit
import GoogleMobileAds

class SearchVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var tblList: UITableView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var viewNoData: UIView!
    var arrCategory = [NSDictionary]()
    @IBOutlet weak var viewAd: UIView!
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var lblTitle: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        var viewAd1: GADBannerView!
        viewAd1 = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait)
        viewAd1.adUnitID = "ca-app-pub-9346622311979155/7031240531"
        viewAd1.rootViewController = self
        viewAd1.translatesAutoresizingMaskIntoConstraints = false
        viewAd1.load(GADRequest())
        viewAd.addSubview(viewAd1)
        viewHeader.addShadow()
        lblTitle.text = Util.localized("Search Content")
        tblList.register(UINib(nibName: "CategoryCell", bundle: nil), forCellReuseIdentifier: "CategoryCell")
        txtSearch.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        txtSearch.attributedPlaceholder = NSAttributedString(string: txtSearch.placeholder!, attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])

        // Do any additional setup after loading the view.
    }

    @IBAction func Action(_ sender: UIButton) {
        if sender == btnBack {
            SharedAppDelegate.navigationControl.popViewController(animated: true)
        }
    }

    @objc func textFieldDidChange(_ textField: UITextField) {
        if txtSearch.text!.count > 0 {
            apiCallGetCategory()
        } else {
            self.arrCategory.removeAll()
            self.tblList.reloadData()
            self.tblList.isHidden = true
            self.viewNoData.isHidden = false

        }
    }

    func apiCallGetCategory() {
        if (Util.isInternetAvailable()) {
            SharedAppDelegate.showLoader()


            var parameter = [String: Any]()
            var strUrl = ""

            parameter = ["user_unique_id": Util.sharedInstance.user_unique_id, "user_id": Util.sharedInstance.user_id, "search_text": txtSearch.text!, "language": Util.sharedInstance.user_language]


            ServerAPIs.postRequestWithoutToken(apiUrl: apiSearch, parameter, completion: { (response, error, statusCode) in
                SharedAppDelegate.hideLoader()

                if statusCode == 200 || statusCode == 201 {


                    if response["success"].intValue == 1 {
                        if let tempData: [NSDictionary] = response["data"].arrayObject as? [NSDictionary] {

                            self.arrCategory = tempData
                            self.tblList.reloadData()
                        }
                    }


                }
                if self.arrCategory.count > 0 {
                    self.tblList.isHidden = false
                    //self.viewCategory.isHidden = true
                    //self.viewSearch.isHidden = false
                    self.viewNoData.isHidden = true

                } else {
                    self.tblList.isHidden = true
                    //self.viewCategory.isHidden = false
                    // self.viewSearch.isHidden = true
                    self.viewNoData.isHidden = false


                }
            })

        } else {
            Toast(text: Util.localized("InternetError")).show()

        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCategory.count
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var singleObj: [String: Any] = [:]
        singleObj = arrCategory[indexPath.row] as! [String: Any]


        let vc = Util.getStoryboard().instantiateViewController(withIdentifier: "MoreVC") as! MoreVC
        vc.strTitle = singleObj["content_name"] as! String
        vc.strUrl = "\(singleObj["content_ios"] as! String)"
        SharedAppDelegate.navigationControl.pushViewController(vc, animated: true)

    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let reuseIdentifire = "CategoryCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifire, for: indexPath) as! CategoryCell
        var singleObj: [String: Any] = [:]
        singleObj = arrCategory[indexPath.row] as! [String: Any]
        cell.lbl.numberOfLines = 0
        cell.lbl.frame.size.width = screenWidth - 88

        if let name = singleObj["content_name"] as? String {
            cell.lbl.text = name
            if name != "" {
                cell.lblChar.text = "\(name.first!)"

            }
        } else {
            if let name = singleObj["content_name"] as? Int {
                cell.lbl.text = "\(name)"
                cell.lblChar.text = "\("\(name)".first!)"

            }
        }

        cell.lbl.sizeToFit()
        cell.lbl.center = cell.viewMain.center
        cell.lbl.frame.origin.x = 80
        if indexPath.row % 2 == 0 {
            cell.viewMain.backgroundColor = UIColor.white
        } else {
            cell.viewMain.backgroundColor = UIColor.returnRGBColor(r: 244, g: 244, b: 244, alpha: 1.0)

        }
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        let lbl = UILabel()
        lbl.frame.size.width = screenWidth - 88
        lbl.font = UIFont.systemFont(ofSize: 17)
        var singleObj: [String: Any] = [:]
        singleObj = arrCategory[indexPath.row] as! [String: Any]

        if let name = singleObj["content_name"] as? String {
            lbl.text = name

        } else {
            if let name = singleObj["content_name"] as? Int {
                lbl.text = "\(name)"

            }
        }

        lbl.sizeToFit()

        if lbl.frame.size.height <= 30 {
            return 75

        } else {
            return 353 - 30 + lbl.frame.size.height 

        }


    } 

}


