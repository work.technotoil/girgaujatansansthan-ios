//
//  DonateViewController.swift
//  Gir Gau Jatan Sansthan
//
//  Created by mac on 20/01/21.
//  Copyright © 2021 Miral Gondaliya. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import GoogleMobileAds
import BLMultiColorLoader
import PassKit
//import Stripe
import StoreKit
class SubscribeVC: UIViewController,GADBannerViewDelegate, SKProductsRequestDelegate,SKPaymentTransactionObserver{
    

    @IBOutlet weak var imgBell: UIImageView!
    @IBOutlet weak var lblAlreadysubcribe: UILabel!
    @IBOutlet weak var lblValid: UILabel!
    @IBOutlet weak var scroll_View: UIScrollView!
    @IBOutlet weak var txtDonateAmount: SkyFloatingLabelTextField!
    @IBOutlet weak var txtName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtContactNumber: SkyFloatingLabelTextField!
    @IBOutlet weak var txtAmount: SkyFloatingLabelTextField!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var btnSubscribe: UIButton!
    @IBOutlet weak var loader: BLMultiColorLoader!
    @IBOutlet weak var viewAd: UIView!
    @IBOutlet weak var lblDetails: UILabel!
    @IBOutlet weak var view_alreadySubscribe: UIView!
    var orderId = ""
    var trans_id = ""
    var pay_Key = ""
    let razorpayKey = "rzp_test_JttNLf76TFDEmg"
    var paymentRequest: PKPaymentRequest!
    var transactionId = ""
    var status = ""
    var amount = 2200
     var product_id: NSString?
    var myproducts:SKProduct?
    var notSubscribe:String = "" 
    var subscribe:String = ""
  
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loader.isHidden = true
        product_id = "com.gaujatan.sansthan"
       // SKPaymentQueue.default().add(self)

        //razorpay = RazorpayCheckout.initWithKey("rzp_test_JttNLf76TFDEmg", andDelegate: self)
        var viewAd1: GADBannerView!
        viewAd1 = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait)
        viewAd1.adUnitID = "ca-app-pub-9346622311979155/7031240531"
        viewAd1.rootViewController = self
        viewAd1.translatesAutoresizingMaskIntoConstraints = false
        viewAd1.load(GADRequest())
        viewAd1.delegate = self
        viewAd.addSubview(viewAd1)
        btnSubscribe.layer.shadowOffset = CGSize(width: 0, height: 1)
        btnSubscribe.layer.shadowColor = UIColor.white.cgColor
        btnSubscribe.layer.shadowOpacity = 10
        btnSubscribe.layer.shadowRadius = 10
        btnSubscribe.layer.masksToBounds = false
        
        loader.lineWidth = 2
        loader.colorArray = [appColour]
        txtName.text = Util.sharedInstance.user_name
        txtContactNumber.text = Util.sharedInstance.user_phone
        //txtAmount.text = Util.sharedInstance.subscription_amount
       // lblStatus.text =  "The subscription fee for 1 year is " + " ₹" + "2499" //Util.sharedInstance.subscription_amount//Util.sharedInstance.donate_amount_message
        //lblDetails.text = Util.sharedInstance.donate_already
        //btnPost.setTitle("SUBSCRIBE", for: .normal)
        
        txtName.placeholder = Util.localized("Full Name")
        txtAmount.placeholder = Util.localized("Amount (in ₹)")
        txtContactNumber.placeholder = Util.localized("Contact Number")
        // Do any additional setup after loading the view.
       
      //  view_alreadySubscribe.isHidden = true
      //  lblDetails.isHidden = true
        btnSubscribe.isHidden = true
        //let yourView = UIView()
      
      
        if Util.sharedInstance.subscription_expiry != "" {
            //"17-03-2021"
            
            let expiryDate = Util.sharedInstance.subscription_expiry
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy"
            if Date() < dateFormatter.date(from: expiryDate) ?? Date() {
                print("Not Yet expiryDate")
                //    scroll_View.contentSize.height = lblDetails.frame.origin.y + lblDetails.frame.size.height + 20
                txtName.isHidden = true
                txtContactNumber.isHidden = true
                txtAmount.isHidden = true
                lblStatus.isHidden = true
                btnSubscribe.isHidden = true
                loader.isHidden = true
                lblDetails.isHidden = false
                view_alreadySubscribe.isHidden = false
                lblValid.text = "Your subscription is valid till"  + " " + expiryDate
             //   txtDonateAmount.isHidden = true
                
                //lblAlreadysubcribe.text = Util.localized("You have already subscribed.")
                //lblValid.text = Util.localized("Your subscription is valid till").replacingOccurrences(of: "****", with: expiryDate)
                //scroll_View.contentSize.height = view_alreadySubscribe.frame.origin.y + view_alreadySubscribe.frame.size.height + 20
                
            }
            else {
                print("expiryDate has passed")
               view_alreadySubscribe.isHidden = true
                txtName.isHidden = false
                txtContactNumber.isHidden = false
                txtAmount.isHidden = false
                lblStatus.isHidden = false
                btnSubscribe.isHidden = false
                loader.isHidden = false
                lblDetails.isHidden = true
              //  txtDonateAmount.isHidden = true
                //                //scroll_View.contentSize.height = btnSubscribe.frame.origin.y + btnSubscribe.frame.size.height + 20
                //                txtAmount.text = Util.sharedInstance.subscription_amount
            }
            //
            
        }
        
        SKPaymentQueue.default().add(self)
        }
    /*override func viewWillDisappear(_ animated: Bool) {
           
           self.loader.stopAnimation()
           self.btnSubscribe.isHidden = false
           self.loader.isHidden = false
           
       }*/
    @IBAction func action_back(_ sender: Any) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: DashboardVC.self) {
               self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    @IBAction func action_RazorpayPayment(_ sender: Any) {
         //self.loader.startAnimation()
       // self.btnSubscribe.isHidden = false
            restorePurchases()
        
        
       if (SKPaymentQueue.canMakePayments())
        {
          DispatchQueue.main.async {
            
            let productID:NSSet = NSSet(object: "com.gaujatan.sansthan");
            let productsRequest:SKProductsRequest = SKProductsRequest(productIdentifiers: productID as! Set<String>);
            productsRequest.delegate = self;
            productsRequest.start();
          }
        }
       // self.loader.stopAnimation()

        //  self.btnSubscribe.isHidden = true

    }
    
    func buyProduct(product: SKProduct) {
            print("Sending the Payment Request to Apple");
           print(product)
             let payment = SKPayment(product: product)
        SKPaymentQueue.default().add(payment)
        SKPaymentQueue.default().add(self)
        }
        
        func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse){
            print(response.products)
              
            let count : Int = response.products.count
            if (count>0)
            {
                 
                let validProduct: SKProduct = response.products[0] as SKProduct
                 print(validProduct)
                if (validProduct.productIdentifier == "com.gaujatan.sansthan")
                {
                    buyProduct(product: validProduct);
                    
                }
                else{
                    print("No Product")
                }
            
            }}
        func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]){
          for transaction:AnyObject in transactions {
                 
                if let trans:SKPaymentTransaction = transaction as? SKPaymentTransaction{
                    switch trans.transactionState {
                    case .purchased:
                        print("Product Purchased");
                        complete(transaction: transaction as! SKPaymentTransaction)
                        self.apiCallGetOrderId(payment: transaction.transactionIdentifier!)
                        break;
                    case .failed:
                        print("Purchased Failed");
                        fail(transaction: transaction as! SKPaymentTransaction)
                        break;
                    case .restored:
                        print("Already Purchased");
                        restore(transaction: transaction as! SKPaymentTransaction)
                    default:
                        break;
                    }
                }
            }

        }
        
         func restorePurchases() {
          SKPaymentQueue.default().restoreCompletedTransactions()
            
        }
    
         func complete(transaction: SKPaymentTransaction) {
            print("complete...")
            guard let productIdentifier = transaction.original?.payment.productIdentifier else { return }
            print("complete... \(productIdentifier)")
            
            SKPaymentQueue.default().finishTransaction(transaction)
            
          }
         
           func restore(transaction: SKPaymentTransaction) {
            guard let productIdentifier = transaction.original?.payment.productIdentifier else { return }
         
            print("restore... \(productIdentifier)")
           // deliverPurchaseNotificationFor(identifier: productIdentifier)
            SKPaymentQueue.default().finishTransaction(transaction)
          }
         
           func fail(transaction: SKPaymentTransaction) {
            print("fail...")
            if let transactionError = transaction.error as NSError?,
              let localizedDescription = transaction.error?.localizedDescription,
                transactionError.code != SKError.paymentCancelled.rawValue {
                print("Transaction Error: \(localizedDescription)")
              }

            SKPaymentQueue.default().finishTransaction(transaction)
          }
         
    //       func deliverPurchaseNotificationFor(identifier: String?) {
    //        guard let identifier = identifier else { return }
    //
    //       // purchasedProductIdentifiers.insert(identifier)
    //        //UserDefaults.standard.set(true, forKey: identifier)
    //      //  NotificationCenter.default.post(name: .IAPHelperPurchaseNotification, object: identifier)
    //      }
    private func request(request: SKRequest!, didFailWithError error: NSError!) {
        print("Error %@ \(error!)")
        }
   
    func apiCallGetOrderId(payment:String) {
        if (Util.isInternetAvailable()) {
            
            var parameter = [String: Any]()
            parameter = ["user_id": Util.sharedInstance.user_id, "user_unique_id": Util.sharedInstance.user_unique_id, "trans_type": "1", "trans_amount": "2499", "trans_detail_id": Util.sharedInstance.user_id]
            
            print(parameter)
            
            ServerAPIs.postRequestWithoutToken(apiUrl: apiTransactionInit, parameter, completion: { (response, error, statusCode) in
                if statusCode == 200 || statusCode == 201 {
                    self.loader.stopAnimation()
                    
                    self.loader.stopAnimation()
                    self.loader.isHidden = true
                    self.btnSubscribe.isHidden = true
                    /*
                     "data": {
                     "trans_id": 2,
                     "razor_pay_order_id": "order_EOujNiK7PYzaB7"
                     }
                     */
                    if let data = response["data"].object as? NSDictionary {
                        if let razor_pay_order_id = data["razor_pay_order_id"] as? String {
                            self.orderId = razor_pay_order_id
                            
                            
                        }
                        if let trans_id = data["trans_id"] as? Int {
                            self.trans_id = "\(trans_id)"
                            
                        }
                        if let pay_key = data["razor_pay_key"] as? String {
                            self.pay_Key = pay_key
                            
                        }
                        self.apiCallOrderTransaction(payment_id: payment)
                        
                    }
                    
                    
                }
                
            })
            
        } else {
            Toast(text: Util.localized("InternetError")).show()
            
        }
    }
    
    func apiCallOrderTransaction(payment_id: String) {
        //apiOrderTransaction
        if (Util.isInternetAvailable()) {
            // SharedAppDelegate.sdLoader.startAnimating(atView: self.view)
            
            var parameter = [String: Any]()
            var status = ""
            if payment_id == "0" {
                status = "0"
            } else {
                status = "1"
                
            }
            parameter = ["user_unique_id": Util.sharedInstance.user_unique_id, "user_id": Util.sharedInstance.user_id, "trans_number": payment_id, "trans_id": trans_id, "trans_status": status]
            
            
            print(parameter)
            ServerAPIs.postRequestWithoutToken(apiUrl: apiTransactionStatus, parameter, completion: { (response, error, statusCode) in
                //SharedAppDelegate.hideLoader()
                
                if statusCode == 200 || statusCode == 201 {
                    if response["success"].intValue == 1
                    {
                        if payment_id == "0" {
                            Toast(text: Util.localized("Transaction Failed.")).show()
                            
                            
                        } else {
                            Toast(text: Util.localized("Subscribe successfully.")).show()
                            let vc = Util.getStoryboard().instantiateViewController(withIdentifier: "DashboardVC") as! DashboardVC
                            SharedAppDelegate.navigationControl.pushViewController(vc, animated: true)
                        }
                        
                    }
                }
                
            })
            
        } else {
            Toast(text: Util.localized("InternetError")).show()
            
        }
        
    }
    
    func onPaymentError(_ code: Int32, description str: String) {
        apiCallOrderTransaction(payment_id: "0")
        
    }
    
    func onPaymentSuccess(_ payment_id: String)  {
        print("Payment Success payment id: \(payment_id)")
        apiCallOrderTransaction(payment_id: payment_id)
        
        
    }
    
    func onExternalWalletSelected(_ walletName: String, withPaymentData paymentData: [AnyHashable : Any]?) {
        
    }
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("adViewDidReceiveAd")
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }

   
    }


 


