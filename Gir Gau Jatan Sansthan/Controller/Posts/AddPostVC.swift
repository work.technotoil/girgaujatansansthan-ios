//
//  AddPostVC.swift
//  Gir Gau Jatan Sansthan
//
//  Created by Miral Gondaliya on 06/03/20.
//  Copyright © 2020 Miral Gondaliya. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import BLMultiColorLoader
import Razorpay
import GoogleMobileAds

class AddPostVC: UIViewController, UITextFieldDelegate, UITextViewDelegate, RazorpayPaymentCompletionProtocol, ExternalWalletSelectionProtocol, RazorpayPaymentCompletionProtocolWithData {
    
    @IBOutlet weak var loader: BLMultiColorLoader!
    @IBOutlet weak var viewHeader: UIView!

    @IBOutlet weak var lblError: UILabel!
    @IBOutlet weak var imgSep: UIImageView!
    @IBOutlet weak var lblFiltName: UILabel!
    @IBOutlet weak var lblInfo: UILabel!
    @IBOutlet weak var lblDetails: UILabel!
    @IBOutlet weak var btnPost: UIButton!
    @IBOutlet weak var txtNumber: SkyFloatingLabelTextField!
    @IBOutlet weak var imgAd: UIImageView!
    

    @IBOutlet weak var imgBorder: UIImageView!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var btnSelectImage: UIButton!
    @IBOutlet weak var txtDetails: UITextView!
    @IBOutlet weak var txtName: SkyFloatingLabelTextField!
    @IBOutlet weak var scrollMain: UIScrollView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    var imagePicker: UIImagePickerController = UIImagePickerController()
    var postId = ""
    var transactionId = ""
    var isImage = false

    var razorpay: RazorpayCheckout!
    var trans_id = ""
    var payKey = ""

    @IBOutlet weak var viewAd: UIView!
    var razorpayObj : RazorpayCheckout? = nil
    let defaultHeight : CGFloat = 40
    let defaultWidth : CGFloat = 120
    
    let razorpayKey = "rzp_test_JttNLf76TFDEmg" // Sign up for a Razorpay Account(https://dashboard.razorpay.com/#/access/signin) and generate the API Keys(https://razorpay.com/docs/payment-gateway/dashboard-guide/settings/#api-keys/) from the Razorpay Dashboard.

    override func viewDidLoad() {
        super.viewDidLoad()
        txtNumber.text = Util.sharedInstance.user_phone
        imgBorder.layer.cornerRadius = 5
        imgBorder.layer.borderColor = UIColor.darkGray.cgColor
        imgBorder.layer.borderWidth = 1
        lblInfo.text = Util.localized("addPOstINfo").replacingOccurrences(of: "200", with:  Util.sharedInstance.advertisement_amount)
        var viewAd1: GADBannerView!
                     viewAd1 = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait)
                     viewAd1.adUnitID = "ca-app-pub-9346622311979155/7031240531"
                     viewAd1.rootViewController = self
                     viewAd1.translatesAutoresizingMaskIntoConstraints = false
                     viewAd1.load(GADRequest())
                     viewAd.addSubview(viewAd1)
               
        
        viewHeader.addShadow()
        imagePicker.delegate = self
      //  btnSelectImage.layer.cornerRadius = 5.0
      //  btnSelectImage.clipsToBounds = true
      //  btnSelectImage.layer.borderColor = appColour.cgColor
       // btnSelectImage.layer.borderWidth = 0.7
       // btnSelectImage.setTitle(Util.localized("Select Image"), for: .normal)
        
        lblDetails.text = Util.localized("Details")
        lblTitle.text = Util.sharedInstance.add_post
        btnPost.setTitle(Util.sharedInstance.add_post, for: .normal)
        lblFiltName.text = Util.localized("Select Image")
        loader.lineWidth = 2
        loader.colorArray = [appColour]
        scrollMain.contentSize.height = btnPost.frame.origin.y + btnPost.frame.size.height + 50
        txtName.placeholder = Util.localized("Name")
        txtNumber.placeholder = Util.localized("Contact Number")

       // razorpay = Razorpay.initWithKey("rzp_test_JttNLf76TFDEmg", andDelegateWithData: self)
        if Util.sharedInstance.donate_amount_message == "-" {
            lblInfo.isHidden = true
        }
        // Do any additional setup after loading the view.
    }

    @IBAction func Actions(_ sender: UIButton) {
        if sender == btnBack {
            SharedAppDelegate.navigationControl.popViewController(animated: true)
        } else if sender == btnPost {
            self.view.endEditing(true)
            var error = false
            if Util.isStringNull(txtName.text!) {
                error = true
                txtName.errorMessage = Util.localized("Please enter name.")
            }
            if Util.isStringNull(txtNumber.text!) {
                error = true
                txtNumber.errorMessage = Util.localized("Please enter contact number.")
            } else if !Util.validatePhoneNumber(enteredPhonenumber: txtNumber.text!) {
                txtNumber.errorMessage = "Please Enter valid 10 digit Mobile Number."

            }

            if Util.isStringNull(txtDetails.text!) && isImage == false {
                error = true
                imgSep.backgroundColor = UIColor.returnRGBColor(r: 176, g: 0, b: 32, alpha: 1.0)
                lblError.text = Util.localized("Please Enter Details or selected image")
                lblError.isHidden = false
            }
            if !error {
                //Post API
                apiCallAdPost()
            }

        } else if sender == btnSelectImage {
            let alertController = UIAlertController(title: Util.localized("App Name"), message: Util.localized("Select Ad Image"), preferredStyle: .alert)

            let YES = UIAlertAction(title: Util.localized("Select From Library"), style: .default, handler: { (action) -> Void in
                    self.imagePicker.sourceType = .photoLibrary
                    self.imagePicker.allowsEditing = false
                    self.present(self.imagePicker, animated: true, completion: nil)
                })

            let NO = UIAlertAction(title: Util.localized("Take Photo"), style: .default, handler: { (action) -> Void in

                    if UIImagePickerController.isSourceTypeAvailable(.camera) {
                        self.imagePicker.sourceType = .camera
                        self.imagePicker.allowsEditing = false
                        self.present(self.imagePicker, animated: true, completion: nil)
                    }
                })
            let cancle = UIAlertAction(title: Util.localized("Cancel"), style: .cancel, handler: { (action) -> Void in


                })
            alertController.addAction(YES)
            alertController.addAction(NO)
            alertController.addAction(cancle)
            self.navigationController!.present(alertController, animated: true, completion: nil)
        }else if sender == btnDelete{
            btnDelete.isHidden = true
            btnSelectImage.isHidden = false
            imgBorder.isHidden = false
            imgAd.image = UIImage()
            isImage = false
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtNumber {
            let currentCharacterCount = textField.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount) {
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            return newLength <= 10
        }
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtNumber {
            txtNumber.errorMessage = nil
        } else if textField == txtName {
            txtName.errorMessage = nil
        }
    }

    func textViewDidBeginEditing(_ textView: UITextView) {
        imgSep.backgroundColor = appColour
        lblDetails.textColor = appColour
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        imgSep.backgroundColor = UIColor.darkGray
        lblDetails.textColor = UIColor.darkGray

    }
    func apiCallAdPost() {
        if (Util.isInternetAvailable()) {
            //  SharedAppDelegate.showLoader()

            loader.isHidden = false
            loader.startAnimation()
            self.btnPost.isHidden = true

            let parameter: [String: Any] = ["user_id": Util.sharedInstance.user_id, "user_unique_id": Util.sharedInstance.user_unique_id, "ads_name": txtName.text!, "ads_contact_no": txtNumber.text!, "ads_details": txtDetails.text!];
            var imageData = Data()
            var mimeType = ""
            if isImage {
                imageData = imgAd.image!.jpegData(compressionQuality: 1.0)!
                mimeType = "image/jpeg"
            }
            ServerAPIs.postMultipartRequestWithOptionData(apiUrl: apiAdPost, parameter, imageParameterName: "ads_image", imageData: imageData, mimeType: mimeType, fileName: "swift_file.jpeg", completion: { (response, error, statusCode) in

                if response["success"].intValue == 1 {
                    // Toast(text: Util.localized("Ad Saved Successfully.")).show()
                    // SharedAppDelegate.navigationControl.popViewController(animated: true)
                    /* "data": {
                        "ads_id": 9
                    }*/
                    if let data = response["data"].object as? NSDictionary {
                        if let ads_id = data["ads_id"] as? Int {
                            self.postId = "\(ads_id)"
                            self.apiCallGetOrderId()
                        }
                    }
                } else {

                }
            })

        } else {
        }
    }

    func apiCallGetOrderId() {
        if (Util.isInternetAvailable()) {

            var parameter = [String: Any]()
            parameter = ["user_id": Util.sharedInstance.user_id, "user_unique_id": Util.sharedInstance.user_unique_id, "trans_type": "2", "trans_amount": Util.sharedInstance.advertisement_amount, "trans_detail_id": postId]



            ServerAPIs.postRequestWithoutToken(apiUrl: apiTransactionInit, parameter, completion: { (response, error, statusCode) in
                if statusCode == 200 || statusCode == 201 {

                    self.loader.stopAnimation()
                    self.loader.isHidden = true
                    self.btnPost.isHidden = false
                    /*
                     "data": {
                         "trans_id": 2,
                         "razor_pay_order_id": "order_EOujNiK7PYzaB7"
                     }
                     */
                    if let data = response["data"].object as? NSDictionary {
                        if let razor_pay_order_id = data["razor_pay_order_id"] as? String {
                            self.transactionId = razor_pay_order_id


                            // self.showPaymentForm()

                        }
                        if let trans_id = data["trans_id"] as? Int {
                            self.trans_id = "\(trans_id)"

                        }
                       self.showPaymentForm()
                    //    self.openRazorpayCheckout()

                    }


                }

            })

        } else {
            Toast(text: Util.localized("InternetError")).show()

        }
    }

    func onPaymentError(_ code: Int32, description str: String) {

    }

    func onPaymentSuccess(_ payment_id: String) {

    }



    func onExternalWalletSelected(_ walletName: String, withPaymentData paymentData: [AnyHashable: Any]?) {

    }

    func showPaymentForm() {
        if Int(Util.sharedInstance.advertisement_amount) != nil{
            razorpayObj = RazorpayCheckout.initWithKey(razorpayKey, andDelegateWithData: self)
            razorpayObj?.setExternalWalletSelectionDelegate(self)

            let options: [AnyHashable: Any] = [
            //20000.0 \((Int(Util.sharedInstance.advertisement_amount)!) * 100)
                "prefill": [
                    "contact": txtNumber.text!,
                    "email": Util.sharedInstance.email
                    //                "method":"wallet",
                    //                "wallet":"amazonpay"
                ],
                "amount": ((Int(Util.sharedInstance.advertisement_amount)!) * 100), //This is in currency subunits. 100 = 100 paise= INR 1.
               // "currency": "INR", //We support more that 92 international currencies.
               //"description": "Gir Gau Jatan Sansthan",
                "order_id": transactionId,
               // "image": "",
            "name": "Gir Gau Jatan Sansthan",
                //"timeout":10,

                //"external" : ["wallets", "paytm"],
                "theme": [
                    "color": "#49AE44"
                ]
               // "image": "http://www.freepngimg.com/download/light/2-2-light-free-download-png.png"

            ]
            self.navigationController?.isNavigationBarHidden = true

           // razorpay.open(options, display: self)
            if let rzp = self.razorpayObj {
                       rzp.open(options)
                   } else {
                       print("Unable to initialize")
                   }
        }

        

    }
    func openRazorpayCheckout() {
        // 1. Initialize razorpay object with provided key. Also depending on your requirement you can assign delegate to self. It can be one of the protocol from RazorpayPaymentCompletionProtocolWithData, RazorpayPaymentCompletionProtocol.
        razorpayObj = RazorpayCheckout.initWithKey(razorpayKey, andDelegate: self)
        let options: [AnyHashable:Any] = [
            "prefill": [
                "contact": "1234567890",
                "email": "a@a.com"
                //                "method":"wallet",
                //                "wallet":"amazonpay"
            ],
            "image": "http://www.freepngimg.com/download/light/2-2-light-free-download-png.png",
            "amount" : 100,
            "timeout":10,
            "theme": [
                "color": "#F37254"
            ]//            "order_id": "order_B2i2MSq6STNKZV"
            // and all other options
        ]
        if let rzp = self.razorpayObj {
            rzp.open(options)
        } else {
            print("Unable to initialize")
        }
    }
    func onPaymentError(_ code: Int32, description str: String, andData response: [AnyHashable: Any]?) {
        print(response)
        //  apiCallOrderTransaction()
        apiCallOrderTransaction(payment_id: "0")

    }

    func onPaymentSuccess(_ payment_id: String, andData response: [AnyHashable: Any]?) {
        print(response)
        apiCallOrderTransaction(payment_id: payment_id)


    }

    func apiCallOrderTransaction(payment_id: String) {
        //apiOrderTransaction
        if (Util.isInternetAvailable()) {
            // SharedAppDelegate.sdLoader.startAnimating(atView: self.view)

            var parameter = [String: Any]()
            var status = ""
            if payment_id == "0" {
                status = "0"
            } else {
                status = "1"

            }
            parameter = ["user_unique_id": Util.sharedInstance.user_unique_id, "user_id": Util.sharedInstance.user_id, "trans_number": payment_id, "trans_id": trans_id, "trans_status": status]


            print(parameter)
            ServerAPIs.postRequestWithoutToken(apiUrl: apiTransactionStatus, parameter, completion: { (response, error, statusCode) in
                //SharedAppDelegate.hideLoader()

                if statusCode == 200 || statusCode == 201 {
                    if response["success"].intValue == 1
                        {
                            if payment_id == "0"{
                                Toast(text: Util.localized("Transaction Failed.")).show()
                                self.txtNumber.text = ""
                                self.txtName.text = ""
                                self.txtDetails.text = ""
                                self.isImage = false
                                self.imgAd.image = UIImage()

                            }else{
                                Toast(text: Util.localized("Ad saved successfully.")).show()
                                SharedAppDelegate.navigationControl.popViewController(animated: true)
                            }
                        
                    }



                }

            })

        } else {
            Toast(text: Util.localized("InternetError")).show()

        }

    }
}
extension AddPostVC: UINavigationControllerDelegate, UIImagePickerControllerDelegate {


    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController,
        didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any])
    {
        if let selectedImage = info[.originalImage] as? UIImage {
            self.imgAd.image = selectedImage
            self.isImage = true
            self.imgBorder.isHidden = true
            self.btnDelete.isHidden = false
            //self.createNewImage()
            self.btnSelectImage.isHidden = true
            self.dismiss(animated: true, completion: nil)

        }

    }
}
