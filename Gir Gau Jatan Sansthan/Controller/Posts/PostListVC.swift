//
//  PostListVC.swift
//  Gir Gau Jatan Sansthan
//
//  Created by Miral Gondaliya on 05/03/20.
//  Copyright © 2020 Miral Gondaliya. All rights reserved.
//

import UIKit
import Kingfisher
import GoogleMobileAds

class PostListVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var viewNoData: UIView!
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var viewCategory: UIView!
    @IBOutlet weak var tblList: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    var arrCategory = [NSDictionary]()
    var arrFilter = [NSDictionary]()
    var subCateId = ""
    var strTitle = ""
    var isSearched = false
    var isSearchedHidden = true

    //Page
    var currentPage = 1
    var isLoading = false
    var totalPage = 0
    @IBOutlet weak var viewAd: UIView!
    var refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        viewHeader.addShadow()
        tblList.register(UINib(nibName: "AdCell", bundle: nil), forCellReuseIdentifier: "AdCell")
        apiCallGetCategory()
        lblTitle.text = Util.sharedInstance.postlist
        //postlist
        var viewAd1: GADBannerView!
        viewAd1 = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait)
        viewAd1.adUnitID = "ca-app-pub-9346622311979155/7031240531"
        viewAd1.rootViewController = self
        viewAd1.translatesAutoresizingMaskIntoConstraints = false
        viewAd1.load(GADRequest())
        viewAd.addSubview(viewAd1)
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        tblList.addSubview(refreshControl) // not required when using UITableViewController

        // Do any additional setup after loading the view.
    }
    @objc func refresh(_ sender: AnyObject) {
       // Code to refresh table view
        currentPage = 1
        apiCallGetCategory()
    }

    @IBAction func Actions(_ sender: UIButton) {
        if sender == btnBack
            {
            SharedAppDelegate.navigationControl.popViewController(animated: true)
        }

    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearched {
            return arrFilter.count

        }
        return arrCategory.count
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var singleObj: [String: Any] = [:]
        singleObj = arrCategory[indexPath.row] as! [String: Any]
        if let ads_image = singleObj["ads_image"] as? String {
            if ads_image == "" {

            } else {
                let vc = Util.getStoryboard().instantiateViewController(withIdentifier: "ImageDisplayVC") as! ImageDisplayVC
                if let image = singleObj["original_main_image"] as? String {
                    vc.imgLink = image
                }
                SharedAppDelegate.navigationControl.pushViewController(vc, animated: true)

            }

        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let reuseIdentifire = "AdCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifire, for: indexPath) as! AdCell
        var singleObj: [String: Any] = [:]
        singleObj = arrCategory[indexPath.row] as! [String: Any]


        if let name = singleObj["ads_name"] as? String {
            cell.lblName.text = name

            cell.lblName.frame.size.width = screenWidth - 29

            cell.lblName.numberOfLines = 0
            cell.lblName.sizeToFit()

        } else {
            if let name = singleObj["ads_name"] as? Int {
                cell.lblName.text = "\(name)"
                cell.lblName.frame.size.width = screenWidth - 29
                cell.lblName.numberOfLines = 0
                cell.lblName.sizeToFit()

            }
        }

        if let name = singleObj["ads_details"] as? String {
            cell.lblDetails.text = name
            //   cell.lblDetails.frame.size.width = screenWidth - 132
            if let ads_image = singleObj["ads_image"] as? String {
                if ads_image == "" {
                    cell.lblDetails.frame.size.width = screenWidth - 29

                } else {
                    cell.lblDetails.frame.size.width = screenWidth - 132

                }

            }
            cell.lblDetails.numberOfLines = 0
            cell.lblDetails.sizeToFit()

        } else if let name = singleObj["ads_details"] as? Int {
            cell.lblDetails.text = "\(name)"
            if let ads_image = singleObj["ads_image"] as? String {
                if ads_image == "" {
                    cell.lblDetails.frame.size.width = screenWidth - 29

                } else {
                    cell.lblDetails.frame.size.width = screenWidth - 132

                }

            }
            cell.lblDetails.numberOfLines = 0
            cell.lblDetails.sizeToFit()

        }
        if cell.lblDetails.frame.height <= 99{
            if let ads_image = singleObj["ads_image"] as? String {
                           if ads_image == "" {
                            cell.imgSep.frame.origin.y =  cell.lblDetails.frame.height +  cell.lblDetails.frame.origin.y + 12
                           } else {
                               cell.imgSep.frame.origin.y = 123

                           }

                       }
            
            
        }else{
            cell.imgSep.frame.origin.y = cell.lblDetails.frame.origin.y + cell.lblDetails.frame.height +  12

        }
        cell.lblName.frame.origin.y = cell.imgSep.frame.origin.y + cell.imgSep.frame.height +  13

        if let image = singleObj["original_main_image"] as? String {
            cell.img.image = UIImage()
            if URL(string: image) != nil {
                let resource = ImageResource(downloadURL: URL(string: image)!, cacheKey: image)
                cell.img.kf.setImage(with: resource)
            } else {
                let original = image
                if let encoded = original.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed),
                    let url = URL(string: encoded)
                    {
                    let resource = ImageResource(downloadURL: url, cacheKey: image)
                    cell.img.kf.setImage(with: resource)
                }
            }

        }

        //ads_contact_no
        cell.btnCall.tag = indexPath.row
        if let ads_contact_no = singleObj["ads_contact_no"] as? Int {
            if ads_contact_no != nil {
                cell.btnCall.addTarget(self, action: #selector(handleCall),
                    for: .touchUpInside)
                cell.btnCall.isHidden = false

            }
        } else {
            cell.btnCall.isHidden = true
        }
        return cell
    }
    @objc func handleCall(sender: UIButton) {
        var singleObj: [String: Any] = [:]
        singleObj = arrCategory[sender.tag] as! [String: Any]

        if let ads_contact_no = singleObj["ads_contact_no"] as? Int {
            if let url = URL(string: "tel://\(ads_contact_no)"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var singleObj: [String: Any] = [:]
        singleObj = arrCategory[indexPath.row] as! [String: Any]
        let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: 17)
        if let name = singleObj["ads_name"] as? String {
            lbl.text = name
            lbl.frame.size.width = screenWidth - 29
            lbl.numberOfLines = 0
            lbl.sizeToFit()

        } else if let name = singleObj["ads_name"] as? Int {
            lbl.text = "\(name)"
            lbl.frame.size.width = screenWidth - 29
            lbl.numberOfLines = 0
            lbl.sizeToFit()

        }

        let lbl1 = UILabel()
        lbl1.font = UIFont.systemFont(ofSize: 15)
        if let name = singleObj["ads_details"] as? String {
            lbl1.text = name
            //lbl1.frame.size.width = screenWidth - 134
            if let ads_image = singleObj["ads_image"] as? String {
                if ads_image == "" {
                    lbl1.frame.size.width = screenWidth - 29

                } else {
                    lbl1.frame.size.width = screenWidth - 132

                }

            }
            lbl1.numberOfLines = 0
            lbl1.sizeToFit()

        } else if let name = singleObj["ads_details"] as? Int {
            lbl1.text = "\(name)"
            //lbl1.frame.size.width = screenWidth - 134
            if let ads_image = singleObj["ads_image"] as? String {
                if ads_image == "" {
                    lbl1.frame.size.width = screenWidth - 29

                } else {
                    lbl1.frame.size.width = screenWidth - 132

                }

            }
            lbl1.numberOfLines = 0
            lbl1.sizeToFit()

        }
       /* if let ads_image = singleObj["ads_image"] as? String {
            if ads_image == "" {
                return 90 + lbl.frame.size.height + lbl1.frame.size.height

            } else {
                return 90 + 185 + lbl.frame.size.height + lbl1.frame.size.height

            }

        }*/
/*var height1 = CGFloat()
        var height2 = CGFloat()
        if lbl.frame.size.height <= 99{
            height1 = 129
        }else{
            height1 = lbl1.frame.size.height + 30

        }
        
        if lbl1.frame.size.height <= 47{
            height2 = 123
        }else{
            height2 = lbl.frame.size.height + 28

        }
        return height1 + height2*/
        /*if lbl1.frame.size.height <= 99{
            return 184

        }else{
            return 184 - 99 + lbl1.frame.size.height + lbl.frame.size.height - 21
        }*/
         if let ads_image = singleObj["ads_image"] as? String {
            if indexPath.row == 1{
                //
                print("test")
            }
            if ads_image == "" {
                return lbl1.frame.size.height + lbl.frame.size.height + 182 - 90

            } else {
                return lbl1.frame.size.height + lbl.frame.size.height + 182

            }

        }
        return 0
    }
    //Paging
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == tblList {
            var beforeLoad: CGFloat = 0
            if IS_IPHONE_4 {
                beforeLoad = 100.0
            }
            else if IS_IPHONE_5 {
                beforeLoad = 100.0
            }
            else if IS_IPHONE_6 {
                beforeLoad = 280.0
            }
            else if IS_IPAD {
                beforeLoad = 100.0
            } else {
                beforeLoad = 300.0

            }
            if Util.isInternetAvailable() {
                if scrollView.contentOffset.y >= scrollView.contentSize.height - scrollView.frame.size.height - beforeLoad && isLoading == false && currentPage < totalPage {
                    self.currentPage = self.currentPage + 1

                    apiCallGetCategory()

                }

            }

        }
    }
    func apiCallGetCategory() {
        if (Util.isInternetAvailable()) {
            SharedAppDelegate.showLoader()
            self.isLoading = true
            if currentPage == 1 {
                SharedAppDelegate.showLoader()
                self.arrCategory = [NSDictionary]()

                tblList.reloadData()

            }

            var parameter = [String: Any]()
            var strUrl = ""

            parameter = ["user_unique_id": Util.sharedInstance.user_unique_id, "user_id": Util.sharedInstance.user_id, "page": currentPage]

            ServerAPIs.postRequestWithoutToken(apiUrl: apiAdList, parameter, completion: { (response, error, statusCode) in
                SharedAppDelegate.hideLoader()
                self.refreshControl.endRefreshing()

                if statusCode == 200 || statusCode == 201 {


                    if response["success"].intValue == 1 {
                        if let tempData: [NSDictionary] = response["data"].arrayObject as? [NSDictionary] {

                            self.arrCategory = tempData
                            self.tblList.reloadData()
                        }
                    }


                }
                //total_page
                if let total_page = response["total_page"].object as? Int {
                    self.totalPage = total_page
                }
                if self.arrCategory.count > 0 {
                    self.tblList.isHidden = false
                    self.viewCategory.isHidden = true
                    //self.viewSearch.isHidden = false
                    self.viewNoData.isHidden = true

                } else {
                    self.tblList.isHidden = true
                    self.viewCategory.isHidden = false
                    // self.viewSearch.isHidden = true
                    self.viewNoData.isHidden = false


                }
                self.isLoading = false


            })

        } else {
            Toast(text: Util.localized("InternetError")).show()

        }
    }




}
