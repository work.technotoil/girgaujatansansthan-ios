//
//  ImageDisplayVC.swift
//  Gir Gau Jatan Sansthan
//
//  Created by Miral Gondaliya on 11/05/20.
//  Copyright © 2020 Miral Gondaliya. All rights reserved.
//

import UIKit
import Kingfisher

class ImageDisplayVC: UIViewController {
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var img: UIImageView!
    var imgLink = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        if let image = imgLink as? String {
                   img.image = UIImage()
                   if URL(string: image) != nil {
                       let resource = ImageResource(downloadURL: URL(string: image)!, cacheKey: image)
                       img.kf.setImage(with: resource)
                   } else {
                       let original = image
                       if let encoded = original.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed),
                           let url = URL(string: encoded)
                           {
                           let resource = ImageResource(downloadURL: url, cacheKey: image)
                           img.kf.setImage(with: resource)
                       }
                   }

               }
        // Do any additional setup after loading the view.
    }
    
    @IBAction func Action(_ sender: Any) {
        SharedAppDelegate.navigationControl.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
