//
//  HomeVC.swift
//  LPBIZ
//
//  Created by miral on 26/04/19.
//  Copyright © 2019 APT Solution. All rights reserved.
//

import UIKit

class HomeVC: UIViewController {
    @IBOutlet var viewBottom: UIView!
    @IBOutlet var tableView: UITableView!
    var arrStory = [NSDictionary]()

    override func viewDidLoad() {
        super.viewDidLoad()
        viewBottom.frame.origin.y =  screenHeight
        UIView.animate(withDuration: 1.0, animations: {
            var viewAnimate1Frame: CGRect = self.viewBottom.frame
           // viewAnimate1Frame.origin.y = screenHeight - self.viewBottom.frame.size.height
            self.viewBottom.center = CGPoint(x: screenWidth  / 2,
            y: screenHeight / 2)

           // self.viewBottom.frame = viewAnimate1Frame
        }){ (completion) in
            UIView.animate(withDuration: 5) {
                if !Util.isLogin(){
                    let vc = Util.getStoryboard().instantiateViewController(withIdentifier: "ViewController") as! ViewController
                    SharedAppDelegate.navigationControl.pushViewController(vc, animated: true)
                }else{
                    //Logined Home
                    let vc = Util.getStoryboard().instantiateViewController(withIdentifier: "DashboardVC") as! DashboardVC
                    SharedAppDelegate.navigationControl.pushViewController(vc, animated: true)
                  //  SharedAppDelegate.registerRemoteNoti()


                }
            }
        }
        
        
        // Do any additional setup after loading the view.
    }
    
    @objc func buttonTapped() {
        print("button was tapped")
        
        
        
    }



}
