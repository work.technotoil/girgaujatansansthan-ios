//
//  ViewController.swift
//  Gir Gau Jatan Sansthan
//
//  Created by Miral Gondaliya on 03/03/20.
//  Copyright © 2020 Miral Gondaliya. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseUI
import SkyFloatingLabelTextField
import BLMultiColorLoader

class ViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var view1: UIView!
    
    @IBOutlet weak var img1: UIImageView!
    
    @IBOutlet weak var txtMobile: SkyFloatingLabelTextField!
    @IBOutlet weak var txtCode: SkyFloatingLabelTextField!
    
    @IBOutlet weak var btnLogin: UIButton!
    
    @IBOutlet weak var loader: BLMultiColorLoader!
    
    @IBOutlet weak var btn_back: UIButton!

    var skiplogin:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view1.addShadow()
        img1.alpha = 0.5
        
        loader.lineWidth = 2
        loader.colorArray = [appColour]
        
        if (skiplogin == true)
        {
            self.btn_back.isHidden = false;
        }else
        {
            self.btn_back.isHidden = true;
        }
        
        
        /* let phoneNumber = "+917874181282"
         SharedAppDelegate.showSideMenu()
         let testVerificationCode = "123456"
         
         Auth.auth().settings?.isAppVerificationDisabledForTesting = false
         PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) {
         verificationID, error in
         if ((error) != nil) {
         // Handles error
         return
         }
         let credential = PhoneAuthProvider.provider().credential(withVerificationID: verificationID ?? "",
         verificationCode: testVerificationCode)
         Auth.auth().signInAndRetrieveData(with: credential) { authData, error in
         if ((error) != nil) {
         // Handles error
         return
         }
         
         
         }
         }*/
    }
    
    @IBAction func action_skip(_ sender: Any) {
        let obj_skip = storyboard?.instantiateViewController(withIdentifier: "DashboardVC") as! DashboardVC
        self.navigationController?.pushViewController(obj_skip, animated: true)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.txtCode.text = "91"
    }
    
    
    @IBAction func action_back(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func Action(_ sender: Any) {
        
        if Util.isStringNull(txtCode.text!) {
            txtCode.errorMessage = "Please Enter Country Code."
        }
        else if Util.isStringNull(txtMobile.text!) {
            txtMobile.errorMessage = "Please Enter Mobile Number."
        } else if !Util.validatePhoneNumber(enteredPhonenumber: txtMobile.text!) {
            txtMobile.errorMessage = "Please Enter valid 10 digit Mobile Number."
            
        } else {
            self.view.endEditing(true)
            apiCallLogin()
            
        }
        
    }
    func authUI(_ authUI: FUIAuth, didSignInWith authDataResult: AuthDataResult?, error: Error?) {
        
        // Do something with the response
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtMobile {
            let currentCharacterCount = textField.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount) {
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            return newLength <= 10
        }
        else if textField == txtCode {
            
            let currentCharacterCount = textField.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount) {
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            return newLength <= 3
            
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtMobile{
            txtMobile.errorMessage = nil
        }
    }
    func apiCallLogin() {
        if (Util.isInternetAvailable()) {
            //   SharedAppDelegate.showLoader()
            loader.isHidden = false
            loader.startAnimation()
            self.btnLogin.isHidden = true
            
            let number = "+\(self.txtCode.text!)\(self.txtMobile.text!)"
            
            let parameter: [String: Any] = ["user_phone": number ?? ""];
            
            ServerAPIs.postRequestWithoutToken(apiUrl: apiLogin, parameter, completion: { (response, error, statusCode) in
                //  SharedAppDelegate.hideLoader()
                
                if response["success"].intValue == 1 {
                    //self.storeToDefault(data: response)
                    UserDefaults.standard.removeObject(forKey: "subscribe")
                    SharedAppDelegate.strMobile = self.txtMobile.text!
                    let phoneNumber = "+\(self.txtCode.text!)\(self.txtMobile.text!)"
                    let testVerificationCode = "123456"
                    var verificationID1 = ""
                    
                    Auth.auth().settings?.isAppVerificationDisabledForTesting = false
                    PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) {
                        verificationID, error in
                        print(verificationID)
                        self.loader.stopAnimation()
                        self.loader.isHidden = true
                        self.btnLogin.isHidden = false
                        
                        if verificationID != nil{
                            verificationID1 = verificationID ?? ""
                            
                        }
                        if ((error) != nil) {
                            Toast(text: "Somthing went wrong. Try again.").show()
                            
                            // Handles error
                        }else{
                            if (response["data"]["user_id"].intValue as? Int) != nil {
                                if response["data"]["user_id"].intValue == 0 {
                                    //USer Not Available
                                    print(0)
                                   let vc = Util.getStoryboard().instantiateViewController(withIdentifier: "RegisterVC") as! RegisterVC
                                    vc.verificationId = verificationID1
                                    Util.setUserDefaultValue(key:"country_code", value:self.txtCode.text!)
                                    SharedAppDelegate.navigationControl.pushViewController(vc, animated: true)
                                } else {
                                    //USer Available
                                    print(1)
                                    /*
                                     {
                                     "success": 1,
                                     "message": "Login Successfully",
                                     "data": {
                                     "user_id": 12,
                                     "user_unique_id": null,
                                     "user_name": "Pradip Dobariya",
                                     "user_language": "hi",
                                     "user_city": "rajkot",
                                     "user_password": null,
                                     "user_sweets": null,
                                     "user_phone": 9824845095,
                                     "user_email": null,
                                     "user_otp_status": 0,
                                     "user_otp": null,
                                     "user_fcm": null,
                                     "user_address": null,
                                     "user_details": null,
                                     "user_phone_verified": 0,
                                     "user_subscription": 0,
                                     "user_subscription_details": null,
                                     "user_status": 1,
                                     "is_delete": 0,
                                     "created_at": "2020-03-04 12:40:03",
                                     "updated_at": null
                                     }
                                     }
                                     */
                                    SharedAppDelegate.dataUser = response["data"].object as! NSDictionary
                                    print(SharedAppDelegate.dataUser)
                                    
                                    let vc = Util.getStoryboard().instantiateViewController(withIdentifier: "LoginStep2VC") as! LoginStep2VC
                                    vc.verificationId = verificationID1
                                    SharedAppDelegate.navigationControl.pushViewController(vc, animated: true)
                                    
                                }
                                
                            }
                        }
                        /*let credential = PhoneAuthProvider.provider().credential(withVerificationID: verificationID ?? "",
                         verificationCode: testVerificationCode)
                         Auth.auth().signInAndRetrieveData(with: credential) { authData, error in
                         if ((error) != nil) {
                         // Handles error
                         return
                         }
                         
                         
                         }*/
                    }
                    
                    
                } else {
                    Toast(text: response["message"].stringValue).show()
                    
                    self.loader.stopAnimation()
                    self.loader.isHidden = true
                    self.btnLogin.isHidden = false
                }
            })
            
        } else {
            Toast(text: Util.localized("InternetError")).show()
        }
    }
}
