//
//  LoginStep2VC.swift
//  Gir Gau Jatan Sansthan
//
//  Created by Miral Gondaliya on 04/03/20.
//  Copyright © 2020 Miral Gondaliya. All rights reserved.
//

import UIKit
import FirebaseAuth
import BLMultiColorLoader

class LoginStep2VC: UIViewController {
    @IBOutlet weak var pin: UIView!
    var config: PinConfig! = PinConfig()
    var otpView: PHOTPView!
    @IBOutlet weak var view1: UIView!

    @IBOutlet weak var btnResend: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var img1: UIImageView!
    @IBOutlet weak var btnLogin: UIButton!
    var enteredOtp: String = ""
    @IBOutlet weak var loader: BLMultiColorLoader!
    @IBOutlet weak var loader2: BLMultiColorLoader!

    var verificationId = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        setupVarificationPINView()
        view1.addShadow()
        img1.alpha = 0.5
        loader.lineWidth = 2
        loader.colorArray = [appColour]
        
        loader2.lineWidth = 2
        loader2.colorArray = [appColour]


        // Do any additional setup after loading the view.
    }
    func setupVarificationPINView()
    {
        config.otpFieldDisplayType = .square
        config.otpFieldSeparatorSpace = 10
        config.otpFieldSize = 35
        config.otpFieldsCount = 6
        config.otpFieldDefaultBorderColor = UIColor.gray
        config.otpFieldEnteredBorderColor = appColour
        config.otpFieldErrorBorderColor = appColour
        config.otpFieldBorderWidth = 2
        config.shouldAllowIntermediateEditing = false

        otpView = PHOTPView(config: config)
        otpView.delegate = self
        otpView.frame = pin.frame
        otpView.frame.origin.y = 0
        otpView.frame.origin.x = 0
        pin.addSubview(otpView)
        otpView.initializeUI()


    }


    @IBAction func Action(_ sender: UIButton) {

        if sender == btnBack {
            SharedAppDelegate.navigationControl.popViewController(animated: true)
        } else if sender == btnResend {
            loader2.isHidden = false
            loader2.startAnimation()
            self.btnResend.isHidden = true

            Auth.auth().settings?.isAppVerificationDisabledForTesting = true
            
            let phoneNumber = "+\(Util.getUserDefaultValue(key:"country_code"))\(SharedAppDelegate.strMobile)"

            var verificationID1 = ""
            PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) {
                verificationID, error in
                print(verificationID)
                self.loader2.stopAnimation()
                self.loader2.isHidden = true
                self.btnResend.isHidden = false

                if verificationID != nil {
                    verificationID1 = verificationID ?? ""

                }
                if ((error) != nil) {
                    // Handles error
                } else {
                                        Toast(text: "OTP Send Successfully!").show()

                }

            }
        } else {
            if enteredOtp.count == 6 {
                loader.isHidden = false
                loader.startAnimation()
                self.btnLogin.isHidden = true

                let credential = PhoneAuthProvider.provider().credential(withVerificationID: verificationId ?? "",
                    verificationCode: enteredOtp)
                Auth.auth().signInAndRetrieveData(with: credential) { authData, error in

                    if ((error) != nil) {
                        // Handles error
                        print(error)
                        Toast(text: "OTP Invalid.").show()
                        return
                    } else {
                        self.apiCallLoginVerify()
                    }


                }
            }
        }
    }
    
    func apiCallLoginVerify(){
        if (Util.isInternetAvailable()) {
            //   SharedAppDelegate.showLoader()
            let parameter: [String: Any] = ["user_phone": SharedAppDelegate.strMobile,"user_verify": "1","user_fcm": Util.sharedInstance.FCMToken,"user_device" : "ios"];

            ServerAPIs.postRequestWithoutToken(apiUrl: apiLogin, parameter, completion: { (response, error, statusCode) in
                //  SharedAppDelegate.hideLoader()
                self.loader.stopAnimation()
                self.loader.isHidden = true
                self.btnLogin.isHidden = false

                if response["success"].intValue == 1 {
                    Util.setUserDetails()
                    print("Successfully veryfied.")
                    
                    let vc = Util.getStoryboard().instantiateViewController(withIdentifier: "DashboardVC") as! DashboardVC
                    SharedAppDelegate.navigationControl.pushViewController(vc, animated: true)

                } else {
                    Toast(text: response["message"].stringValue).show()



                }
            })

        } else {
           Toast(text: Util.localized("InternetError")).show()
        }
    }
}
extension LoginStep2VC: PHOTPViewDelegate
{
    func hasEnteredAllOTP(hasEntered: Bool) -> Bool
    {
        print("Has entered all OTP? \(hasEntered)")
        return true
    }

    func shouldBecomeFirstResponderForOTP(otpFieldIndex index: Int) -> Bool
    {
        return true
    }

    func getenteredOTP(otpString: String) {
        enteredOtp = otpString
    }
}
