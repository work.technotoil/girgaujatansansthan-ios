//
//  LogoutVC.swift
//  Vyaparam
//
//  Created by Miral Gondaliya on 05/11/19.
//  Copyright © 2019 Miral Gondaliya. All rights reserved.
//

import UIKit
import EzPopup
import BLMultiColorLoader

class LogoutVC: UIViewController {
    @IBOutlet weak var loader: BLMultiColorLoader!

    @IBOutlet weak var lbl2: UILabel!
    @IBOutlet weak var lbl1: UILabel!
    @IBOutlet weak var btnClose1: UIButton!
    @IBOutlet weak var btnOk: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        btnCancel.layer.cornerRadius = 4.0
        btnCancel.layer.borderColor = appGrayColor.cgColor
        btnCancel.layer.borderWidth = 0.7
        loader.lineWidth = 2
        loader.colorArray = [appColour]

        btnOk.layer.cornerRadius = 4.0
        lbl1.text = Util.localized("LogoutInstrusction")
        lbl2.text = Util.localized("Logout")
        btnCancel.setTitle(Util.localized("Cancel").uppercased(), for: .normal)
        btnOk.setTitle(Util.localized("Logout").uppercased(), for: .normal)
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func Actions(_ sender: UIButton) {
        if sender == btnOk{
            apiCallLogout()

        }else{
            self.dismiss(animated: true, completion: nil)

        }
    }
    
   func apiCallLogout() {
        if (Util.isInternetAvailable()){
            
            var parameter = [String: Any]()
         parameter = ["user_phone" : Util.sharedInstance.user_phone]
            loader.isHidden = false
            loader.startAnimation()
            self.btnOk.isHidden = true

        
            ServerAPIs.postRequestWithoutToken(apiUrl: apiLogout, parameter, completion: { (response, error, statusCode) in
                
                if statusCode == 200 || statusCode == 201 {

                    self.loader.stopAnimation()
                    self.loader.isHidden = true
                    self.btnOk.isHidden = false
                 Util.resetDefault()
                SharedAppDelegate.doSingleTap()
                    self.dismiss(animated: true, completion: nil)

                let vc = Util.getStoryboard().instantiateViewController(withIdentifier: "ViewController") as! ViewController
                SharedAppDelegate.navigationControl.pushViewController(vc, animated: true)
                   
                    
                }
             
            })
            
        }else{
             Toast(text: Util.localized("InternetError")).show()
            
        }
    }
}
