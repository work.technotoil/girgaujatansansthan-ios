//
//  RegisterVC.swift
//  Gir Gau Jatan Sansthan
//
//  Created by Miral Gondaliya on 04/03/20.
//  Copyright © 2020 Miral Gondaliya. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import FirebaseAuth
import BLMultiColorLoader

class RegisterVC: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {

    var verificationId = ""
    @IBOutlet weak var loader: BLMultiColorLoader!
    @IBOutlet weak var loader2: BLMultiColorLoader!

    @IBOutlet weak var btnResend: UIButton!
    @IBOutlet weak var pin: UIView!
    var config: PinConfig! = PinConfig()
    var otpView: PHOTPView!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var txtLanguage: SkyFloatingLabelTextField!
    @IBOutlet weak var txtName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtCity: SkyFloatingLabelTextField!

    @IBOutlet weak var scrollMain: UIScrollView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var img1: UIImageView!
    @IBOutlet weak var btnLogin: UIButton!
    var enteredOtp: String = ""
    var pickerLanguage = UIPickerView()
    var language = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        setupVarificationPINView()
        loader.lineWidth = 2
        loader.colorArray = [appColour]

        loader2.lineWidth = 2
        loader2.colorArray = [appColour]

        view1.addShadow()
        img1.alpha = 0.5
        pickerLanguage.delegate = self
        pickerLanguage.dataSource = self
        scrollMain.contentSize.height = view1.frame.size.height + view1.frame.origin.y + 20
        language = ["English", "ગુજરાતી", "हिन्दी"]
        createTypePicker()
        txtLanguage.text = "English"
        // Do any additional setup after loading the view.
    }
    func createTypePicker() {
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let buttonDone = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressedCountry))
        toolbar.setItems([buttonDone], animated: false)
        txtLanguage?.inputAccessoryView = toolbar
        txtLanguage?.inputView = pickerLanguage

    }
    @objc func donePressedCountry() {
        self.view.endEditing(true)
    }
    func setupVarificationPINView()
    {
        config.otpFieldDisplayType = .square
        config.otpFieldSeparatorSpace = 10
        config.otpFieldSize = 40
        config.otpFieldsCount = 6
        config.otpFieldDefaultBorderColor = UIColor.gray
        config.otpFieldEnteredBorderColor = appColour
        config.otpFieldErrorBorderColor = appColour
        config.otpFieldBorderWidth = 2
        config.shouldAllowIntermediateEditing = false

        otpView = PHOTPView(config: config)
        otpView.delegate = self
        otpView.frame = pin.frame
        otpView.frame.origin.y = 0
        otpView.frame.origin.x = 0
        pin.addSubview(otpView)
        otpView.initializeUI()


    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 3

    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {

        return language[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        txtLanguage.text = language[row]
    }
    @IBAction func Action(_ sender: UIButton) {

        if sender == btnBack {
            SharedAppDelegate.navigationControl.popViewController(animated: true)
        }else if sender == btnResend{
            let phoneNumber = "+\(Util.getUserDefaultValue(key:"country_code"))\(SharedAppDelegate.strMobile)"
            loader2.isHidden = false
            loader2.startAnimation()
            self.btnResend.isHidden = true
             Auth.auth().settings?.isAppVerificationDisabledForTesting = true
            var verificationID1 = ""
             PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) {
                 verificationID, error in
                 print(verificationID)
                 self.loader2.stopAnimation()
                 self.loader2.isHidden = true
                 self.btnResend.isHidden = false

                 if verificationID != nil{
                     verificationID1 = verificationID ?? ""

                 }
                 if ((error) != nil) {
                     // Handles error
                 }else{
                    Toast(text: "OTP Send Successfully!").show()
                 }
                 
             }
        } else {
            var error = false
            self.view.endEditing(true)
            if enteredOtp.count != 6 {
                error = true

            }
            if Util.isStringNull(txtName.text!) {
                txtName.errorMessage = "Please enter name."
                error = true
            }
            if Util.isStringNull(txtCity.text!) {
                txtCity.errorMessage = "Please enter city."
                error = true
            }
            if Util.isStringNull(txtLanguage.text!) {
                txtLanguage.errorMessage = "Please select Language."
                error = true
            }
            
            if !error{
                if enteredOtp.count == 6{
                    let credential = PhoneAuthProvider.provider().credential(withVerificationID: verificationId ?? "",
                                               verificationCode: enteredOtp)
                                           Auth.auth().signInAndRetrieveData(with: credential) { authData, error in
                                               if ((error) != nil) {
                                                   // Handles error
                                                Toast(text: "OTP invalid.").show()

                                                print(error)
                                                   return
                                               }else{
                                                print("Successfully veryfied.")
                                                self.apiCallLogin()
                                                
                                            }


                                           }
                }else{
                    Toast(text: "Please enter valid code.").show()
                }
            }
        }
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtCity {
            txtCity.errorMessage = nil
        } else if textField == txtName {
            txtName.errorMessage = nil
        } else if textField == txtLanguage {
            txtLanguage.errorMessage = nil
        }
    }

    func apiCallLogin() {
        if (Util.isInternetAvailable()) {
            loader.isHidden = false
                       loader.startAnimation()
                       self.btnLogin.isHidden = true
            //   SharedAppDelegate.showLoader()
            var lang = ""
            if txtLanguage.text == "English" {
                lang = "en"
            } else if txtLanguage.text == "हिन्दी" {
                lang = "hi"
            } else if txtLanguage.text == "ગુજરાતી" {
                lang = "gj"
            }
            let parameter: [String: Any] = ["user_language": lang, "user_name": txtName.text!, "user_city": txtCity.text!, "user_phone": "+\(Util.getUserDefaultValue(key:"country_code"))\(SharedAppDelegate.strMobile)","user_fcm" : Util.sharedInstance.FCMToken, "user_device" : "ios"];

            ServerAPIs.postRequestWithoutToken(apiUrl: apiRegister, parameter, completion: { (response, error, statusCode) in
                //  SharedAppDelegate.hideLoader()
                self.loader.stopAnimation()
                self.loader.isHidden = true
                self.btnLogin.isHidden = false

                if response["success"].intValue == 1 {
                    SharedAppDelegate.dataUser = response["data"].object as! NSDictionary
                    print(SharedAppDelegate.dataUser)
                    //self.storeToDefault(data: response)
                    /*
                     {
                         "success": 1,
                         "message": "Login Successfully",
                         "data": {
                             "user_id": 12,
                             "user_unique_id": null,
                             "user_name": "Pradip Dobariya",
                             "user_language": "hi",
                             "user_city": "rajkot",
                             "user_password": null,
                             "user_sweets": null,
                             "user_phone": 9824845095,
                             "user_email": null,
                             "user_otp_status": 0,
                             "user_otp": null,
                             "user_fcm": null,
                             "user_address": null,
                             "user_details": null,
                             "user_phone_verified": 0,
                             "user_subscription": 0,
                             "user_subscription_details": null,
                             "user_status": 1,
                             "is_delete": 0,
                             "created_at": "2020-03-04 12:40:03",
                             "updated_at": null
                         }
                     }
                     */
                    
                    Util.setUserDetails()
                    let vc = Util.getStoryboard().instantiateViewController(withIdentifier: "DashboardVC") as! DashboardVC
                    SharedAppDelegate.navigationControl.pushViewController(vc, animated: true)



                } else {
                    Toast(text: response["message"].stringValue).show()



                }
            })

        } else {
           Toast(text: Util.localized("InternetError")).show()
        }
    }
}
extension RegisterVC: PHOTPViewDelegate
{
    func hasEnteredAllOTP(hasEntered: Bool) -> Bool
    {
        print("Has entered all OTP? \(hasEntered)")
        return true
    }

    func shouldBecomeFirstResponderForOTP(otpFieldIndex index: Int) -> Bool
    {
        return true
    }

    func getenteredOTP(otpString: String) {
        enteredOtp = otpString
    }
}
