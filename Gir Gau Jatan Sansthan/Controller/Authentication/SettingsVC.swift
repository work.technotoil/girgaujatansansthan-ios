//
//  SettingsVC.swift
//  Gir Gau Jatan Sansthan
//
//  Created by Miral Gondaliya on 05/03/20.
//  Copyright © 2020 Miral Gondaliya. All rights reserved.
//

import UIKit
import BLMultiColorLoader
import GoogleMobileAds

class SettingsVC: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource {
   
    
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var loader: BLMultiColorLoader!

    @IBOutlet weak var lblSelctedLanguage: UILabel!
    @IBOutlet weak var lbl1: UILabel!
    @IBOutlet weak var btnDropDown: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var imgSave: UIImageView!

    var strLanguage = "gj"
    var toolBar = UIToolbar()
    var picker  = UIPickerView()
    var language = [String]()
    @IBOutlet weak var viewAd: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        var viewAd1: GADBannerView!
                     viewAd1 = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait)
                     viewAd1.adUnitID = "ca-app-pub-9346622311979155/7031240531"
                     viewAd1.rootViewController = self
                     viewAd1.translatesAutoresizingMaskIntoConstraints = false
                     viewAd1.load(GADRequest())
                     viewAd.addSubview(viewAd1)
               
        viewHeader.addShadow()
        lblTitle.text = Util.localized("Settings")
        lbl1.text = Util.localized("Selected Language")
        loader.lineWidth = 2
        loader.colorArray = [appColour]
       language = ["English", "ગુજરાતી", "हिन्दी", "संस्कृत", "मराठी", "ಕನ್ನಡ", "తెలుగు", "മലയാളം" , "தமிழ்" ,"ਪੰਜਾਬੀ", "বাংলা" ,"उड़िया"]
         if UserDefaults.standard.string(forKey: "user_language") != nil{
                   if UserDefaults.standard.string(forKey: "user_language") == "en"{
                       lblSelctedLanguage.text = "English"
                   }else if UserDefaults.standard.string(forKey: "user_language") == "hi"{
                       lblSelctedLanguage.text = "हिन्दी"
                   }else if UserDefaults.standard.string(forKey: "user_language") == "gj"{
                       lblSelctedLanguage.text = "ગુજરાતી"
                   }
                   else if UserDefaults.standard.string(forKey: "user_language") == "sanskrut"{
                       lblSelctedLanguage.text = "संस्कृत"
                              }
                   else if UserDefaults.standard.string(forKey: "user_language") == "marathi"{
                       lblSelctedLanguage.text = "मराठी"
                              }
                   else if UserDefaults.standard.string(forKey: "user_language") == "kannad"{
                       lblSelctedLanguage.text = "ಕನ್ನಡ"
                              }
                   else if UserDefaults.standard.string(forKey: "user_language") == "telugu"{
                       lblSelctedLanguage.text = "తెలుగు"
                              }
                   else if UserDefaults.standard.string(forKey: "user_language") == "malyalm"{
                       lblSelctedLanguage.text = "മലയാളം"
                              }
                   else if UserDefaults.standard.string(forKey: "user_language") == "tamil"{
                       lblSelctedLanguage.text = "தமிழ்"
                              }
                   else if UserDefaults.standard.string(forKey: "user_language") == "punjabi"{
                           lblSelctedLanguage.text = "ਪੰਜਾਬੀ"
                              }
                   else if UserDefaults.standard.string(forKey: "user_language") == "bengali"{
                                 lblSelctedLanguage.text = "বাংলা"
                              }
                   else if UserDefaults.standard.string(forKey: "user_language") == "udiya"{
                                 lblSelctedLanguage.text = "उड़िया"
                           }
        }else{
        }

        // Do any additional setup after loading the view.
    }

    @IBAction func Action(_ sender: UIButton) {
        if sender == btnBack {
            SharedAppDelegate.navigationControl.popViewController(animated: true)
        } else if sender == btnDropDown{
            picker = UIPickerView.init()
            picker.delegate = self
            picker.dataSource = self
            picker.autoresizingMask = .flexibleWidth
            picker.contentMode = .center
            picker.frame = CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 300)
            self.view.addSubview(picker)

            toolBar = UIToolbar.init(frame: CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 50))
            toolBar.items = [UIBarButtonItem.init(title: "Done", style: .done, target: self, action: #selector(onDoneButtonTapped))]
            self.view.addSubview(toolBar)

        }else {
            apiCallChangeLanguage()
        }
    }
    
    @objc func onDoneButtonTapped() {
        toolBar.removeFromSuperview()
        picker.removeFromSuperview()
    }
    
    func apiCallChangeLanguage() {
        if (Util.isInternetAvailable()) {

            var parameter = [String: Any]()
           if lblSelctedLanguage.text == "English" {
                strLanguage = "en"
            } else if lblSelctedLanguage.text == "हिन्दी" {
                strLanguage = "hi"
            } else if lblSelctedLanguage.text == "ગુજરાતી" {
                strLanguage = "gj"
            }
            else if lblSelctedLanguage.text == "संस्कृत" {
                strLanguage = "sanskrut"
            }
            else if lblSelctedLanguage.text == "मराठी" {
                strLanguage = "marathi"
            }
            else if lblSelctedLanguage.text == "ಕನ್ನಡ" {
                strLanguage = "kannad"
            }
            else if lblSelctedLanguage.text == "తెలుగు" {
                strLanguage = "telugu"
            }
            else if lblSelctedLanguage.text == "മലയാളം" {
                strLanguage = "malyalm"
            }
            else if lblSelctedLanguage.text == "தமிழ்" {
                strLanguage = "tamil"
            }
            else if lblSelctedLanguage.text == "ਪੰਜਾਬੀ" {
                strLanguage = "punjabi"
            }
            else if lblSelctedLanguage.text == "বাংলা" {
                strLanguage = "bengali"
            }
            else if lblSelctedLanguage.text == "उड़िया" {
                strLanguage = "udiya"
                      }
            parameter = ["user_id": Util.sharedInstance.user_id, "user_unique_id": Util.sharedInstance.user_unique_id, "user_language": strLanguage]
            loader.isHidden = false
            loader.startAnimation()
            self.btnSave.isHidden = true
            self.imgSave.isHidden = true

            ServerAPIs.postRequestWithoutToken(apiUrl: apiChangeLanguage, parameter, completion: { (response, error, statusCode) in
                if statusCode == 200 || statusCode == 201 {

                    self.loader.stopAnimation()
                    self.loader.isHidden = true
                    self.btnSave.isHidden = false
                    self.imgSave.isHidden = false
                    if let data = response["data"].object as? NSDictionary {
                        if let abc = data["user_language"] as? String {
                            Util.sharedInstance.user_language = "\(abc)"
                            Util.setUserDefaultValue(key: "user_language", value: "\(abc)")
                            if UserDefaults.standard.string(forKey: "user_language") == "en"{
                                SharedAppDelegate.localization.setLanguage("EN")

                            }else if UserDefaults.standard.string(forKey: "user_language") == "hi"{
                                SharedAppDelegate.localization.setLanguage("HI")

                            }else if UserDefaults.standard.string(forKey: "user_language") == "gj"{
                                SharedAppDelegate.localization.setLanguage("GJ")

                            }
                            else if UserDefaults.standard.string(forKey: "user_language") == "sanskrut"{
                                SharedAppDelegate.localization.setLanguage("sanskrut")

                            }
                            else if UserDefaults.standard.string(forKey: "user_language") == "marathi"{
                                SharedAppDelegate.localization.setLanguage("marathi")

                            }
                            else if UserDefaults.standard.string(forKey: "user_language") == "kannad"{
                                SharedAppDelegate.localization.setLanguage("kannad")

                            }
                            else if UserDefaults.standard.string(forKey: "user_language") == "telugu"{
                                SharedAppDelegate.localization.setLanguage("telugu")

                            }
                            else if UserDefaults.standard.string(forKey: "user_language") == "malyalm"{
                                SharedAppDelegate.localization.setLanguage("malyalm")

                            }
                            else if UserDefaults.standard.string(forKey: "user_language") == "tamil"{
                                SharedAppDelegate.localization.setLanguage("tamil")

                            }
                            else if UserDefaults.standard.string(forKey: "user_language") == "punjabi"{
                                SharedAppDelegate.localization.setLanguage("punjabi")

                            }
                            else if UserDefaults.standard.string(forKey: "user_language") == "bengali"{
                                SharedAppDelegate.localization.setLanguage("bengali")

                            }
                            else if UserDefaults.standard.string(forKey: "user_language") == "udiya"{
                                SharedAppDelegate.localization.setLanguage("udiya")

                            }
                        }
                    }

                    let vc = Util.getStoryboard().instantiateViewController(withIdentifier: "DashboardVC") as! DashboardVC
                    SharedAppDelegate.navigationControl.pushViewController(vc, animated: true)

                }

            })

        } else {
            Toast(text: Util.localized("InternetError")).show()

        }
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
           return 1
       }

       func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
           return 12

       }

       func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {

           return language[row]
       }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
           lblSelctedLanguage.text = language[row]
       }
}
