//
//  ConnectDetailVC.swift
//  Gir Gau Jatan Sansthan
//
//  Created by RAJ ORIYA on 29/11/20.
//  Copyright © 2020 Miral Gondaliya. All rights reserved.
//

import UIKit
import Kingfisher

class ConnectDetailVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
   @IBOutlet weak var imgCategory: UIImageView!
        @IBOutlet weak var lblnodata: UILabel!
        var strTitle = String()
        var strId :String = ""
        var city:String = ""
        
        var isNewDataLoading = Bool()
        var pageNumber = Int()
        var totalPage = Int()
        
        @IBOutlet weak var lblTitle : UILabel!
        
        @IBOutlet weak var viewNoData: UIView!
        
        @IBOutlet weak var tableForList : UITableView!
        
        var arrList = NSMutableArray()
        var gavpalan1:String = ""
        var milkandmilk1:String = ""
        var gaurakshak1:String = ""
        var vetenariydoctor1:String = ""
        
        var gavpalan2:String = "Gavpalan"
        var milkandmilk2:String = "Buymilkandmilk"
        var gaurakshak2:String = "GavRashadal"
        var vetenariydoctor2:String = "vetinarydoctor"

        override func viewDidLoad() {
            super.viewDidLoad()
             print(city)
            print(gavpalan1)
            print(milkandmilk1)
            print(gaurakshak1)
            print(vetenariydoctor1)
            //self.lblTitle.text = "Connect - \(strTitle)"
            
            self.pageNumber = 1
            self.arrList.removeAllObjects()
            if(gavpalan1 == "Gavpalan"){
                self.tableForList.isHidden = true

                self.viewNoData.isHidden = false
                
               self.lblnodata.text = "No Connection Found!"
                self.imgCategory.image = UIImage(named: "logo")
                self.lblTitle.text = "Connect - \("Gaupalak Matrimon")"


            }
            else if (gaurakshak1 == "GavRashadal"){
                self.tableForList.isHidden = true
                self.viewNoData.isHidden = false
                self.lblnodata.text = "No Connection Found!"
                self.lblTitle.text = "Connect - \("Gau Raksha Dal")"
                self.imgCategory.image = UIImage(named: "logo")

            }
             else if(milkandmilk1 == "Buymilkandmilk"){
                self.tableForList.isHidden = true
                self.viewNoData.isHidden = false
                self.lblnodata.text = "No Connection Found!"
                self.lblTitle.text = "Connect - \("Buy Milk and Milk Products")"
                self.imgCategory.image = UIImage(named: "logo")
            }
            else {
                if(vetenariydoctor1 == "vetinarydoctor"){
                getDetails()
                self.lblTitle.text = "Connect - \("Veterinary doctor near me")"

          }
            else{
                  self.tableForList.isHidden = true
                  self.viewNoData.isHidden = false
                  self.lblnodata.text = "No Connection Found!"
                  self.lblTitle.text = "Connect - \("Veterinary doctor near me")"
                  self.imgCategory.image = UIImage(named: "logo")
                }
        }
        }
        func getDetails()
        {
            if (Util.isInternetAvailable()) {
                ActivityIndicatorVC.sharedInstance.startIndicator()

                if(isNewDataLoading)
                {
                    pageNumber += 1
                }
                
              //SharedAppDelegate.showLoader()

                let parameter: [String: Any] = ["user_id": Util.sharedInstance.user_id, "user_unique_id": Util.sharedInstance.user_unique_id,"cat_id":strId,"city":city, "page" : String(format:"%d",pageNumber)]
                print(parameter)
                
                ServerAPIs.postRequestWithoutToken(apiUrl: apiConnectDetail, parameter, completion: { (response, error, statusCode) in
                   // SharedAppDelegate.hideLoader()
                    ActivityIndicatorVC.sharedInstance.stopIndicator()

                    if response["success"].intValue == 1 {
                        
                        self.arrList.addObjects(from:(response["data"].object as! NSArray) as! [Any])
                        print(self.arrList)
                        
                        self.totalPage = response["total_page"].intValue
                        print(self.totalPage)
                        
                        DispatchQueue.main.async {
                            
                            if(self.arrList.count > 0)
                            {
                                self.viewNoData.isHidden = true
                                self.tableForList.isHidden = false
                                self.tableForList.reloadData()
                            }
                            else
                            {
                                self.tableForList.isHidden = true
                                self.viewNoData.isHidden = false
                            }
                        }
                        
                    } else {
                        //Toast(text: response["message"].stringValue).show()
                        self.tableForList.isHidden = true
                        self.viewNoData.isHidden = false
                    }
                })
                
            } else {
                Toast(text: Util.localized("InternetError")).show()
            }
        }
        
        //MARK: - Tableview Methods
        
        func numberOfSections(in tableView: UITableView) -> Int {
            return 1
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            
            return arrList.count
            
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            let cell = tableForList.dequeueReusableCell(withIdentifier:"listCell", for:indexPath) as! ConnectDetailCell
            
            let singleObj = arrList[indexPath.row] as! NSDictionary
            cell.lblCity.text = singleObj .value(forKey:"conn_city") as? String
            cell.lblMainName.text = singleObj .value(forKey:"conn_supplier_name") as? String
            cell.lblName.text = singleObj .value(forKey:"conn_company_name") as? String
            cell.lblCity.text = singleObj .value(forKey:"conn_city") as? String
            cell.lblState.text = singleObj .value(forKey:"conn_state") as? String
            cell.lblAddress.text = (singleObj .value(forKey:"conn_address") is NSNull) ? "N/A" : singleObj .value(forKey:"conn_address") as? String
            cell.btnCall.tag = indexPath.row
            cell.btnCall.addTarget(self, action: #selector(onClickCall(_:)), for:.touchUpInside)
                    
            if let image = singleObj .value(forKey:"original_main_image") as? String {
                cell.img.image = UIImage()
                if URL(string: image) != nil {
                    let resource = ImageResource(downloadURL: URL(string: image)!, cacheKey: image)
                    cell.img.kf.setImage(with: resource)
                    cell.img.backgroundColor = UIColor.clear
                }
            }
            
            return cell
        }
        
        func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
            
            if scrollView == tableForList {
                
                if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height)
                {
                    if !isNewDataLoading {
                        
                        isNewDataLoading = true
                        
                        if(self.pageNumber < self.totalPage)
                        {
                            self.getDetails()
                        }
                    }
                }
            }
        }
        
        @objc @IBAction func onClickCall(_ sender:UIButton)
        {
            let phoneDict = self.arrList[sender.tag] as! NSDictionary
            
            if(phoneDict .value(forKey:"conn_contact_no") is NSNull)
            {
                
            }
            else
            {
                let phone = phoneDict .value(forKey:"conn_contact_no") as! String
                
                guard let number = URL(string: "tel://" + phone) else { return }
                UIApplication.shared.open(number)
            }
        }
        
        @IBAction func Action(_ sender: Any) {
            
            SharedAppDelegate.navigationControl.popViewController(animated: true)
        }
        
    }
