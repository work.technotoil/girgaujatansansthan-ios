//
//  ConnectVC.swift
//  Gir Gau Jatan Sansthan
//
//  Created by RAJ ORIYA on 28/11/20.
//  Copyright © 2020 Miral Gondaliya. All rights reserved.
//

import UIKit
import Kingfisher

class ConnectVC: UIViewController, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UICollectionViewDelegate {
    
   @IBOutlet weak var collectionMain: UICollectionView!
        
        var arrConnects = NSMutableArray()
       var gavpalan:String = "Gavpalan"
       var milkandmilk:String = "Buymilkandmilk"
       var gaurakshadal:String = "GavRashadal"
       var vetenariydoctor:String = "vetinarydoctor"

        override func viewDidLoad() {
            super.viewDidLoad()
            
            callConnectApi()

            // Do any additional setup after loading the view.
        }
        
        func callConnectApi()
        {
            if (Util.isInternetAvailable()) {
                
                ActivityIndicatorVC.sharedInstance.startIndicator()

                let parameter: [String: Any] = ["user_id": Util.sharedInstance.user_id, "user_unique_id": Util.sharedInstance.user_unique_id];

                ServerAPIs.postRequestWithoutToken(apiUrl: apiConnect, parameter, completion: { (response, error, statusCode) in
                    ActivityIndicatorVC.sharedInstance.stopIndicator()
                    print(response)
                    
                    if response["success"].intValue == 1 {
                        self.arrConnects.removeAllObjects()
                        self.arrConnects.addObjects(from:(response["data"].object as! NSArray) as! [Any])
                        
                        DispatchQueue.main.async {
                            
                            if(self.arrConnects.count > 0)
                            {
                                self.collectionMain.isHidden = false
                                self.collectionMain.reloadData()
                            }
                            else
                            {
                                self.collectionMain.isHidden = true
                                self.Action(self)
                            }
                        }
                        
                    } else {
                        Toast(text: response["message"].stringValue).show()

                    }
                })

            } else {
                Toast(text: Util.localized("InternetError")).show()
            }
        }
        
        func numberOfSections(in collectionView: UICollectionView) -> Int {
            return 1
        }
        
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            
            return arrConnects.count
            
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            return CGSize(width: screenWidth / 2 - 10, height: screenWidth/2 + 20)
        }
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "connectCell", for: indexPath) as! ConnectCell
            
            let singleObj = arrConnects[indexPath.row] as! NSDictionary
            print(singleObj)
            let id = singleObj.object(forKey: "id") as! String
            print(id)
            cell.lblName.text = singleObj .value(forKey:"name") as? String
                    
            if let image = singleObj .value(forKey:"image") as? String {
                cell.img.image = UIImage()
                if URL(string: image) != nil {
                    let resource = ImageResource(downloadURL: URL(string: image)!, cacheKey: image)
                    cell.img.kf.setImage(with: resource)
                    cell.img.backgroundColor = UIColor.clear
                }
            }
            
            return cell
        }
        
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            
            let vc = storyboard?.instantiateViewController(withIdentifier: "StateListVC") as! StateListVC
            let singleObj = arrConnects[indexPath.row] as! NSDictionary
          //  let id = singleObj.object(forKey: "id") as! String
            let name = singleObj.object(forKey: "name") as! String
            
           if name == "Gaupalak Matrimon"{
            vc.gavpalan = gavpalan

            SharedAppDelegate.navigationControl.pushViewController(vc, animated: true)


    }
          else  if name == "Buy Milk and Milk Products"{
                    vc.milkandmilk = milkandmilk

                    SharedAppDelegate.navigationControl.pushViewController(vc, animated: true)


            }
            else  if name == "Gau Raksha Dal"{
            vc.gaurakshak = gaurakshadal

                    SharedAppDelegate.navigationControl.pushViewController(vc, animated: true)
               }
           else{
            vc.vetenariydoctor = vetenariydoctor

            SharedAppDelegate.navigationControl.pushViewController(vc, animated: true)

            }
        }
        @IBAction func Action(_ sender: Any) {
            
            SharedAppDelegate.navigationControl.popViewController(animated: true)
        }
        
        @IBAction func onClickProfile(_ sender: Any) {
            
            let vc = Util.getStoryboard().instantiateViewController(withIdentifier: "profile") as! ProfileVC
            SharedAppDelegate.navigationControl.pushViewController(vc, animated: true)
        }
    }
