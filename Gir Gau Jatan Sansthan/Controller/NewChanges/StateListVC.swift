//
//  StateListVC.swift
//  Gir Gau Jatan Sansthan
//
//  Created by mac on 13/01/21.
//  Copyright © 2021 Miral Gondaliya. All rights reserved.
//

import UIKit

class StateTableViewCell:UITableViewCell{
    
    @IBOutlet weak var lblStateList: UILabel!
    
}

class StateListVC: UIViewController {
    
    var strTitle = String()
    var strId = String()
    var gavpalan:String = ""
    var milkandmilk:String = ""
    var gaurakshak:String = ""
    var vetenariydoctor:String = ""

    @IBOutlet weak var state_TableView: UITableView!
    var array_StateList :NSMutableArray = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
                 statelist_Api()
                print(gavpalan)
                print(milkandmilk)
                print(gaurakshak)
                print(vetenariydoctor)
                //print(catId)
                //self.lblTitle.text = "Connect - \(strTitle)"

                //  state_TableView.delegate = self
                //  state_TableView.dataSource = self
                // Do any additional setup after loading the view.
            }

            @IBAction func action_back(_ sender: Any) {
                self.navigationController?.popViewController(animated: true)
                
            }
            
            func statelist_Api(){
                
                
                if (Util.isInternetAvailable()) {
                    ActivityIndicatorVC.sharedInstance.startIndicator()
                     // SharedAppDelegate.showLoader()

                    ServerAPIs.getRequestWithoutToken(apiUrl: apiStateList, completion: { (response, error, statusCode) in
                        ActivityIndicatorVC.sharedInstance.stopIndicator()
                      //  SharedAppDelegate.hideLoader()

                        if response["success"].intValue == 1 {
                            //self.array_StateList.removeAllObjects()
                            self.array_StateList.addObjects(from:(response["data"].object as! NSArray) as! [Any])
                            print(self.array_StateList)
                            DispatchQueue.main.async {
                                self.state_TableView.reloadData()
                            }
                        }
                    })
                }
            }
            
            override func viewWillAppear(_ animated: Bool) {
                self.state_TableView.reloadData()
            }
        }

        extension StateListVC: UITableViewDelegate,UITableViewDataSource{
            func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
                return array_StateList.count
            }
            
            func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                let cell = state_TableView.dequeueReusableCell(withIdentifier:"StateTableViewCell", for:indexPath) as! StateTableViewCell
                let statelist:NSDictionary = array_StateList[indexPath.row] as!NSDictionary
                let state_name:String = statelist.object(forKey: "state_name") as! String
                cell.lblStateList.text = state_name
                //cell.lblStateList.text = state_id
                
                return cell
            }
            func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
                
                let statelist:NSDictionary = array_StateList[indexPath.row] as!NSDictionary
                let state_id:String = statelist.object(forKey: "state_id") as! String
              //  let state_name:String = statelist.object(forKey: "state_name") as! String

                let selectstate = storyboard?.instantiateViewController(withIdentifier: "CityListVC") as! CityListVC
                selectstate.state_id = state_id
                selectstate.gavpalan = gavpalan
                selectstate.gaurakshak = gaurakshak
                selectstate.milkandmilk = milkandmilk
                selectstate.vetenariydoctor = vetenariydoctor
                self.navigationController?.pushViewController(selectstate, animated: true)
            }
            
            
        }
