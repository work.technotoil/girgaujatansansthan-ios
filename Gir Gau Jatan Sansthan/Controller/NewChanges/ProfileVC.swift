//
//  ProfileVC.swift
//  Gir Gau Jatan Sansthan
//
//  Created by RAJ ORIYA on 29/11/20.
//  Copyright © 2020 Miral Gondaliya. All rights reserved.
//

import UIKit

class ProfileVC: UIViewController {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var lblCity: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lblNumber.text = Util.sharedInstance.user_phone
        self.lblName.text = Util.sharedInstance.user_name
        self.lblCity.text = Util.sharedInstance.user_city

        // Do any additional setup after loading the view.
    }
    
    @IBAction func Action(_ sender: Any) {
        
        SharedAppDelegate.navigationControl.popViewController(animated: true)
    }
    
    @IBAction func onClickChangeLanguage(_ sender:UIButton)
    {
        let vc = Util.getStoryboard().instantiateViewController(withIdentifier: "SettingsVC") as! SettingsVC
        SharedAppDelegate.navigationControl.pushViewController(vc, animated: true)
    }
}
