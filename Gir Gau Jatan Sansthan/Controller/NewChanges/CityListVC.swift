//
//  CityListVC.swift
//  Gir Gau Jatan Sansthan
//
//  Created by mac on 13/01/21.
//  Copyright © 2021 Miral Gondaliya. All rights reserved.
//

import UIKit
class CityTableViewCell: UITableViewCell{
    @IBOutlet weak var lblCty: UILabel!
}

class CityListVC: UIViewController {
    
    @IBOutlet weak var City_TableView: UITableView!
    var strTitle = String()
    var array_cities :NSMutableArray = []
    var state_id:String = ""
    var gavpalan:String = ""
    var milkandmilk:String = ""
    var gaurakshak:String = ""
    var vetenariydoctor:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Citylist_Api()
        print(state_id)
        print(gavpalan)
        print(milkandmilk)
        print(gaurakshak)
        print(vetenariydoctor)
        self.City_TableView.reloadData()        // Do any additional setup after loading the view.
    }
    
    @IBAction func action_back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func Citylist_Api(){
        if (Util.isInternetAvailable()) {
            ActivityIndicatorVC.sharedInstance.startIndicator()

            ServerAPIs.getRequestWithoutToken(apiUrl: apiCityList + state_id, completion: { (response, error, statusCode) in
                ActivityIndicatorVC.sharedInstance.stopIndicator()
                print(response)
                
                if response["success"].intValue == 1 {
                    // self.array_StateList.removeAllObjects()
                    self.array_cities.addObjects(from:(response["data"].object as! NSArray) as! [Any])
                    print(self.array_cities)
                    DispatchQueue.main.async {
                    self.City_TableView.reloadData()
                    }
                }
            })
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.City_TableView.reloadData()
    }
}
extension CityListVC: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array_cities.count
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CityTableViewCell") as! CityTableViewCell
        let citylist:NSDictionary = array_cities[indexPath.row] as! NSDictionary
        let city_name:String = citylist.object(forKey: "city_name") as! String
        cell.lblCty.text = city_name
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj_ConnectDetailVC = storyboard?.instantiateViewController(withIdentifier: "ConnectDetailVC") as! ConnectDetailVC
        let citylist:NSDictionary = array_cities[indexPath.row] as! NSDictionary
        let city_name:String = citylist.object(forKey: "city_name") as! String
        obj_ConnectDetailVC.city = city_name
        obj_ConnectDetailVC.gavpalan1 = gavpalan
        obj_ConnectDetailVC.gaurakshak1 = gaurakshak
        obj_ConnectDetailVC.milkandmilk1 = milkandmilk
        obj_ConnectDetailVC.vetenariydoctor1 = vetenariydoctor
        self.navigationController?.pushViewController(obj_ConnectDetailVC, animated: true)
        
        
    }
    
    
}




