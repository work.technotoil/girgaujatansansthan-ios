//
//  AddBlog.swift
//  Gir Gau Jatan Sansthan
//
//  Created by RAJ ORIYA on 29/11/20.
//  Copyright © 2020 Miral Gondaliya. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import BLMultiColorLoader

class AddBlog: UIViewController {
    
    @IBOutlet weak var loader: BLMultiColorLoader!
    @IBOutlet weak var viewHeader: UIView!

    @IBOutlet weak var lblError: UILabel!
    @IBOutlet weak var imgSep: UIImageView!
    @IBOutlet weak var lblFiltName: UILabel!
    @IBOutlet weak var lblDetails: UILabel!
    @IBOutlet weak var btnPost: UIButton!
    @IBOutlet weak var imgAd: UIImageView!
    

    @IBOutlet weak var imgBorder: UIImageView!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var btnSelectImage: UIButton!
    @IBOutlet weak var txtDetails: UITextView!
    @IBOutlet weak var scrollMain: UIScrollView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    
    var imagePicker: UIImagePickerController = UIImagePickerController()
    var postId = ""
    var transactionId = ""
    var isImage = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        imgBorder.layer.cornerRadius = 5
        imgBorder.layer.borderColor = UIColor.darkGray.cgColor
        imgBorder.layer.borderWidth = 1
        viewHeader.addShadow()
        imagePicker.delegate = self
        lblDetails.text = Util.localized("Details")
        lblTitle.text = Util.sharedInstance.add_post
        btnPost.setTitle(Util.sharedInstance.add_post, for: .normal)
        lblFiltName.text = Util.localized("Select Image")
        loader.lineWidth = 2
        loader.colorArray = [appColour]
        scrollMain.contentSize.height = btnPost.frame.origin.y + btnPost.frame.size.height + 50
    }
    
    @IBAction func Actions(_ sender: UIButton) {
        if sender == btnBack {
            SharedAppDelegate.navigationControl.popViewController(animated: true)
        } else if sender == btnPost {
            self.view.endEditing(true)
            var error = false
            if Util.isStringNull(txtDetails.text!) && isImage == false {
                error = true
                imgSep.backgroundColor = UIColor.returnRGBColor(r: 176, g: 0, b: 32, alpha: 1.0)
                lblError.text = Util.localized("Please Enter Details or selected image")
                lblError.isHidden = false
            }
            if !error {
                //Post API
                apiCallAdPost()
            }

        } else if sender == btnSelectImage {
            let alertController = UIAlertController(title: Util.localized("App Name"), message: Util.localized("Select Ad Image"), preferredStyle: .alert)

            let YES = UIAlertAction(title: Util.localized("Select From Library"), style: .default, handler: { (action) -> Void in
                    self.imagePicker.sourceType = .photoLibrary
                    self.imagePicker.allowsEditing = false
                    self.present(self.imagePicker, animated: true, completion: nil)
                })

            let NO = UIAlertAction(title: Util.localized("Take Photo"), style: .default, handler: { (action) -> Void in

                    if UIImagePickerController.isSourceTypeAvailable(.camera) {
                        self.imagePicker.sourceType = .camera
                        self.imagePicker.allowsEditing = false
                        self.present(self.imagePicker, animated: true, completion: nil)
                    }
                })
            let cancle = UIAlertAction(title: Util.localized("Cancel"), style: .cancel, handler: { (action) -> Void in


                })
            alertController.addAction(YES)
            alertController.addAction(NO)
            alertController.addAction(cancle)
            self.navigationController!.present(alertController, animated: true, completion: nil)
        }else if sender == btnDelete{
            btnDelete.isHidden = true
            btnSelectImage.isHidden = false
            imgBorder.isHidden = false
            imgAd.image = UIImage()
            isImage = false
        }
    }
    
    func apiCallAdPost() {
        if (Util.isInternetAvailable()) {
            //  SharedAppDelegate.showLoader()
            
            loader.isHidden = false
            loader.startAnimation()
            self.btnPost.isHidden = true
            
            let parameter: [String: Any] = ["user_id": Util.sharedInstance.user_id, "user_unique_id": Util.sharedInstance.user_unique_id, "ads_name": "s", "ads_contact_no": "s", "ads_details": txtDetails.text! , "ads_is_blog" : "1"];
            var imageData = Data()
            var mimeType = ""
            if isImage {
                imageData = imgAd.image!.jpegData(compressionQuality: 1.0)!
                mimeType = "image/jpeg"
            }
            ServerAPIs.postMultipartRequestWithOptionData(apiUrl: apiAdPost, parameter, imageParameterName: "ads_image", imageData: imageData, mimeType: mimeType, fileName: "swift_file.jpeg", completion: { (response, error, statusCode) in
                
                if response["success"].intValue == 1 {
                    
                    self.Actions(self.btnBack)
                    
                } else {
                    
                }
            })
            
        } else {
        }
    }
    
}
extension AddBlog: UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController,
        didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any])
    {
        if let selectedImage = info[.originalImage] as? UIImage {
            self.imgAd.image = selectedImage
            self.isImage = true
            self.imgBorder.isHidden = true
            self.btnDelete.isHidden = false
            //self.createNewImage()
            self.btnSelectImage.isHidden = true
            self.dismiss(animated: true, completion: nil)

        }

    }
}
