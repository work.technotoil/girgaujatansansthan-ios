//
//  BlogVC.swift
//  Gir Gau Jatan Sansthan
//
//  Created by RAJ ORIYA on 29/11/20.
//  Copyright © 2020 Miral Gondaliya. All rights reserved.
//

import UIKit
import Kingfisher

class BlogVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableForList : UITableView!
    
    @IBOutlet weak var lblTitle : UILabel!
    
    @IBOutlet weak var btnAddNew : UIButton!
    
    var arrList = NSMutableArray()
    
    var strTitle = String()
    var isNewDataLoading = Bool()
    var pageNumber = Int()
    var totalPage = Int()
    var isBlog = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(isBlog)
        {
            self.btnAddNew.isHidden = false
        }
        else
        {
            self.btnAddNew.isHidden = true
        }
        
        self.lblTitle.text = strTitle
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.pageNumber = 1
        self.arrList.removeAllObjects()
        
        if(isBlog)
        {
            callBlogApi()
        }
        else
        {
            callSponsiredApi()
        }
    }
    
    func callSponsiredApi()
    {
        if (Util.isInternetAvailable()) {
            
            SharedAppDelegate.showLoader()
            
            if(isNewDataLoading)
            {
                pageNumber += 1
            }
            
            let parameter: [String: Any] = ["user_id": Util.sharedInstance.user_id, "user_unique_id": Util.sharedInstance.user_unique_id, "ads_is_blog" : "0", "page" : String(format:"%d",pageNumber)];
            
            ServerAPIs.postRequestWithoutToken(apiUrl: apiAdList, parameter, completion: { (response, error, statusCode) in
                SharedAppDelegate.hideLoader()
                print(response)
                if response["success"].intValue == 1 {
                    
                    self.totalPage = response["total_page"].intValue
                    self.arrList.addObjects(from:(response["data"].object as! NSArray) as! [Any])
                    print(self.arrList)
                    DispatchQueue.main.async {
                        
                        if(self.arrList.count > 0)
                        {
                            self.tableForList.isHidden = false
                            self.tableForList.reloadData()
                        }
                        else
                        {
                            self.tableForList.isHidden = true
                            self.Action(self)
                        }
                    }
                    
                } else {
                    Toast(text: response["message"].stringValue).show()
                    
                }
            })
            
        } else {
            Toast(text: Util.localized("InternetError")).show()
        }
    }
    
    func callBlogApi()
    {
        if (Util.isInternetAvailable()) {
            
            SharedAppDelegate.showLoader()
            
            if(isNewDataLoading)
            {
                pageNumber += 1
            }
            
            let parameter: [String: Any] = ["user_id": Util.sharedInstance.user_id, "user_unique_id": Util.sharedInstance.user_unique_id, "ads_is_blog" : "1", "page" : String(format:"%d",pageNumber)];
            print(parameter)
            
            ServerAPIs.postRequestWithoutToken(apiUrl: apiAdList, parameter, completion: { (response, error, statusCode) in
                SharedAppDelegate.hideLoader()
                
                if response["success"].intValue == 1 {
                    
                    self.totalPage = response["total_page"].intValue
                    self.arrList.addObjects(from:(response["data"].object as! NSArray) as! [Any])
                    
                    DispatchQueue.main.async {
                        
                        if(self.arrList.count > 0)
                        {
                            self.tableForList.isHidden = false
                            self.tableForList.reloadData()
                        }
                        else
                        {
                            self.tableForList.isHidden = true
                            self.Action(self)
                        }
                    }
                    
                } else {
                    Toast(text: response["message"].stringValue).show()
                    
                }
            })
            
        } else {
            Toast(text: Util.localized("InternetError")).show()
        }
    }
    
    //MARK: - Tableview Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrList.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableForList.dequeueReusableCell(withIdentifier:"listCell", for:indexPath) as! ConnectDetailCell
        
        let singleObj = arrList[indexPath.row] as! NSDictionary
        
        cell.lblName.text = (singleObj .value(forKey:"ads_details") is NSNull) ? "N/A" : singleObj .value(forKey:"ads_details") as? String
        
        if let image = singleObj .value(forKey:"original_main_image") as? String {
            cell.img.image = UIImage()
            if URL(string: image) != nil {
                let resource = ImageResource(downloadURL: URL(string: image)!, cacheKey: image)
                cell.img.kf.setImage(with: resource)
                cell.img.backgroundColor = UIColor.clear
            }
        }
        cell.btn_report.tag = indexPath.row
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let singleObj = arrList[indexPath.row] as! NSDictionary
        
        let vc = Util.getStoryboard().instantiateViewController(withIdentifier: "ImageDisplayVC") as! ImageDisplayVC
        if let image = singleObj["original_main_image"] as? String {
            vc.imgLink = image
        }
        SharedAppDelegate.navigationControl.pushViewController(vc, animated: true)
        
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if scrollView == tableForList {
            
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height)
            {
                if !isNewDataLoading {
                    
                    isNewDataLoading = true
                    
                    if(self.pageNumber < self.totalPage)
                    {
                        self.callBlogApi()
                    }
                }
            }
        }
    }
    
    @IBAction func Action(_ sender: Any) {
        
        SharedAppDelegate.navigationControl.popViewController(animated: true)
    }
    
    @IBAction func onClickAddAd(_ sender: Any) {
        
        
        let subscribe = Util.sharedInstance.user_id
        if subscribe == ""
        {
            let alert = UIAlertController(title: "Gir Gau Jatan Sasthan", message: "You Have to login first ", preferredStyle: .alert)
            let ok = UIAlertAction(title: "Ok", style: .default, handler: { action in
                
                let vc = Util.getStoryboard().instantiateViewController(withIdentifier: "ViewController") as! ViewController
                vc.skiplogin = true
                
                SharedAppDelegate.navigationControl.pushViewController(vc, animated: true)
                
            })
            
            alert.addAction(ok)
            
            DispatchQueue.main.async(execute: {
                self.present(alert, animated: true)
            })
            
            return;

        }
        
        let vc = Util.getStoryboard().instantiateViewController(withIdentifier: "addBlog") as! AddBlog
        SharedAppDelegate.navigationControl.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func action_reports(_ sender: UIButton) {
        
        let subscribe = Util.sharedInstance.user_id
        if subscribe == ""
        {
            let alert = UIAlertController(title: "Gir Gau Jatan Sasthan", message: "You Have to login first ", preferredStyle: .alert)
            let ok = UIAlertAction(title: "Ok", style: .default, handler: { action in
                
                let vc = Util.getStoryboard().instantiateViewController(withIdentifier: "ViewController") as! ViewController
                vc.skiplogin = true
                
                SharedAppDelegate.navigationControl.pushViewController(vc, animated: true)
                
            })
            
            alert.addAction(ok)
            
            DispatchQueue.main.async(execute: {
                self.present(alert, animated: true)
            })
            
            return;

        }
        let blog_list : NSDictionary = self.arrList.object(at: sender.tag) as! NSDictionary
        
        
        let ads_id :String = "\(blog_list.object(forKey: "ads_id") ?? "")"
        // Create the AlertController and add its actions like button in ActionSheet
        let actionSheetController = UIAlertController(title: "", message: "Options", preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            print("Cancel")
        }
        actionSheetController.addAction(cancelActionButton)
        
        let spam = UIAlertAction(title: "Block User", style: .default) { action -> Void in
            
            
            var parameter = [String: Any]()
            let ads_user_id :String = "\(blog_list.object(forKey: "ads_user_id") ?? "")"

            
            
            parameter = ["user_id": Util.sharedInstance.user_id,"other_user_id":ads_user_id]
            
            print(parameter)
            ServerAPIs.postRequestWithoutToken(apiUrl: appUserBlock, parameter, completion: { (response, error, statusCode) in
                //SharedAppDelegate.hideLoader()
                
                if statusCode == 200 || statusCode == 201 {
                    
                    if response["success"].intValue == 1
                    {
                            self.pageNumber = 1
                            self.arrList.removeAllObjects()

                            self.callBlogApi()
                    }
                    
                }
                
            })
            
            
        }
        actionSheetController.addAction(spam)
        
        let inappropriate = UIAlertAction(title: "Report a post", style: .default) { action -> Void in
            
            
            let actionSheetController = UIAlertController(title: "Report", message: "Why are you reporting this post ?", preferredStyle: .actionSheet)
            
            let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
                print("Cancel")
            }
            actionSheetController.addAction(cancelActionButton)
            
            let spam = UIAlertAction(title: "it's spam", style: .default) { action -> Void in
                if (Util.isInternetAvailable()) {
                    // SharedAppDelegate.sdLoader.startAnimating(atView: self.view)
                    
                    var parameter = [String: Any]()
                    
                    
                    parameter = ["user_id": Util.sharedInstance.user_id,"content_id":ads_id,"reason":"it's spam"]
                    
                    print(parameter)
                    ServerAPIs.postRequestWithoutToken(apiUrl: apiblogReport, parameter, completion: { (response, error, statusCode) in
                        //SharedAppDelegate.hideLoader()
                        
                        if statusCode == 200 || statusCode == 201 {
                            
                            if response["success"].intValue == 1
                            {
                                Toast(text: response["message"].stringValue).show()
                                
                            }
                            
                        }
                        
                    })
                    
                } else {
                    Toast(text: Util.localized("InternetError")).show()
                    
                }
            }
            actionSheetController.addAction(spam)
            
            let inappropriate = UIAlertAction(title: "it's inappropriate", style: .default) { action -> Void in
                
                if (Util.isInternetAvailable()) {
                    // SharedAppDelegate.sdLoader.startAnimating(atView: self.view)
                    
                    var parameter = [String: Any]()
                    
                    
                    parameter = ["user_id": Util.sharedInstance.user_id,"content_id":ads_id,"reason":"it's inappropriate"]
                    
                    
                    print(parameter)
                    ServerAPIs.postRequestWithoutToken(apiUrl: apiblogReport, parameter, completion: { (response, error, statusCode) in
                        //SharedAppDelegate.hideLoader()
                        
                        if statusCode == 200 || statusCode == 201 {
                            if response["success"].intValue == 1
                            {
                                
                                Toast(text: response["message"].stringValue).show()
                            }
                        }
                        
                    })
                    
                } else {
                    Toast(text: Util.localized("InternetError")).show()
                    
                }
                
            }
            actionSheetController.addAction(inappropriate)
            self.present(actionSheetController, animated: true, completion: nil)


            
        }
        actionSheetController.addAction(inappropriate)
        self.present(actionSheetController, animated: true, completion: nil)
        
        
    }
    
}
